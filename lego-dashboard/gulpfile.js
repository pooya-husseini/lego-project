var gulp = require('gulp');
var gulpConfig = require('./gulp.config');
var concat = require('gulp-concat');
var angularFilesort = require('gulp-angular-filesort'),
    inject = require('gulp-inject');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');
var mainBowerFiles = require('main-bower-files');
var cleanCSS = require('gulp-clean-css');
var copy = require('gulp-contrib-copy');
var mustache = require("gulp-mustache");
var through = require('through2');
var StringDecoder = require('string_decoder').StringDecoder;
var rename = require("gulp-rename");
var _ = require("lodash");
var template = require('gulp-template');

gulp.task('finalConcat', ["projectMinify"], function () {
    gulp.src(gulpConfig.config.jsSources.finalFiles)
        .pipe(clean());

    return gulp.src(gulpConfig.config.jsSources.finalFiles)
        .pipe(concat('index.js'))
        .pipe(gulp.dest('./src/main/resources/static/app/dist'));
});

gulp.task('projectMinify', ["depsMinify"], function () {
    return gulp.src(gulpConfig.config.jsSources.sources)
        .pipe(angularFilesort()).pipe(concat('project.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./src/main/resources/static/app/dist'));
});

gulp.task('injectCss', ["injectJs"], function () {
    var cssFiles = mainBowerFiles("**/*.css");
    console.log(cssFiles);
    return gulp.src(gulpConfig.config.indexPage)
        .pipe(inject(
            gulp.src(cssFiles.concat(gulpConfig.config.cssSources.sources))
            , {ignorePath: 'src/main/resources/static/', addRootSlash: false}))
        // .pipe(gulp.dest('./src/main/resources/static/'));
        .pipe(gulp.dest('./src/main/resources/static/'));
});

gulp.task('injectJs', ["injectDependencies", "renderTranslations", "renderRests"], function () {
    return gulp.src(gulpConfig.config.indexPage)
        .pipe(inject(
            gulp.src(gulpConfig.config.jsSources.sources)
                .pipe(angularFilesort())
            , {ignorePath: 'src/main/resources/static/', addRootSlash: false}))
        .pipe(gulp.dest('./src/main/resources/static/'));
});

gulp.task('injectDependencies', ["renderTranslations", "renderRests"], function () {
    return gulp.src(gulpConfig.config.indexPage)
        .pipe(inject(
            gulp.src(mainBowerFiles("**/*.js"))
            , {ignorePath: 'src/main/resources/static/', addRootSlash: false, name: "deps"}))
        .pipe(gulp.dest('./src/main/resources/static/'));

});

gulp.task('minifyCss', function () {
    var cssFiles = mainBowerFiles("**/*.css");

    return gulp.src(cssFiles)
        .pipe(concat("index.css"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./src/main/resources/static/app/dist'));

});

gulp.task('copyResources', function () {
    var bowerFiles = mainBowerFiles("**/*.woff").concat(mainBowerFiles("**/*.ttf"));
    return gulp.src(bowerFiles)
        .pipe(copy())
        .pipe(gulp.dest('./src/main/resources/static/app/dist'));
});

gulp.task('depsMinify', function () {
    var bowerFiles = mainBowerFiles("**/*.js");
    return gulp.src(bowerFiles)
        .pipe(concat('lego.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./src/main/resources/static/app/dist'));

});

gulp.task('release', ["depsMinify", "projectMinify", "finalConcat", "minifyCss", "copyResources", "renderTranslations", "renderRests"], function () {

});

gulp.task('development', [
    "injectJs",
    "injectDependencies",
    "injectCss",
    "renderTranslations",
    "renderRests",
    "renderCrudServices"], function () {
    
});

gulp.task('cdnizer', function () {
    gulp.src(gulpConfig.config.indexPage)
        .pipe(googlecdn(require('./bower.json'), {
            bowerComponents: './src/main/resources/static/vendors/',
            allowRev: true,
            allowMin: true
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('renderTranslations', function () {

    var decoder = new StringDecoder('utf8');
    return gulp.src("**/app/**/translations/*.json")
        .pipe(through.obj(function (chunk, enc, cb) {
            var contents = decoder.write(chunk.contents).trim();
            var fileNameReg = /(.+)-(.*)\.json/g;
            var fileName = chunk.relative.substring(chunk.relative.lastIndexOf("/") + 1);
            var fileVars = fileNameReg.exec(fileName);
            gulp.src("./src/main/resources/static/vendors/lego-web/dist/te/mustache/translation.mustache")
                .pipe(mustache({
                    prefix: fileVars[1],
                    translations: contents.substring(1, contents.length - 1),
                    locale: fileVars[2]
                }, {"extension": ".js"}))
                .pipe(rename({
                    basename: fileVars[1] + "-" + fileVars[2]
                }))
                .pipe(gulp.dest("./src/main/resources/static/app/gen-src/translations"));
            cb(null, chunk)
        }));
});

gulp.task('renderRests', function () {
    var decoder = new StringDecoder('utf8');
    gulp.src("**/app/**/rest/**/*.json")
        .pipe(through.obj(function (chunk, enc, cb) {

            var contents = JSON.parse(decoder.write(chunk.contents));

            var fileName = chunk.relative.substring(chunk.relative.lastIndexOf("/") + 1).replace(".json", "");

            var extraOperations = JSON.stringify(contents.extraOperations);

            if (contents.cacheableKeys != undefined) {
                var cacheKeysBody = _.map(contents.cacheableKeys, function (item) {
                    return "data." + item;
                }).join("+");
            }
            if (contents.cacheableValues != undefined) {
                var cacheValuesBody = _.map(contents.cacheableValues, function (item) {
                    return "data." + item;
                }).join("+");
            }

            var mustacheConfig = {
                module: contents.module,
                name: contents.name,
                url: contents.url,
                extraOperations: extraOperations == undefined ? "{}" : extraOperations,
                cacheKeys: contents.cacheableKeys != undefined,
                cacheKeyBody: cacheKeysBody,
                cacheValues: contents.cacheableValues != undefined,
                cacheValueBody: cacheValuesBody
            };

            gulp.src("./src/main/resources/static/vendors/lego-web/dist/te/mustache/restService.mustache")
                .pipe(mustache(mustacheConfig, {"extension": ".js"}))
                .pipe(rename({
                    basename: fileName
                }))
                .pipe(gulp.dest("./src/main/resources/static/app/gen-src/restServices"));
            cb(null, chunk)
        }));
});

gulp.task('renderCrudServices', function () {
    var decoder = new StringDecoder('utf8');
    gulp.src("**/app/**/services/crud/**/*.json")
        .pipe(through.obj(function (chunk, enc, cb) {
            var contents = JSON.parse(decoder.write(chunk.contents).trim());

            var fileName = chunk.relative.substring(chunk.relative.lastIndexOf("/") + 1).replace(".json", "");

            gulp.src("./src/main/resources/static/vendors/lego-web/dist/te/underscore/crudService.ejs")
                .pipe(template({service: contents}))
                .pipe(rename({
                    basename: fileName,
                    extname: ".js"
                }))
                .pipe(gulp.dest("./src/main/resources/static/app/gen-src/crudServices"));
            cb(null, chunk)
        }));
});