package com.caspco.spl.projectgenerator

import java.io.{File, InputStream}

import com.caspco.spl.cg.util.io.{FileStreamType, OutputStreamFactory, StreamType, StreamUtils}
import com.caspco.spl.cg.util.template.TemplateEngine
import com.caspco.spl.entitygenerator.model.ProjectModelDto
import com.caspco.spl.entitygenerator.xml.vpsuite.VpSuiteModelProjectMaker
import org.apache.commons.io.FileUtils

import scala.collection.JavaConverters._
import scala.io.Source
import scala.language.postfixOps
import scala.sys.process._

/**
  * @author : Pooya Hosseini
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: 12/29/15
  *         Time: 6:49 PM
  */

case class GenerationResult(resultPath: String, result: String, hasError: Boolean)
object Generator {

  //  dataBaseName: String,
  //  dataBaseUrl: String,
  //  dataBaseUsername: String,
  //  dataBasePassword: String,
  //  models: List[ClassModelDto],
  //  groupId: String,
  //  artifactId: String,
  //  projectName: String,
  //  packages: Set[String]

  def generate(projectModel: ProjectModelDto): GenerationResult = {

    val groupId = projectModel.getProperties.get("groupId")
    val artifactId = projectModel.getProperties.get("artifactId")
    val dataBaseUrl = projectModel.getProperties.get("dataBaseUrl")
    val dataBaseUsername = projectModel.getProperties.get("dataBaseUsername")
    val dataBasePassword = projectModel.getProperties.get("dataBasePassword")
    val dataBaseName = projectModel.getProperties.get("dataBaseName")
    val projectName = projectModel.getProperties.get("projectName")

    val destinationPath = System.getProperty("java.io.tmpdir") + "/" + groupId + "/" + artifactId + "/"
    val mkdirs: Boolean = new File(destinationPath).mkdirs()
    if (!mkdirs) {
      //      throw new FileNotFoundException(destinationPath)
    }

    FileUtils.copyDirectory(new File(this.getClass.getResource("/project").getFile), new File(destinationPath + "project"))

    implicit val streamType: StreamType = FileStreamType
    implicit val formatOutput = true

    val packages = VpSuiteModelProjectMaker.doMakeProject(projectModel)

    val packagesJava = (packages ++ Set("com.caspco.spl.project")).toList.asJava

    val application: String = TemplateEngine("/templates/velocity/project/spring-boot-application.vm",
      Map("packageNames" -> packagesJava)
    )

    val security: String = TemplateEngine("/templates/velocity/project/spring-boot-security-configuration.vm", Map.empty)

    val applicationProperties = TemplateEngine("/templates/velocity/project/applicationProperties.vm", Map(
      "databaseUrl" -> dataBaseUrl,
      "databaseUsername" -> dataBaseUsername,
      "databasePassword" -> dataBasePassword,
      "database" -> dataBaseName
    ))

    val pom: String = TemplateEngine("/templates/velocity/project/pom.vm",
      Map("groupId" -> groupId,
        "artifactId" -> artifactId,
        "projectName" -> projectName,
        "packages" -> packagesJava,
        "database" -> dataBaseName
      )
    )

    OutputStreamFactory(destinationPath + "/project/pom.xml") <# pom

    OutputStreamFactory(destinationPath + "/project/src/main/java/com/caspco/spl/project/configuration/Application.java") <# application

    OutputStreamFactory(destinationPath + "/project/src/main/resources/application.properties") <# applicationProperties

    val lnn = s"sh ${this.getClass.getResource("/shell/compile-spl-project.sh").getFile}  $destinationPath/project".lineStream_!
    val result = lnn.mkString("\n")

    OutputStreamFactory(destinationPath + "/project/src/main/java/com/caspco/spl/project/configuration/SecurityConfiguration.java") <# security

    val zipFile: String = destinationPath.substring(0, destinationPath.length - 1) + ".zip"

    StreamUtils.zip(destinationPath)
    FileUtils.deleteDirectory(new File(destinationPath))
    GenerationResult(zipFile, result, hasError = false)
  }
}

class StreamGobbler(is: InputStream) extends Thread {
  override def run() {
    Source.fromInputStream(is).bufferedReader().readLine()
  }
}