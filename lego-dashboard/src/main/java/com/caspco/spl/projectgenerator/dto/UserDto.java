package com.caspco.spl.projectgenerator.dto;

/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Dec 9, 2015 3:26:15 PM
 * @version 1.0.0
 */


import com.caspco.spl.wrench.annotations.cg.Generated;
import com.caspco.spl.wrench.json.JsonDateDeserializer;
import com.caspco.spl.wrench.json.JsonDateFormatter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class UserDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private Date lastLoginDate;


    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @JsonSerialize(using = JsonDateFormatter.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }
}