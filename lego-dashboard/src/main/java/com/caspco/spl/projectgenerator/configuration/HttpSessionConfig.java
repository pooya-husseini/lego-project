package com.caspco.spl.projectgenerator.configuration;

import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 1/23/16
 *         Time: 2:53 PM
 */

@EnableRedisHttpSession
public class HttpSessionConfig {
}
