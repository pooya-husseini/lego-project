package com.caspco.spl.projectgenerator.controllers;

import com.caspco.spl.entitygenerator.model.ClassModelDto;
import com.caspco.spl.entitygenerator.model.ProjectModelDto;
import com.caspco.spl.entitygenerator.xml.vpsuite.VpSuiteModelProjectMaker;
import com.caspco.spl.projectgenerator.GenerationResult;
import com.caspco.spl.projectgenerator.Generator;
import com.caspco.spl.projectgenerator.models.GenerationLogEntity;
import com.caspco.spl.projectgenerator.service.GenerationLogService;
import com.caspco.spl.wrench.locale.Locale;
import com.caspco.spl.wrench.spring.security.CurrentUserUtil;
import com.caspco.spl.wrench.utils.enums.EnumUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 1/5/16
 *         Time: 9:47 PM
 */

@RestController
@RequestMapping(value = "/com/caspco/spl/projectgenerator")
@Transactional
public class MainController {

    @Autowired
    private GenerationLogService logService;

    @Autowired
    private CurrentUserUtil userUtil;

    @RequestMapping(value = "/classInformation", method = RequestMethod.POST)
    public List<ClassModelDto> getClassesInformation(MultipartHttpServletRequest request) throws IOException {
//        try {
//            Thread.sleep(100000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        Map<String, MultipartFile> fileMap = request.getFileMap();
        for (Map.Entry<String, MultipartFile> item : fileMap.entrySet()) {
            MultipartFile file = item.getValue();
            String fileName = file.getOriginalFilename();

            if (!item.getValue().isEmpty()) {
                return VpSuiteModelProjectMaker.getClassesAsJava(item.getValue().getInputStream());
            }
        }
        return null;
    }

    @RequestMapping(value = "/locales", method = RequestMethod.GET)
    public List<EnumUtils.Value> getLocales() {
        return EnumUtils.convertEnumToList(Locale.values());
    }

    @RequestMapping(method = RequestMethod.POST)
    public void save(@Valid @RequestBody ProjectModelDto projectModel, HttpServletResponse response) throws IOException {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        GenerationResult result = Generator.generate(projectModel);

        response.reset();
        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment; filename=\"project.zip\"");
        response.addHeader("filename", "project.zip");

        File file = new File(result.resultPath());
        FileInputStream fis = new FileInputStream(file);
        ServletOutputStream os = response.getOutputStream();
        int read;
        byte[] bytes = new byte[1024];
        int size = 0;

        while ((read = fis.read(bytes)) > 0) {
            os.write(bytes, 0, read);
            size += read;
        }

        os.flush();

        stopWatch.stop();

        GenerationLogEntity obj = new GenerationLogEntity();
        obj.setDuration(stopWatch.getTime());
        obj.setResult(result.result());
        obj.setUsername(userUtil.getCurrentUser().getUsername());
        obj.setStartDate(new Date(stopWatch.getStartTime()));
        obj.setHasError(result.hasError());

        logService.save(obj);
    }
}