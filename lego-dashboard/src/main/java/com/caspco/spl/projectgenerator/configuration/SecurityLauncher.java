package com.caspco.spl.projectgenerator.configuration;

import com.caspco.spl.projectgenerator.security.UserSecurityProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/11/15
 *         Time: 4:02 PM
 */

@Configuration
public class SecurityLauncher extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        /*.successHandler(successHandler())*//*.defaultSuccessUrl("/")*/
        http
                .authorizeRequests()
                .antMatchers("/login/*",
                        "/app/resources/**",
                        "/credentials/**",
                        "/app/css/**",
                        "/app/lib/**",
                        "/login.html*", "/register.html*")
                .permitAll()
                .anyRequest()
                .fullyAuthenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .loginPage("/login.html")
                .passwordParameter("password")
                .usernameParameter("username")
                .and()
                .logout()
                .logoutUrl("/logout")
                .deleteCookies("JSESSIONID")
                .permitAll()
                .and()
                .csrf()
                .disable();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth, UserSecurityProvider securityProvider) throws Exception {
        auth.userDetailsService(securityProvider);
    }

}