package com.caspco.spl.projectgenerator.service;

import com.caspco.spl.projectgenerator.models.GenerationLogEntity;
import com.caspco.spl.projectgenerator.repository.GenerationLogRepository;
import com.caspco.spl.wrench.annotations.cg.Generated;
import com.caspco.spl.wrench.beans.service.SimpleService;
import org.springframework.stereotype.Service;

@Service
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class GenerationLogService extends SimpleService<GenerationLogEntity, Long, GenerationLogRepository> {

}