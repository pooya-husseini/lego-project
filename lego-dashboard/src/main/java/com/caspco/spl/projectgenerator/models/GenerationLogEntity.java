package com.caspco.spl.projectgenerator.models;

/**
 * @author : Pooya Hosseini
 * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 * Date: 10/12/2015
 * Time: 3:37 PM
 */

import com.caspco.spl.wrench.annotations.cg.Searchable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

//@Audited
@Entity
@Table(name = "GENERATION_LOG")
public class GenerationLogEntity {

    private Long id;
    private String result;
    private Date startDate;
    private String username;
    private Long duration;
    private Boolean hasError;

    @Id
    @GeneratedValue
    @Column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Searchable
    @NotNull
    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }


    /**
     * Getter for property 'result'.
     *
     * @return Value for property 'result'.
     */
    @Lob
    public String getResult() {
        return result;
    }

    /**
     * Setter for property 'result'.
     *
     * @param result Value to set for property 'result'.
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * Getter for property 'duration'.
     *
     * @return Value for property 'duration'.
     */
    public Long getDuration() {
        return duration;
    }

    /**
     * Setter for property 'duration'.
     *
     * @param duration Value to set for property 'duration'.
     */
    public void setDuration(Long duration) {
        this.duration = duration;
    }

    /**
     * Getter for property 'startDate'.
     *
     * @return Value for property 'startDate'.
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Setter for property 'startDate'.
     *
     * @param startDate Value to set for property 'startDate'.
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Getter for property 'hasError'.
     *
     * @return Value for property 'hasError'.
     */
    public Boolean getHasError() {
        return hasError;
    }

    /**
     * Setter for property 'hasError'.
     *
     * @param hasError Value to set for property 'hasError'.
     */
    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }
}