package com.caspco.spl.projectgenerator.dto;


import javax.validation.constraints.NotNull;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 3/9/16
 *         Time: 3:58 PM
 */


public class RegisterDto {


    private String name;
    private String family;
    private String username;
    private String password;
    private String repass;
    private String email;

    /**
     * Getter for property 'name'.
     *
     * @return Value for property 'name'.
     */
    @NotNull
    public String getName() {
        return name;
    }

    /**
     * Setter for property 'name'.
     *
     * @param name Value to set for property 'name'.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for property 'family'.
     *
     * @return Value for property 'family'.
     */
    @NotNull
    public String getFamily() {
        return family;
    }

    /**
     * Setter for property 'family'.
     *
     * @param family Value to set for property 'family'.
     */
    public void setFamily(String family) {
        this.family = family;
    }

    /**
     * Getter for property 'username'.
     *
     * @return Value for property 'username'.
     */
    @NotNull
    public String getUsername() {
        return username;
    }

    /**
     * Setter for property 'username'.
     *
     * @param username Value to set for property 'username'.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Getter for property 'password'.
     *
     * @return Value for property 'password'.
     */
    @NotNull
    public String getPassword() {
        return password;
    }

    /**
     * Setter for property 'password'.
     *
     * @param password Value to set for property 'password'.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for property 'repass'.
     *
     * @return Value for property 'repass'.
     */
    @NotNull
    public String getRepass() {
        return repass;
    }

    /**
     * Setter for property 'repass'.
     *
     * @param repass Value to set for property 'repass'.
     */
    public void setRepass(String repass) {
        this.repass = repass;
    }

    /**
     * Getter for property 'email'.
     *
     * @return Value for property 'email'.
     */
    @NotNull
    public String getEmail() {
        return email;
    }

    /**
     * Setter for property 'email'.
     *
     * @param email Value to set for property 'email'.
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
