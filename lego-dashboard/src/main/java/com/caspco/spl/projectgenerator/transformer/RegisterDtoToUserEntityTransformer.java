package com.caspco.spl.projectgenerator.transformer;


/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Dec 9, 2015 3:26:16 PM
 * @version 1.0.0
 */

import com.caspco.spl.projectgenerator.dto.RegisterDto;
import com.caspco.spl.projectgenerator.models.UserEntity;
import com.caspco.spl.wrench.annotations.cg.Generated;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.stereotype.Component;


@Component
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class RegisterDtoToUserEntityTransformer extends SimpleBeanTransformer<RegisterDto, UserEntity> {


    @Override
    public UserEntity convert(RegisterDto source, Object parent) {

        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(source.getEmail());
        userEntity.setFirstName(source.getName());
        userEntity.setLastName(source.getFamily());
        userEntity.setPassword(source.getPassword());
        userEntity.setUsername(source.getUsername());
        return userEntity;
    }
}