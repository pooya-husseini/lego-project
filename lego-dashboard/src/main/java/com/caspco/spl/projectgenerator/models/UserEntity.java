package com.caspco.spl.projectgenerator.models;

/**
 * @author : Pooya Hosseini
 * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 * Date: 10/12/2015
 * Time: 3:37 PM
 */

import com.caspco.spl.wrench.annotations.cg.ExactOnSearch;
import com.caspco.spl.wrench.annotations.cg.Searchable;
import com.caspco.spl.wrench.annotations.cg.Translation;
import com.caspco.spl.wrench.annotations.cg.Translations;
import com.caspco.spl.wrench.locale.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Audited
@Entity
@Table(name = "USER_MANAGEMENT")
public class UserEntity {

    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String email;
    private Date lastLoginDate;
    private String role;


    @Id
    @GeneratedValue
    @Column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column
    @Searchable
    @NotNull
    @Translations({@Translation(label = "Name", locale = Locale.English_United_States), @Translation(label = "نام", locale = Locale.Persian_Iran)})
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column
    @Searchable
    @NotNull
    @Translations({@Translation(label = "Last Name", locale = Locale.English_United_States), @Translation(label = "نام خانوادگی", locale = Locale.Persian_Iran)})
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(unique = true)
    @Searchable
    @NotNull
    @Translations({@Translation(label = "User Name", locale = Locale.English_United_States), @Translation(label = "شناسه کاربری", locale = Locale.Persian_Iran)})
    @ExactOnSearch
    @Pattern(regexp = "[a-zA-Z]([0-9]|[a-zA-Z])+")
    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    /**
     * Getter for property 'password'.
     *
     * @return Value for property 'password'.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter for property 'password'.
     *
     * @param password Value to set for property 'password'.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for property 'email'.
     *
     * @return Value for property 'email'.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter for property 'email'.
     *
     * @param email Value to set for property 'email'.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter for property 'role'.
     *
     * @return Value for property 'role'.
     */
    public String getRole() {
        return role;
    }

    /**
     * Setter for property 'role'.
     *
     * @param role Value to set for property 'role'.
     */
    public void setRole(String role) {
        this.role = role;
    }

    @PrePersist
    public void prePersist() {
        if (username != null) {
            username = username.toLowerCase();
        }
    }
}