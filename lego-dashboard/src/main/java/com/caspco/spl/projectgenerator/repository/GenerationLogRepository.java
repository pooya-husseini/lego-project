package com.caspco.spl.projectgenerator.repository;

/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Nov 3, 2015 10:14:00 PM
 * @version 1.0.0
 */


import com.caspco.spl.projectgenerator.models.GenerationLogEntity;
import com.caspco.spl.wrench.annotations.cg.Generated;
import com.caspco.spl.wrench.data.SwiftRepository;

@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public interface GenerationLogRepository extends SwiftRepository<GenerationLogEntity, Long> {

}