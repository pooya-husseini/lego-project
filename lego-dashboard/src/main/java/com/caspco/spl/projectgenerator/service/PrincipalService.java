package com.caspco.spl.projectgenerator.service;

import com.caspco.spl.projectgenerator.models.UserEntity;
import com.caspco.spl.projectgenerator.repository.UserRepository;
import com.caspco.spl.wrench.annotations.cg.Generated;
import com.caspco.spl.wrench.beans.service.SimpleService;
import org.springframework.stereotype.Service;

@Service("PrincipalService")
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class PrincipalService extends SimpleService<UserEntity, Long, UserRepository> {

    public UserEntity findByUsername(String s) {
        return repository.findByUsername(s);
    }
}