package com.caspco.spl.projectgenerator.dto;

/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : Lego Code Generator
 *         Email : husseini@caspco.ir
 *         Date: Dec 1, 2015 5:37:15 PM
 * @version 1.0.0
 */


public class InformationDto {
    private String version;

    public InformationDto(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}