package com.caspco.spl.project.model.rest;
/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Jan 2, 2016 1:10:14 PM
* @version 1.0.0
*/


import com.caspco.spl.wrench.beans.rest.BasicRestService;
import com.caspco.spl.wrench.annotations.cg.Generated;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


import com.caspco.spl.project.model.*;
import com.caspco.spl.project.model.dto.*;
import com.caspco.spl.project.model.service.*;


@RestController
@RequestMapping("/com/caspco/spl/project/model/users")
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class UserRestService extends BasicRestService<UserDto,UserEntity,UserService>{

    @Autowired
    protected UserRestService(UserService service) {
        super(service);
    }

    @Override
    public void handle(HttpMessageNotReadableException e) {

    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.PUT)
    public void changePassword(@Valid @RequestBody PasswordDto dto) {
        UserDto userDto = new UserDto();
        userDto.setUsername(dto.getUsername());

        List<UserEntity> list = getService().findBySample(getDTransformer().convert(userDto));

        if (list.size() != 1) {
            throw new IllegalArgumentException("User is not valid!");
        }

        UserEntity domainObject = list.get(0);
        if (!domainObject.getPassword().equals(dto.getOldPassword())) {
            throw new com.caspco.spl.wrench.exception.OldPasswordIsNotCorrect();
        }

        domainObject.setPassword(dto.getPassword());
        getService().save(domainObject);
    }

    @RequestMapping(value = "/userManagementRoles", method = org.springframework.web.bind.annotation.RequestMethod.GET)
    public java.util.List<com.caspco.spl.wrench.utils.enums.EnumUtils.Value> getUserManagementRoles() {
        return super.getEnumTypes(com.caspco.spl.project.model.UserManagementRole.class);
    }
}