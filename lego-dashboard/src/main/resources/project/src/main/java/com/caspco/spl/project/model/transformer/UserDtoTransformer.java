package com.caspco.spl.project.model.transformer;


/**
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Jan 2, 2016 1:10:14 PM
* @version 1.0.0
*/
import com.caspco.spl.project.model.*;
import com.caspco.spl.project.model.dto.*;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import com.caspco.spl.wrench.annotations.cg.Generated;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;



@Component
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class UserDtoTransformer extends SimpleBeanTransformer<UserDto,UserEntity> {
    
    @Override
    public UserEntity convert(UserDto o,Object parent) {
        if(o == null ) {
            return null;
        }
        UserEntity e=new UserEntity();
                        e.setId(o. getId());
                        e.setFirstName(o. getFirstName());
                        e.setLastName(o. getLastName());
                        e.setPassword(o. getPassword());
                        e.setUsername(o. getUsername());
                        e.setRole(o. getRole());
                        e.setLastLoginDate(o. getLastLoginDate());
                    
        return e;
    }

}