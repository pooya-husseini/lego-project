package com.caspco.spl.project.rest;
/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Dec 1, 2015 5:37:15 PM
 * @version 1.0.0
 */


import com.caspco.spl.project.dto.InformationDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/")
public class VersionRestService {

    @Value("${version}")
    private String version;

    @RequestMapping(value = "/information", method = RequestMethod.GET)
    public InformationDto get() {
        return new InformationDto(version);
    }
}