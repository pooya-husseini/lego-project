package com.caspco.spl.project.model.dto;

/**
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Jan 2, 2016 1:10:14 PM
* @version 1.0.0
*/


import com.caspco.spl.wrench.annotations.cg.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.caspco.spl.project.model.*;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class PasswordDto {
    private String oldPassword;
    private String password;
    private String username;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}