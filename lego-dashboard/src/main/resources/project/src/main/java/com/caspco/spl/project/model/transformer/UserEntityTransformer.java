package com.caspco.spl.project.model.transformer;


/**
* THIS IS AN AUTO GENERATED CODE.
* @author : Lego Code Generator
* Email : husseini@caspco.ir
* Date: Jan 2, 2016 1:10:14 PM
* @version 1.0.0
*/
import com.caspco.spl.project.model.*;
import com.caspco.spl.project.model.dto.*;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import com.caspco.spl.wrench.annotations.cg.Generated;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;


@Component("UserEntityTransformer")
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class UserEntityTransformer extends SimpleBeanTransformer<UserEntity,UserDto> {
    
    @Override
    public UserDto convert(UserEntity o,Object parent) {
        if(o == null ){
            return null;
        }
        UserDto e=new UserDto();
        
            e.setId(o. getId());
            e.setFirstName(o. getFirstName());
            e.setLastName(o. getLastName());
            e.setPassword(o. getPassword());
            e.setUsername(o. getUsername());
            e.setRole(o. getRole());
            e.setLastLoginDate(o. getLastLoginDate());
                    return e;
    }
}