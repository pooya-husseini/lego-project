package com.caspco.spl.project.model.transformer;

/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Jan 2, 2016 1:10:14 PM
* @version 1.0.0
*/



import com.caspco.xview.wrench.beans.transformer.SimpleBeanTransformer;
import com.caspco.spl.project.model.dto.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("PrincipalToDtoTransformer")
public class PrincipalToDtoTransformer extends SimpleBeanTransformer<User, UserDto> {


    @Override
    public UserDto convert(User source,Object parent) {

        UserDto user = new UserDto();
        user.setUsername(source.getUsername());
        user.setPassword(source.getPassword());
        if (!source.getAuthorities().isEmpty()) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.addAll(source.getAuthorities());

            user.setRole(authorities.get(0).getAuthority());
        }
        return user;
    }

}