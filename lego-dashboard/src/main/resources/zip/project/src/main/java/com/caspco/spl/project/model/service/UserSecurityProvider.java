
package com.caspco.spl.project.model.service;


import com.caspco.xview.wrench.beans.transformer.BasicTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/6/15
 *         Time: 2:18 PM
 */
@Service
public class UserSecurityProvider implements UserDetailsService {

    @Autowired
    private UserService service;

    private User defaultUser;

    @Autowired
    private BasicTransformer basicTransformer;

    public UserSecurityProvider() {
        this.defaultUser = new User("admin", "admin", AuthorityUtils.createAuthorityList("ROLE_ADMIN"));
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        if (defaultUser != null && !service.isEmpty()) {
            defaultUser = null;
        }

        if (defaultUser != null && defaultUser.getUsername().equalsIgnoreCase(s)) {
            return defaultUser;
        }

        User user = new User(s, "", Collections.EMPTY_LIST);
        user.eraseCredentials();
        User result = basicTransformer.convert(service.findByUsername(user.getUsername()), User.class);
        if (result == null) {
            throw new UsernameNotFoundException("Invalid username/password.");
        }
        return new User(result.getUsername().trim(), result.getPassword().trim(), result.getAuthorities());
    }
}