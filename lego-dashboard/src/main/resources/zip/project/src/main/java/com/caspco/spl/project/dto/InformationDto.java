package com.caspco.spl.project.dto;

public class InformationDto {
    private String version;

    public InformationDto(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}