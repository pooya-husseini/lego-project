package com.caspco.spl.project.model.service;

/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspco.ir
* Date: Jan 2, 2016 1:10:14 PM
* @version 1.0.0
*/

import com.caspco.spl.project.model.QUserEntity;
import com.caspco.spl.project.model.UserEntity;
import com.caspco.spl.project.model.repository.UserRepository;
import com.caspco.xview.wrench.annotations.cg.Generated;
import com.caspco.xview.wrench.beans.service.SimpleService;
import org.springframework.stereotype.Service;


@Service("UserService")
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class UserService extends SimpleService<UserEntity,java.lang.Long,UserRepository> {
    protected QUserEntity q = QUserEntity.userEntity;

    public UserEntity findByUsername(String username) {
        return repository.findByUsername(username);
    }
}