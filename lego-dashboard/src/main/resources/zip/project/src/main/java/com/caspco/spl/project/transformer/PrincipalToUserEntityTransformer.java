package com.caspco.spl.project.transformer;


import com.caspco.spl.project.model.UserEntity;
import com.caspco.xview.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PrincipalToUserEntityTransformer extends SimpleBeanTransformer<User, UserEntity> {

    @Override
    public UserEntity convert(User source, Object parent) {

        UserEntity user = new UserEntity();
        user.setUsername(source.getUsername());
        user.setPassword(source.getPassword());
        if (!source.getAuthorities().isEmpty()) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.addAll(source.getAuthorities());

            user.setRole(authorities.get(0).getAuthority());
        }
        return user;
    }
}