package com.caspco.spl.project.model;

/**
 * @author : Pooya Hosseini
 * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 * Date: 10/12/2015
 * Time: 3:37 PM
 */

import com.caspco.xview.wrench.annotations.cg.*;
import com.caspco.xview.wrench.annotations.cg.security.Password;
import com.caspco.xview.wrench.annotations.cg.security.UserPrincipal;
import com.caspco.xview.wrench.locale.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Audited
@Translations({@Translation(label = "User", locale = Locale.English_United_States), @Translation(label = "کاربران", locale = Locale.Persian_Iran)})
@UserPrincipal
@Entity
@Table(name = "USER_MANAGEMENT")
@Ignore(mods = {IgnoreType.DTO, IgnoreType.SERVER_SIDE_CONTROLLER, IgnoreType.SERVER_SIDE_SERVICES, IgnoreType.TRANSFORMER})
public class UserEntity {

    private Long id;
    private String firstName;
    private String lastName;
    private String password;
    private String username;
    private String role;
    private Date lastLoginDate;

    @Id
    @GeneratedValue
    @Column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column
    @Searchable
    @NotNull
    @Translations({@Translation(label = "Name", locale = Locale.English_United_States), @Translation(label = "نام", locale = Locale.Persian_Iran)})
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column
    @Searchable
    @NotNull
    @Translations({@Translation(label = "Last Name", locale = Locale.English_United_States), @Translation(label = "نام خانوادگی", locale = Locale.Persian_Iran)})
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column
//    @Type(type = "encryptedString")
    @NotNull
    @Password
    @Translations({@Translation(label = "PassWord", locale = Locale.English_United_States), @Translation(label = "کلمه عبور", locale = Locale.Persian_Iran)})
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(unique = true)
    @Searchable
    @NotNull
    @Translations({@Translation(label = "User Name", locale = Locale.English_United_States), @Translation(label = "شناسه کاربری", locale = Locale.Persian_Iran)})
    @ExactOnSearch
    @Pattern(regexp = "[a-zA-Z]([0-9]|[a-zA-Z])+")
    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    @Column
    @Searchable
    @Translations({@Translation(label = "Role", locale = Locale.English_United_States), @Translation(label = "نقش", locale = Locale.Persian_Iran)})
    @NotNull
    @EnumeratedWith(UserManagementRole.class)
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }
}