#parse("/template/util/array-utils.vm")
package com.caspco.spl.project.configuration;

import com.caspco.spl.data.dataaccess.swift.SwiftRepositoryFactoryBean;
import com.caspco.spl.wrench.beans.transformer.BasicTransformer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Vector;

@SpringBootApplication
@ComponentScan(basePackages = { "#mkString(${packageNames},'","')", "com.caspco.spl.wrench.utils.reflection"})
@EntityScan(basePackages = {"#mkString(${packageNames},'","')", "com.caspco.spl.data.revision"})
@PropertySource("classpath:version.properties")
@EnableJpaRepositories(value = {"#mkString(${packageNames},'","')"}, repositoryFactoryBeanClass = SwiftRepositoryFactoryBean.class)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public Cache getCache() {
        return new org.springframework.cache.concurrent.ConcurrentMapCache("MainCache", false);
    }

    @Bean
    public CacheManager getCacheManager() {
        SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
        Vector<Cache> caches = new Vector<>();
        caches.add(getCache());
        simpleCacheManager.setCaches(caches);
        return simpleCacheManager;
    }

    @Bean
    public BasicTransformer basicTransformer() {
        return new BasicTransformer();
    }
}