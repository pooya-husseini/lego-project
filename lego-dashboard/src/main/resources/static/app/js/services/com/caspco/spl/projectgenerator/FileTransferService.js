/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Dec 1, 2015 5:37:15 PM
 * @version 1.0.0
 */

(function (module) {
    module.service("FileTransferService", Service);

    Service.$inject = ["$http"];
    function Service($http) {

        var url = '/com/caspco/spl/projectgenerator/classInformation';

        return {
            send: function (data) {
                $http({
                    method: 'POST',
                    url: '/com/caspco/spl/projectgenerator',
                    data: JSON.stringify(data),
                    headers: {accept: 'application/zip'},
                    responseType: 'arraybuffer'
                }).success(
                    function (data, status, headers, config) {
                        var fileName = headers("filename");
                        var contentType = headers("content-type");

                        var a = document.createElement("a");
                        document.body.appendChild(a);
                        a.style.display = "none";

                        var blob = new Blob([data], {type: contentType}),
                            url = window.URL.createObjectURL(blob);
                        a.href = url;
                        a.download = fileName;
                        a.click();
                        window.URL.revokeObjectURL(url);

                    }).error(
                    function (failed) {
                        notify($scope.translate('app.failed'), "danger");
                    }
                );
            },
            upload: function (item) {
                var children = item.getElementsByTagName("input");
                var fileInput = {};

                if (item.files !== undefined) {
                    fileInput = item;
                } else {
                    for (var i = 0; i < children.length; i++) {
                        if (children[i].className.indexOf('ng-hide') < 0) {
                            fileInput = children[i];
                            break;
                        }
                    }
                }

                var formData = new FormData();
                formData.append("file", fileInput.files[0]);

                return $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': undefined},
                    data: formData,
                    transformRequest: angular.identity
                });
            }
        }
    }
})(angular.module("application"));