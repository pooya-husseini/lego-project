/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Dec 1, 2015 5:37:15 PM
 * @version 1.0.0
 */

(function (module) {

    module.controller('XmlUploaderController', Controller);

    Controller.$inject = ['$timeout', 'FileTransferService', '$state', 'SplRestService', 'utils'];

    function Controller($timeout, FileTransferService, $state, SplRestService, utils) {
        this.Action = {};
        this.Model = {};
        this.Model.properties = {groupId: "ir.caspco.spl.project"};
        this.pageCount = 0;
        //this.currentPage = 0;
        this.currentModel = {};
        this.currentField = {};

        $state.go("classModel.uploader");

        this.FieldsGrid = {
            columnDefs: getFieldsGridHeaders(),
            data: 'currentModel.fields',
            multiSelect: false,
            enableColumnMenus: false,
            isSelected: true,
            enableHighlighting: true,
            enableColumnReordering: true,
            enableColumnResize: true,
            enableRowSelection: true,
            enableRowHeaderSelection: false
        };

        this.$watch('currentModel.fields', function () {
            this.FieldsGrid.isSelected = false;
        });


        function getFieldsGridHeaders() {
            return [
                {
                    field: 'properties["name"]',
                    displayName: 'نام'
                },
                {
                    field: 'type',
                    displayName: 'نوع'
                }
            ];
        }

        this.$watch('currentPage', function (newValue, oldValue) {
            if (!angular.equals(newValue, oldValue)) {
                this.currentModel = this.Model.classes[newValue - 1];
                if (this.currentModel.properties.tableName == undefined) {
                    this.currentModel.properties.tableName = this.currentModel.properties.name;
                }
                $state.go("classModel." + (newValue - 1));
            }
        });

        this.Action.nextPage = function () {
            if (this.currentPage < this.pageCount) {
                this.currentPage = this.currentPage + 1;
            }
        };

        this.Action.previousPage = function () {
            if (this.currentPage > 0) {
                this.currentPage = this.currentPage - 1;
            }
        };

        this.Action.send = function () {
            if (this.validate()) {
                FileTransferService.send(this.Model).success(function () {

                });
            }
        };

        this.validate = function () {
            for (var i = 0; i < this.Model.classes.length; i++) {
                var obj = this.Model.classes[i];
                if (obj.properties["label"] == undefined) {
                    notify(" لطفا برای کلاس" + obj.properties["name"] + " یک نام انتخاب نمایید ", "danger");
                    return false;
                }

                for (var j = 0; j < obj.fields.length; j++) {
                    var field = obj.fields[j];
                    if (field.properties["label"] == undefined) {
                        notify(" لطفا برای فیلد " + field.properties["name"] + " در کلاس " + obj.properties["name"] + " یک نام انتخاب نمایید ", "danger");
                        return false;
                    }
                }
            }
            return true;
        };

        this.Action.upload = function (item) {
            FileTransferService.upload(item).success(function (data) {

                this.Model.classes = data;
                this.pageCount = data.length;

                angular.forEach(data, function (value, key) {
                    var state = {
                        url: "/classModel" + key,
                        templateUrl: '/app/view/com/caspco/spl/projectgenerator/classModel.html',
                        parent: 'classModel'
                    };
                    module.$stateProviderRef.state("classModel." + key, state);
                });
                this.currentPage = 1;
                //$state.go('classModel.0');
            }).error(function () {
                notify(this.translate('app.upload.failed'), "danger");
            });
        };

        (function () {
            SplRestService.getLocales().$promise.then(function (success) {
                this.Locales = ModelService.convertEnum(success, 'locales');
            });
            this.Databases = [{code: "mysql"}, {code: "oracle"}]
        })();
    }


})(angular.module('application'));