package com.caspco.spl.cgpluign;
/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 21/06/14
 *         Time: 14:49
 */

import com.caspco.spl.cg.bootstrap.CodeGeneratorJobType;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "clean", threadSafe = true)
public class CodeCleaningPlugin extends AbstractCodeGenerationPlugin {

    @Override
    protected CodeGeneratorJobType task() {
        return CodeGeneratorJobType.CLEAN;
    }
}