package com.caspco.spl.cgpluign;

/**
 *         @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */

public interface ProjectBuilder {
    void build();
}
