package com.caspco.spl.cgpluign;

import com.caspco.spl.cg.bootstrap.CGConfig;
import com.caspco.spl.cg.bootstrap.CodeGenerationConfiguration;
import com.caspco.spl.cg.bootstrap.CodeGeneratorJobType;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.DefaultProjectBuilderConfiguration;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 06/12/14
 *         Time: 15:24
 */
public abstract class AbstractCodeGenerationPlugin extends AbstractPlugin implements ProjectBuilder {

    @Parameter(property = "project", required = true, readonly = true)
    private MavenProject mavenProject;

    @Component
    private MavenProjectBuilder mavenProjectBuilder;

    @Parameter
    private String[] scanPackages = new String[]{"com.caspco", "com.caspian"};

    @Parameter(name = "inputPath")
    private String inputPath = "";

    @Parameter(name = "mainPageTypeAhead")
    private String mainPageTypeAhead = "";

    @Parameter(name = "formatOutput")
    private boolean formatOutput = Boolean.FALSE;

    @Parameter(name = "compressJsFiles")
    private boolean compressJsFiles = Boolean.FALSE;

    @Parameter(name = "validate")
    private Boolean validate = Boolean.FALSE;

    @Parameter(name = "packagePrefix", required = true)
    private String packagePrefix = "";

    @Parameter(name = "serverOutputPath")
    private String serverOutputPath = "";

    @Parameter(name = "serverResourcesPath")
    private String serverResourcesPath = "";

//    @Parameter(name = "reportOutputPath")
//    private String reportOutputPath = "";


    @Parameter(name = "clientOutputPath")
    private String clientOutputPath = "";

    @Parameter(name = "scalaServerOutputPath")
    private String scalaServerOutputPath = "";

    @Parameter(property = "project.build.directory")
    private File buildDirectory;


    @Parameter(property = "basedir")
    private File baseDirectory;

    @Parameter(alias = "includes")
    private String[] mIncludes;

    @Parameter(alias = "excludes")
    private String[] mExcludes;

    public String[] getScanPackages() {
        return scanPackages;
    }

    public String getInputPath() {
        return inputPath;
    }

    public String getPackagePrefix() {
        return packagePrefix;
    }

    public String getServerOutputPath() {
        return serverOutputPath;
    }

    public String getClientOutputPath() {
        return clientOutputPath;
    }

    public File getBuildDirectory() {
        return buildDirectory;
    }

//    public String getReportOutputPath() {
//        return reportOutputPath;
//    }


    public Boolean getValidate() {
        return validate;
    }

    public String getServerResourcesPath() {
        return serverResourcesPath;
    }

    public File getBaseDirectory() {
        return baseDirectory;
    }

    public String[] getmIncludes() {
        return mIncludes;
    }

    public String[] getmExcludes() {
        return mExcludes;
    }

    public String getScalaServerOutputPath() {
        return scalaServerOutputPath;
    }

    public String getMainPageTypeAhead() {
        return mainPageTypeAhead;
    }

    public boolean isFormatOutput() {
        return formatOutput;
    }

    public boolean isCompressJsFiles() {
        return compressJsFiles;
    }

    @Override
    public MavenProject getMavenProject() {
        return mavenProject;
    }

    @Override
    public List<Runnable> getTasks() {

        List<Runnable> runnables = new ArrayList<>();
        String targetPath = buildDirectory.getPath() + File.separator + getMavenProject().getBuild().getFinalName();
        CodeGenerationConfiguration config = new CodeGenerationConfiguration();
        CGConfig pathConfig = new CGConfig();
        pathConfig.addConfiguration(CGConfig.BootstrapAttribute.INPUT_PATH, inputPath);
        pathConfig.addConfiguration(CGConfig.BootstrapAttribute.SERVER_OUTPUT_PATH, serverOutputPath);
        pathConfig.addConfiguration(CGConfig.BootstrapAttribute.CLIENT_OUTPUT_PATH, clientOutputPath);
        pathConfig.addConfiguration(CGConfig.BootstrapAttribute.SCALA_SERVER_OUTPUT_PATH, scalaServerOutputPath);
        pathConfig.addConfiguration(CGConfig.BootstrapAttribute.SERVER_SIDE_RESOURCE_PATH, serverResourcesPath);
        pathConfig.addConfiguration(CGConfig.BootstrapAttribute.VALIDATE, validate);
        pathConfig.addConfiguration(CGConfig.BootstrapAttribute.TARGET_PATH, targetPath);
        pathConfig.addConfiguration(CGConfig.BootstrapAttribute.MAIN_PAGE_TYPE_AHEAD, mainPageTypeAhead);
        pathConfig.addConfiguration(CGConfig.BootstrapAttribute.FORMAT_OUTPUT, formatOutput);
        pathConfig.addConfiguration(CGConfig.BootstrapAttribute.COMPRESS_JS, compressJsFiles);
//        pathConfig.addConfiguration(PathConfig.BootstrapAttribute.REPORT_OUTPUT_PATH, reportOutputPath);

        config.setCgConfig(pathConfig);

        config.setTask(task());
        runnables.add(new PluginTask(mavenProject.getBasedir().getPath(), packagePrefix, config, this));

        return runnables;
    }

    public MavenProjectBuilder getMavenProjectBuilder() {
        return mavenProjectBuilder;
    }

    protected abstract CodeGeneratorJobType task();

    @Override
    public void build() {
        try {
            DefaultProjectBuilderConfiguration config = new DefaultProjectBuilderConfiguration();
            config.setBuildStartTime(new Date());
            MavenProject build = mavenProjectBuilder.build(mavenProject.getFile(), config);

        } catch (ProjectBuildingException e) {
            throw new RuntimeException(e);
        }
    }
}