package com.caspco.spl.cgpluign; /**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 21/06/14
 *         Time: 14:49
 */

import com.caspco.spl.cg.bootstrap.CodeGeneratorJobType;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "preview", threadSafe = true)
public class CodePreviewPlugin extends AbstractCodeGenerationPlugin {

    @Override
    protected CodeGeneratorJobType task() {
        return CodeGeneratorJobType.PREVIEW;
    }
}