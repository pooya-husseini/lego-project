package com.caspco.spl.cgpluign;//import com.caspco.infrastructure.xview.generator.CodeGenerator;


import com.caspco.spl.cg.bootstrap.CGConfig;
import com.caspco.spl.cg.bootstrap.CodeGenerationConfiguration;
import com.caspco.spl.cg.bootstrap.CodeGenerator;
import com.caspco.spl.wrench.annotations.cg.EnumEntity;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import javax.persistence.Entity;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 21/06/14
 *         Time: 17:57
 */
public class PluginTask implements Runnable {
    private final CodeGenerationConfiguration config;
    private final String packagePrefix;
    private final ProjectBuilder projectBuilder;
    private String baseDir;
    private Reflections reflections;

    public PluginTask(String baseDir, String packagePrefix, CodeGenerationConfiguration config, ProjectBuilder projectBuilder) {
        this.baseDir = baseDir;
        this.config = config;
        this.packagePrefix = packagePrefix;
        this.projectBuilder = projectBuilder;
    }

    @Override
    public void run() {

        config.getCgConfig().setPrefix(baseDir);

        if (packagePrefix != null && !packagePrefix.trim().isEmpty()) {

            List<Class<?>> entities = new ArrayList<>();
            entities.addAll(extractEntityClasses());

            List<Class<?>> enums = new ArrayList<>();
            enums.addAll(extractEnumEntityClasses());

            config.setClasses(entities);
            config.setEnums(enums);
            switch (config.getTask()) {
                case CLEAN:
                    CodeGenerator.entityCleaner(config);
                    break;
                case GENERATE:
                    CodeGenerator.entityCodeGenerator(config);
                    break;
                case PREVIEW:
                    CodeGenerator.entityCodeGenerator(config);
                    projectBuilder.build();
                    CodeGenerator.entityCodePreviewer(config);
                    break;

            }
        } else if (config.getCgConfig().getSimpleConfiguration(CGConfig.BootstrapAttribute.INPUT_PATH) != null) {
//            CodeGenerator.xmlCodeGenerator(config);
        } else {
            throw new IllegalArgumentException("Package prefix or Input path should not be empty!");
        }
    }


    private Set<Class<?>> extractEntityClasses() {
        createReflections();

        return reflections.getTypesAnnotatedWith(Entity.class);
    }

    private Set<Class<?>> extractEnumEntityClasses() {
        createReflections();
        return reflections.getTypesAnnotatedWith(EnumEntity.class);
    }

    private void createReflections() {
        if (reflections == null) {
            if (packagePrefix.contains(";")) {
                String[] split = packagePrefix.split(";");
                FilterBuilder filterBuilder = null;
                Set<URL> urls = new HashSet<>();
                for (String s : split) {
                    if (filterBuilder == null) {
                        filterBuilder = new FilterBuilder().include(FilterBuilder.prefix(s));
                    } else {
                        filterBuilder = filterBuilder.include(FilterBuilder.prefix(s));
                    }
                    urls.addAll(ClasspathHelper.forPackage(s));
                }

                reflections = new Reflections(
                        new ConfigurationBuilder()
                                .filterInputsBy(filterBuilder)
                                .setUrls(urls).setScanners(new SubTypesScanner(),new TypeAnnotationsScanner())
                );
            } else {
                reflections = new Reflections(new ConfigurationBuilder()
                        .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(packagePrefix)))
                        .setUrls(ClasspathHelper.forPackage(packagePrefix))
                        .setScanners(new SubTypesScanner(),new TypeAnnotationsScanner())
                );
            }
        }
    }
}