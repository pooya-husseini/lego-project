package com.caspco.spl.projectgenerator.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 1/5/16
 *         Time: 9:47 PM
 */

@RestController
@RequestMapping(value = "/com/caspco/spl/projectgenerator")
public class MainController {

    @RequestMapping(value = "/make", method = RequestMethod.GET)
    public void doMakeProject() {

    }
}