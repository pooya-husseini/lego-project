/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Dec 10, 2015 12:50:32 PM
 * @version 1.0.0
 */


module.service("InformationRestService", ['$resource', function ($resource) {
    return $resource('/information', {}, {
        information: {method: 'GET'}
    });
}]);