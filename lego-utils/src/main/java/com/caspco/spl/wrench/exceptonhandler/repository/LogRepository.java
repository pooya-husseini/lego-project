package com.caspco.spl.wrench.exceptonhandler.repository;

import com.caspco.spl.wrench.data.SwiftRepository;
import com.caspco.spl.wrench.exceptonhandler.LogEntity;

/**
* THIS IS AN AUTO GENERATED CODE.
* @author : XVIEW CODE GENERATOR
* Email : husseini@caspian.com
* Date: Nov 24, 2014 2:13:05 PM
* @version 1.0.0
*/



public interface LogRepository extends SwiftRepository<LogEntity,Long> {

}