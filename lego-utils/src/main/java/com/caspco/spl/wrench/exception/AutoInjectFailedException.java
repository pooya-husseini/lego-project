package com.caspco.spl.wrench.exception;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 24/12/14
 *         Time: 11:38
 */
public class AutoInjectFailedException  extends SplException {
    public AutoInjectFailedException() {
    }

    public AutoInjectFailedException(String message) {
        super(message);
    }

    public AutoInjectFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AutoInjectFailedException(Throwable cause) {
        super(cause);
    }

    public AutoInjectFailedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}