package com.caspco.spl.wrench.utils.string;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 24/12/14
 *         Time: 18:46
 */
public class StringUtils {
    public static final String DEFAULT_SEPARATOR = ",";

    private StringUtils() {
    }

    public static String capitalize(String str) {
        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }

    public static String makeString(Object[] objects) {
        return makeString(objects, DEFAULT_SEPARATOR);
    }

    public static String makeString(Object[] objects, String separator) {
        return makeString(Arrays.asList(objects), separator);
    }

    public static String makeString(Collection<?> objects) {
        return makeString(objects, DEFAULT_SEPARATOR);
    }

    public static String makeString(Collection<?> objects, String separator) {
        String result = "";
        if (objects.size() > 0) {
            for (Object o : objects) {
                result += o + separator;
            }
            return result.substring(0, result.length() - 1);
        } else {
            return "";
        }
    }
}
