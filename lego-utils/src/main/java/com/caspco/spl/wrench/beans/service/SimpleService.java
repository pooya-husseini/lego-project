package com.caspco.spl.wrench.beans.service;

/**
 * @author : Пуя Гуссейни
 * Email : info@pooya-hfp.ir
 * Date: 23/06/14
 * Time: 17:37
 */

import com.caspco.spl.wrench.data.SwiftRepository;
import com.caspco.spl.wrench.locale.LocaleService;
import com.caspco.spl.wrench.report.ReportEngine;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.List;

public abstract class SimpleService<E, K extends Serializable, R extends SwiftRepository<E, K>> {

    @Autowired
    protected R repository;
    @Autowired(required = false)
    private ReportEngine reportUtil;

    @Autowired
    private LocaleService localeService;


    public boolean isEmpty() {
        return repository.count() == 0;
    }

    public E save(E obj) {
        return repository.save(obj);
    }

    public E findById(K id) {
        return repository.findOne(id);
    }

    public List<E> findBySample(E e) {
        return repository.find(e);
    }

    public List<E> findAll() {
        return repository.findAll();
    }

    public boolean exists(K id) {
        return repository.exists(id);
    }

    public boolean exists(E e) {

        return repository.exists(e);
    }

    public long count(E e) {
        return repository.count(e);
    }

    public long countAll() {
        return repository.count();
    }

    // delete operations
    public void removeById(K id) {
        repository.delete(id);
    }

    public void remove(E e) {

        repository.delete(e);
    }

    public void removeAll() {
        repository.deleteAllInBatch();
    }

    public List<String> searchColumn(String column, String value) {
        return repository.searchColumn(column, value);
    }

    public void report(String reportType, E e, HttpServletResponse response, String locale) {
        final List<E> entities = repository.find(e);
        reportUtil.sendReport(entities, e.getClass().getName(), localeService.findLocale(locale), reportType, response);
    }

    public void report(String reportName, String reportType, E e, HttpServletResponse response, String locale) {
        final List<E> entities = repository.find(e);
        reportUtil.sendReport(entities, reportName, localeService.findLocale(locale), reportType, response);
    }
}