package com.caspco.spl.wrench.exception;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 19/01/15
 *         Time: 14:08
 */
public class OldPasswordIsNotCorrect extends SplException {
    public OldPasswordIsNotCorrect() {
    }

    public OldPasswordIsNotCorrect(String message) {
        super(message);
    }

    public OldPasswordIsNotCorrect(String message, Throwable cause) {
        super(message, cause);
    }

    public OldPasswordIsNotCorrect(Throwable cause) {
        super(cause);
    }

    public OldPasswordIsNotCorrect(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
