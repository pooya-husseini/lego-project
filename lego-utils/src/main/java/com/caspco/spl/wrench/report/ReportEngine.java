package com.caspco.spl.wrench.report;

import com.caspco.spl.wrench.annotations.cg.Report;
import com.caspco.spl.wrench.locale.Locale;
import com.caspco.spl.wrench.utils.reflection.TypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 20/09/14
 *         Time: 12:58
 */
public abstract class ReportEngine {

    @Autowired
    protected TypeUtils typeUtils;
    @Value("${server.reports.basepath}")
    private String reportUrl;

//    public ReportUtil(URL reportUrl) {
//        this.reportUrl = reportUrl;
//    }

    public ClassPathResource getReportClassPath() {
//        String path = reportUrl.getPath();
//        String path = reportUrl;
//        if (!path.endsWith(File.separator)) {
//            path += File.separator;
//        }
        return new ClassPathResource(reportUrl);
    }

    protected String getReportName(Class<?> clazz) {
        final Report annotation = clazz.getAnnotation(Report.class);

        if (annotation != null) {
            return (String) typeUtils.getAnnotationValue(annotation, "name");
        } else {
            return clazz.getSimpleName().replace("Entity", "");
        }
    }

    public abstract void sendReport(List<?> items, String reportId, Locale locale, String reportType,
                                    HttpServletResponse response);

}