package com.caspco.spl.wrench.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 16/09/14
 *         Time: 18:44
 */
@Deprecated
public class JsonDateFormatter extends JsonSerializer<Date> {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public void serialize(Date date, JsonGenerator gen, SerializerProvider provider) throws IOException {

        String formattedDate = dateFormat.format(date);
        String fieldName = gen.getOutputContext().getCurrentName();

        gen.writeString(formattedDate);

    }


}