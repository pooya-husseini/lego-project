package com.caspco.spl.wrench.utils.reflection;


import com.caspco.spl.wrench.exception.SplException;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.ThreadSafe;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@CacheConfig(cacheNames = "typeUtil")
@ThreadSafe
public class TypeUtils {



    public ClassUtils resolveType(Class<?> aClass) {
        return new ClassUtils(aClass);
    }

    /**
     * Gets property value.
     *
     * @param o            the o
     * @param propertyName the property name
     * @return the property value
     */
    public Object getPropertyValue(Object o, String propertyName) {
        try {
            PropertyDescriptor descriptor =resolveType(o.getClass()).getPropertyDescriptor(propertyName);
            return descriptor.getReadMethod().invoke(o);
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new SplException(e);
        }
    }

    /**
     * Sets property value.
     *
     * @param o            the o
     * @param propertyName the property name
     * @param value        the value
     * @return the property value
     */
    public Object setPropertyValue(Object o, String propertyName, Object value) {
        try {
            PropertyDescriptor descriptor =resolveType(o.getClass()). getPropertyDescriptor(propertyName);
            return descriptor.getWriteMethod().invoke(o, value);
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new SplException(e);
        }
    }


    /**
     * Gets annotation value.
     *
     * @param a    the a
     * @param name the name
     * @return the annotation value
     */
    @Cacheable
    public Object getAnnotationValue(Annotation a, String name) {
        try {
            MethodDescriptor[] descriptors = Introspector.getBeanInfo(a.getClass()).getMethodDescriptors();
            return Arrays.stream(descriptors).filter(item -> item.getName().equals(name)).findFirst().get().getMethod().invoke(a);
        } catch (IllegalAccessException | IntrospectionException | InvocationTargetException e) {
            throw new SplException(e);
        }
    }

    /**
     * Object to string string.
     *
     * @param o         the o
     * @param delimiter the delimiter
     * @return the string
     */
    public String objectToString(Object o, String delimiter) {
        Stream<PropertyDescriptor> descriptors = resolveType(o.getClass()).getPropertyDescriptors();
        return descriptors
                .filter(item -> getPropertyValue(o, item.getName()) != null)
                .map(item -> item.getName() + "=" + getPropertyValue(o, item.getName()) + delimiter)
                .reduce((x, y) -> x + y).get();
    }


    /**
     * Object to map map.
     *
     * @param o the o
     * @return the map
     */
    public Map<String, Object> objectToMap(Object o) {
        Stream<PropertyDescriptor> descriptors = resolveType(o.getClass()).getPropertyDescriptors();
        return descriptors
                .filter(item -> getPropertyValue(o, item.getName()) != null)
                .collect(Collectors.toMap(PropertyDescriptor::getName, item -> getPropertyValue(o, item.getName())));
    }

    /**
     * Gets generic object type.
     *
     * @param t the t
     * @return the generic object type
     */
    @Cacheable
    public Class<?> getGenericObjectType(Type t) {
        if (t instanceof ParameterizedType) {
            Type elementType = ((ParameterizedType) t).getActualTypeArguments()[0];
            return (Class<?>) elementType;
        } else {
            return null;
        }
    }

    /**
     * Is types equals boolean.
     *
     * @param type1 the type 1
     * @param type2 the type 2
     * @return the boolean
     */
    @Cacheable
    public Boolean isTypesEquals(Class<?> type1, Class<?> type2) {
        return type1.isAssignableFrom(type2) || type2.isAssignableFrom(type1);
    }
}