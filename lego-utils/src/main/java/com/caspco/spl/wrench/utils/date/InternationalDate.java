package com.caspco.spl.wrench.utils.date;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.ULocale;

import java.util.Date;
import java.util.Locale;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 28/12/14
 *         Time: 11:56
 */
public class InternationalDate {

    private final ULocale locale;

    private final Calendar calendar;

    private final String format;


    public InternationalDate(Locale locale, Date date) {
        this.locale = new ULocale(locale.toString());
        this.calendar = Calendar.getInstance(this.locale);
        calendar.setTime(date);
        format = "yyyy MMMM d";
    }

    public InternationalDate(Locale locale, Date date, String format) {
        this.locale = new ULocale(locale.toString());
        this.calendar = Calendar.getInstance(this.locale);
        calendar.setTime(date);
        this.format = format;
    }

    public InternationalDate(Locale locale, String format) {
        this.locale = new ULocale(locale.toString());
        this.calendar = Calendar.getInstance(this.locale);
        this.format = format;
    }
    public InternationalDate(Locale locale) {
        this.locale = new ULocale(locale.toString());
        this.calendar = Calendar.getInstance(this.locale);
        format = "yyyy MMMM d";
    }

    public String formatDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(format, locale);
        return formatter.format(date);
    }

    public String formatDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(format, locale);
        return formatter.format(calendar);
    }


    @Override
    public String toString() {
        return formatDate();
    }
}