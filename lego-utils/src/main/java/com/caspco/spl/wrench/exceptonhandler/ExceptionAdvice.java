package com.caspco.spl.wrench.exceptonhandler;


import com.caspco.spl.wrench.exception.SplException;
import com.caspco.spl.wrench.exceptonhandler.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/16/15
 *         Time: 3:48 PM
 */
@ControllerAdvice
@Transactional(noRollbackFor = SplException.class)
public class ExceptionAdvice {

    @Autowired
    private LogRepository repository;

    @Autowired(required = false)
    private ExceptionTranslator exceptionTranslator;

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "An error occurred on server!")
    @ExceptionHandler(Exception.class)
    public void logError(Exception e, HttpServletResponse response) throws SplException {
        try {
            SplException translateException = translateException(e);
//            response.addHeader("exception_type", translateException.);
            response.setHeader("exceptionType",translateException.getExceptionType());
            response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
            persistException(translateException);
            throw translateException;
        } catch (IOException e1) {
            throw translateException(e1);
        }
    }

    public void persistException(Exception e) {
        LogEntity logEntity = new LogEntity();
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        e.printStackTrace(pw);
        logEntity.setTime(new Date());
        logEntity.setExceptionMessage(sw.getBuffer().toString());
        repository.save(logEntity);
    }

    private SplException translateException(Exception x) {
        if (exceptionTranslator != null) {
            return exceptionTranslator.translateException(x);
        }
        return new SplException(x);
    }
}