package com.caspco.spl.wrench.exception;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 19/01/15
 *         Time: 11:08
 */
public class MailNotConfiguredException extends SplException {
    public MailNotConfiguredException() {
    }

    public MailNotConfiguredException(String message) {
        super(message);
    }

    public MailNotConfiguredException(String message, Throwable cause) {
        super(message, cause);
    }

    public MailNotConfiguredException(Throwable cause) {
        super(cause);
    }

    public MailNotConfiguredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
