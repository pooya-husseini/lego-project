package com.caspco.spl.wrench.exceptonhandler;

import com.caspco.spl.wrench.exception.SplException;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/29/15
 *         Time: 11:47 AM
 */
public interface ExceptionTranslator {
    SplException translateException(Exception x);
}
