package com.caspco.spl.wrench.data;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/9/15
 *         Time: 10:21 PM
 */

public class HistoricalEntity {
    private Object id;
    private ModificationType modificationType;

    public HistoricalEntity() {
    }


    public HistoricalEntity(Object id, ModificationType modificationType) {
        this.id = id;
        this.modificationType = modificationType;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public ModificationType getModificationType() {
        return modificationType;
    }

    public void setModificationType(ModificationType modificationType) {
        this.modificationType = modificationType;
    }

    public enum ModificationType {
        DELETED, MODIFIED, INSERTED
    }
}


