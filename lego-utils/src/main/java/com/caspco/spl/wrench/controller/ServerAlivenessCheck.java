package com.caspco.spl.wrench.controller;
/**
 * THIS IS AN AUTO GENERATED CODE.
 *
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Jan 6, 2015 11:25:59 AM
 * @version 1.0.0
 */

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RequestMapping("/alive")
@RestController
public class ServerAlivenessCheck {
    @RequestMapping(method = RequestMethod.GET)
    public Date alive() throws Exception {
        return new Date();
    }
}