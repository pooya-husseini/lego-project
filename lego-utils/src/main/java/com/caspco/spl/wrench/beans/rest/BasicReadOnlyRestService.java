package com.caspco.spl.wrench.beans.rest;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/8/15
 *         Time: 11:02 AM
 */

import com.caspco.spl.wrench.beans.service.SimpleService;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
import com.caspco.spl.wrench.utils.enums.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Transactional
public abstract class BasicReadOnlyRestService<D, E,R extends SimpleService> {

    private final R service;
    private int pageSize = 10;
    @Autowired
    private SimpleBeanTransformer<D, E> dtoeTransformer;
    @Autowired
    private SimpleBeanTransformer<E, D> etodTransformer;

    protected BasicReadOnlyRestService(R service) {
        this.service = service;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public D getById(@PathVariable Long id) {
        return etodTransformer.convert((E) service.findById(id));
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<D> getAll() {
        return etodTransformer.convert(service.findAll());
    }


    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public Long getCount(@Valid @RequestBody D dto) {
        return service.count(dtoeTransformer.convert(dto));
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public List<D> search(@RequestBody D dto, @RequestParam(defaultValue = "0", value = "pageIndex") int pageIndex) {
        List<E> entities = service.findBySample(dtoeTransformer.convert(dto));
        return etodTransformer.convert(entities);
    }

    @RequestMapping(value = "/report/{reportType}/{locale}", method = RequestMethod.POST)
    public void report(@PathVariable String reportType, @PathVariable String locale, @Valid @RequestBody D dto, HttpServletResponse response) {
        service.report(reportType, dtoeTransformer.convert(dto), response, locale);
    }

    //    @RequestMapping(value = "/enumTypes", method = RequestMethod.GET)
    public List<EnumUtils.Value> getEnumTypes(Class<? extends Enum> e) {
        return EnumUtils.convertEnumToList(e.getEnumConstants());
    }

    @ExceptionHandler
    public abstract void handle(HttpMessageNotReadableException e);

    @RequestMapping(value = "/searchColumn", method = RequestMethod.GET)
    public List<String> searchColumns(@RequestParam("column") String column, @RequestParam("value") String value) {
        return service.searchColumn(column, value);
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }


    protected R getService() {
        return service;
    }

    protected SimpleBeanTransformer<D, E> getDTransformer() {
        return dtoeTransformer;
    }

    protected SimpleBeanTransformer<E, D> getETransformer() {
        return etodTransformer;
    }
}