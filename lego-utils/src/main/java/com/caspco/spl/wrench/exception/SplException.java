package com.caspco.spl.wrench.exception;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 31/12/14
 *         Time: 14:23
 */
public class SplException extends RuntimeException {

    private String exceptionType;


    public SplException() {
    }

    public SplException(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public SplException(String message, String exceptionType) {
        super(message);
        this.exceptionType = exceptionType;
    }

    public SplException(String message, Throwable cause, String exceptionType) {
        super(message, cause);
        this.exceptionType = exceptionType;
    }

    public SplException(Throwable cause, String exceptionType) {
        super(cause);
        this.exceptionType = exceptionType;
    }

    public SplException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String exceptionType) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.exceptionType = exceptionType;
    }

    public SplException(String message, Throwable cause) {
        super(message, cause);
    }

    public SplException(Throwable cause) {
        super(cause);
    }

    public SplException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public String getExceptionType() {
        if (exceptionType == null) {
            return this.getCause().getClass().getName();
        }
        return exceptionType;
    }
}
