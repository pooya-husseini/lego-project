package com.caspco.spl.wrench.exceptions;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/29/15
 *         Time: 12:39 PM
 */
public class CanNotTransformException extends RuntimeException {
    public CanNotTransformException(String s) {
    }
}
