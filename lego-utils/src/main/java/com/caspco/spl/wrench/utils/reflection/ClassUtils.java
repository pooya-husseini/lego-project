package com.caspco.spl.wrench.utils.reflection;

import com.caspco.spl.wrench.exception.SplException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.Assert;

import javax.annotation.concurrent.ThreadSafe;
import java.beans.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The type Class utils.
 */
@ThreadSafe
public class ClassUtils {
    private final Class clazz;

    public ClassUtils(Class clazz) {
        this.clazz = clazz;
    }
    public static ClassUtils create(Class clazz) {
        return new ClassUtils(clazz);
    }

    /**
     * Find field field.
     *
     * @param fieldName the field name
     * @return the field
     */
    @Cacheable
    public Field findField(String fieldName) {
        return findField(clazz, fieldName);
    }

    private  Field findField(Class<?> clazz, String fieldName) {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            findField(clazz.getSuperclass(), fieldName);
        }
        return null;
    }


    @Cacheable
    public Stream<PropertyDescriptor> getPropertyDescriptors() {
        try {
            BeanInfo info = Introspector.getBeanInfo(clazz);
            return Arrays.stream(info.getPropertyDescriptors()).filter(item -> !"class".equals(item.getName()));
        } catch (IntrospectionException e) {
            throw new SplException(e);
        }
    }


    public PropertyDescriptor getPropertyDescriptor(String propertyName) {
        Stream<PropertyDescriptor> descriptors = getPropertyDescriptors();
        return descriptors.filter(desc -> desc.getName().equals(propertyName)).findFirst().get();
    }

    /**
     * Gets property annotations.
     *
     * @param propertyName the property name
     * @return the property annotations
     */
    @Cacheable
    public Stream<Annotation> getPropertyAnnotations(String propertyName) {
        List<Annotation> annotations = new ArrayList<>();

        Field field = findField(propertyName);
        if (field != null) {
            Collections.addAll(annotations, field.getAnnotations());
        }
        PropertyDescriptor descriptor = getPropertyDescriptor(propertyName);
        Collections.addAll(annotations, descriptor.getReadMethod().getAnnotations());
        Collections.addAll(annotations, descriptor.getWriteMethod().getAnnotations());
        return annotations.stream();
    }

    /**
     * Gets property annotation.
     *
     * @param propertyName    the property name
     * @param annotationClass the annotation class
     * @return the property annotation
     */
    @Cacheable
    public Annotation getPropertyAnnotation(String propertyName, Class<? extends Annotation> annotationClass) {
        PropertyDescriptor descriptor = getPropertyDescriptor(propertyName);

        Annotation declaredAnnotation = descriptor.getReadMethod().getAnnotation(annotationClass);
        if (declaredAnnotation == null) {
            declaredAnnotation = descriptor.getWriteMethod().getAnnotation(annotationClass);
        }
        if (declaredAnnotation == null) {
            Field field = findField(propertyName);
            if (field != null) {
                return field.getAnnotation(annotationClass);
            }
        }
        return declaredAnnotation;
    }

    /**
     * Gets property bound methods.
     *
     * @param propertyName the property name
     * @return the property bound methods
     */
    @Cacheable
    public Stream<Method> getPropertyBoundMethods(String propertyName) {
        PropertyDescriptor propertyDescriptor = getPropertyDescriptor(propertyName);
        return Stream.of(propertyDescriptor.getReadMethod(), propertyDescriptor.getWriteMethod());
    }

    /**
     * Is property annotated with boolean.
     *
     * @param propertyName the property name
     * @param annotation   the annotation
     * @return the boolean
     */
    @Cacheable
    public boolean isPropertyAnnotatedWith(String propertyName, Class<? extends Annotation> annotation) {
        return getPropertyAnnotations(propertyName).filter(a -> a.annotationType() == annotation).count() > 0;
    }

    /**
     * Is property annotated with any boolean.
     *
     * @param propertyName the property name
     * @param annotations  the annotations
     * @return the boolean
     */
    @Cacheable
    public boolean isPropertyAnnotatedWithAny(String propertyName, List<Class<? extends Annotation>> annotations) {

        Assert.notNull(annotations);
        Assert.isTrue(annotations.size() > 0);
        List<Class<? extends Annotation>> collect = getPropertyAnnotations(propertyName)
                .map(Annotation::annotationType).collect(Collectors.toList());
        collect.retainAll(annotations);
        return collect.size() > 0;
    }


    /**
     * Is property annotated with all boolean.
     *
     * @param propertyName the property name
     * @param annotations  the annotations
     * @return the boolean
     */
    @Cacheable
    public boolean isPropertyAnnotatedWithAll(String propertyName,
                                              List<Class<? extends Annotation>> annotations) {
        Assert.isTrue(annotations.size() > 0);
        List<Class<? extends Annotation>> collect = getPropertyAnnotations(propertyName)
                .map(Annotation::annotationType).collect(Collectors.toList());

        return !new ArrayList<>(annotations).retainAll(collect);
    }


    /**
     * Find properties annotated with all stream.
     *
     * @param annotation the annotation
     * @return the stream
     */
    @Cacheable
    public Stream<String> findPropertiesAnnotatedWithAll(List<Class<? extends Annotation>> annotation) {
        Stream<PropertyDescriptor> descriptors = getPropertyDescriptors();
        return descriptors.filter(item -> isPropertyAnnotatedWithAll(item.getName(), annotation)).map(FeatureDescriptor::getName);
    }

    /**
     * Find properties annotated with stream.
     *
     * @param annotation the annotation
     * @return the stream
     */
    @Cacheable
    public Stream<String> findPropertiesAnnotatedWith(Class<? extends Annotation> annotation) {
        return findPropertiesAnnotatedWithAll(Collections.singletonList(annotation));
    }

    /**
     * Find properties annotated with any stream.
     *
     * @param annotation the annotation
     * @return the stream
     */
    @Cacheable
    public  Stream<String> findPropertiesAnnotatedWithAny(List<Class<? extends Annotation>> annotation) {
        Stream<PropertyDescriptor> descriptors = getPropertyDescriptors();
        return descriptors.filter(item -> isPropertyAnnotatedWithAny(item.getName(), annotation)).map(PropertyDescriptor::getName);
    }
}