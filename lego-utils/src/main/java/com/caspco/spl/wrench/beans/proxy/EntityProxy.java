package com.caspco.spl.wrench.beans.proxy;

/**
 * @author : Pooya Hosseini
 * Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 * Date: 3/2/16
 * Time: 2:44 PM
 */


import com.caspco.spl.wrench.annotations.cg.EnumeratedWith;
import com.caspco.spl.wrench.annotations.cg.GeneralParameter;
import com.caspco.spl.wrench.annotations.cg.Translation;
import com.caspco.spl.wrench.annotations.cg.Translations;
import com.caspco.spl.wrench.beans.resolver.GeneralParameterResolver;
import com.caspco.spl.wrench.locale.Locale;
import com.caspco.spl.wrench.utils.report.ReportUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 06/12/14
 *         Time: 18:11
 */

public class EntityProxy {

    @Autowired(required = false)
    private GeneralParameterResolver generalParameterResolver;

    @Autowired
    private ReportUtils reportUtils;

    //    @Autowired
//    @Qualifier("GeneralParameterEntity")
//    private  BaseGeneralParameter gpEntity;
    public Object translate(Object entity, Locale locale) {
        return Enhancer.create(entity.getClass(), new TranslatorMethodInterceptor(entity, locale));
    }

    class TranslatorMethodInterceptor implements MethodInterceptor {
        private final Object parentObject;
        private final Locale locale;

        TranslatorMethodInterceptor(Object parentObject, Locale locale) {
            this.parentObject = parentObject;
            this.locale = locale;
        }


//
//        private void getGeneralParameter(String mainType,String code) throws NoSuchFieldException {
//            List all = gpService.findAll();
//            for (Object o : all) {
//                BaseGeneralParameter gp = (BaseGeneralParameter) o;
//                cache.put(gp.getMainType() + "." + gp.getCode(), gp.getDescription());
//            }
//        }

        public Object intercept(Object object, Method method, Object[] args,
                                MethodProxy methodProxy) throws Throwable {
            if (method.isAnnotationPresent(EnumeratedWith.class)) {
                EnumeratedWith annotation = method.getAnnotation(EnumeratedWith.class);
                Class<?> value = annotation.value();
//                if (cache.get(value.getName()) == null) {
//                    fillEnumTranslationsToCache(value);
//                }
                String invoke = (String) methodProxy.invoke(parentObject, args);
                Translations translations = reportUtils.fillEnumTranslationsToCache(value, invoke);
                // cache.get(value.getName() + "." + invoke, Translations.class);

                if (translations != null) {
                    for (Translation translation : translations.value()) {
                        if (translation.locale() == locale) {
                            return translation.label();
                        }
                    }
                } else {
                    return invoke;
                }
            }

            if (method.isAnnotationPresent(GeneralParameter.class)) {
                GeneralParameter annotation = method.getAnnotation(GeneralParameter.class);
//                String type = annotation.type();
                String invoke = (String) methodProxy.invoke(parentObject, args);
                return generalParameterResolver.resolve(annotation.type(), invoke);
//                Method declaredMethod = GeneralParameter.class.getDeclaredMethod("type");
//                String targetField = declaredMethod.getAnnotation(TargetField.class).value();
//                CacheUtils.getCacheItem(invoke)
//                Class<?> clazz = classMap.get(GeneralParameter.class);
//                String key = CacheKeyResolver.resolve(clazz, annotation, invoke);
//                return cache.get(key, String.class);
            }
            return methodProxy.invoke(parentObject, args);
        }
    }
}