package com.caspco.spl.wrench.beans.transformer;

import com.caspco.spl.wrench.exceptions.CanNotTransformException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BasicTransformer {

    @Autowired
    private List<SimpleBeanTransformer> transformers;

    private Map<String, SimpleBeanTransformer> simpleBeanTransformerMap = new HashMap<>();

    @PostConstruct
    public void init() {
        for (SimpleBeanTransformer transformer : transformers) {
            String s = transformer.getOutboundTypeArgument().getName() + transformer.getInboundTypeArgument().getName();
            simpleBeanTransformerMap.put(s, transformer);
        }
    }

    public <E, T> List<T> convert(List<E> o, Class<T> resultClass) {
        List<T> ts = new ArrayList<>();
        for (E e : o) {
            T convert = convert(e, resultClass);
            ts.add(convert);
        }
        return ts;
    }

    public <E, T> T convert(E o, Class<T> resultClass) {
        String s = resultClass.getName() + o.getClass().getName();

        SimpleBeanTransformer transformer = simpleBeanTransformerMap.get(s);
        if (transformer == null) {
            throw new CanNotTransformException("Can not transform type " + o.getClass().getName() + " to " + resultClass.getName());
        }
        return (T) transformer.convert(o);
    }
}