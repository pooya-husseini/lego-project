package com.caspco.spl.wrench.beans.rest;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/8/15
 *         Time: 11:02 AM
 */

import com.caspco.spl.wrench.beans.service.SimpleService;
import com.caspco.spl.wrench.beans.transformer.SimpleBeanTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@Transactional
public abstract class BasicRestService<D, E,R extends SimpleService> extends BasicReadOnlyRestService<D,E,R>{

    private final R service;
    private int pageSize = 10;

    @Autowired
    private SimpleBeanTransformer<D, E> dTransformer;
    @Autowired
    private SimpleBeanTransformer<E, D> eTransformer;

    public BasicRestService(R service) {
        super(service);
        this.service = service;
    }

    //creation services
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public D add(@Valid @RequestBody D dto) {
        return  eTransformer.convert((E) service.save(dTransformer.convert(dto)));
    }

    // updating services
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public D update(@PathVariable Long id, @Valid @RequestBody D dto) {

        if (service.exists(id)) {
            E entity = (E) service.save(dTransformer.convert(dto));
            return eTransformer.convert(entity);
        } else {
            throw new EntityNotFoundException();
        }
    }

    // deleting services
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable Long id) {
        service.removeById(id);
    }

}