package com.caspco.spl.wrench.spring.configuration;

import com.caspco.spl.wrench.beans.transformer.BasicTransformer;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.PBEStringEncryptor;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.hibernate4.encryptor.HibernatePBEStringEncryptor;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/16/15
 *         Time: 6:46 PM
 */

@Configuration
@ComponentScan(basePackages = "com.caspco.spl.wrench")
@EntityScan(basePackages = {"com.caspco.spl.wrench"})
public class WrenchConfiguration {

    @Bean
    public HibernatePBEStringEncryptor stringEncryptor() {
        HibernatePBEStringEncryptor stringEncryptor = new HibernatePBEStringEncryptor();
        stringEncryptor.setRegisteredName("stringEncryptor");
        stringEncryptor.setEncryptor(encryptor());
        return stringEncryptor;
    }

    @Bean
    public PBEStringEncryptor encryptor() {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setProvider(new BouncyCastleProvider());
        encryptor.setAlgorithm("PBEWITHSHA256AND128BITAES-CBC-BC");
        encryptor.setPassword("That which does not kill us makes us stronger.");
        return encryptor;
    }

    @Bean
    public BasicTransformer basicTransformer() {
        return new BasicTransformer();
    }


}
