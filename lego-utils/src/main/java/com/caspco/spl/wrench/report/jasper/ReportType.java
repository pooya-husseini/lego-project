package com.caspco.spl.wrench.report.jasper;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 06/12/14
 *         Time: 11:04
 */
public enum ReportType {
    PDF,DOC,EXCEL,XML,CSV,HTML
}
