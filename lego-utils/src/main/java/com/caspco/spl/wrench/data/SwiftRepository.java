package com.caspco.spl.wrench.data;

import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.types.Predicate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/7/15
 *         Time: 2:03 PM
 */

@NoRepositoryBean
public interface SwiftRepository<T, ID extends Serializable> extends QueryDslPredicateExecutor<T>, JpaRepository<T, ID> {
    JPQLQuery select();

    int delete(Predicate predicate) throws InterruptedException;

    T getEntityAtVersion(Number revision);

    Number getLastRevisionNumber();

    Number getLastDeletedRevisionNumber();

    Number getLastModifiedRevisionNumber();

    Number getLastInsertedRevisionNumber();

    List<HistoricalEntity> getRevisionNumberModificationType(Number revision);

    List<T> find(T e);

    boolean exists(T entity);

    long count(T entity);

    List<String> searchColumn(String column, String value);
}