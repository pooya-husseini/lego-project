package com.caspco.spl.wrench.locale;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/9/15
 *         Time: 11:26 AM
 */

@Service
public class LocaleService {


    public Locale findLocale(String langTag, String country) {
        return findLocale(Locale.formatLocale(langTag, country));
    }

    @Cacheable
    public Locale findLocale(String value) {
        Locale[] values = Locale.values();
        return Arrays.stream(values).filter(locale -> locale.getLocale().equals(value)).findFirst().get();
    }
}
