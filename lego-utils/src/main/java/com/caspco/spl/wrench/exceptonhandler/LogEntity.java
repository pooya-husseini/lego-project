/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.caspco.spl.wrench.exceptonhandler;

import com.caspco.spl.wrench.annotations.cg.Ignore;
import com.caspco.spl.wrench.annotations.data.FillWithCurrentDate;
import com.caspco.spl.wrench.annotations.data.HasEvents;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name="LogErrors")
@Ignore
@HasEvents
public class LogEntity {

    private Long id;
    private String exceptionMessage;

    private Date time;

    @Lob
    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @FillWithCurrentDate
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}