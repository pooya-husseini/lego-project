package com.caspco.spl.wrench.beans.resolver;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 3/2/16
 *         Time: 3:00 PM
 */
public interface GeneralParameterResolver {
    String resolve(String mainType, String code);
}
