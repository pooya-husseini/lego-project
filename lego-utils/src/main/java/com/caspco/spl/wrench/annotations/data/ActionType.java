package com.caspco.spl.wrench.annotations.data;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 15/11/14
 *         Time: 18:59
 */
public enum ActionType {
    INSERT,UPDATE,DELETE,NoOP
}
