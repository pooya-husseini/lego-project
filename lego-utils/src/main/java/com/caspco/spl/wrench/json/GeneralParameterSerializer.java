package com.caspco.spl.wrench.json;


import com.caspco.spl.wrench.annotations.data.GeneralParameterFormatter;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;

import java.io.IOException;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 16/09/14
 *         Time: 18:44
 */
public class GeneralParameterSerializer extends JsonSerializer<String> implements ContextualSerializer {

    private String mainType;

    public GeneralParameterSerializer(String mainType) {
        this.mainType = mainType;
    }

    public GeneralParameterSerializer() {
        mainType = null;
    }

    @Override
    public void serialize(String data, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(mainType+data);
    }

    @Override
    public Class<String> handledType() {
        return String.class;
    }

    @Override
    public void serializeWithType(String value, JsonGenerator jgen, SerializerProvider provider, TypeSerializer typeSer) throws IOException, JsonProcessingException {
        super.serializeWithType(value, jgen, provider, typeSer);
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) throws JsonMappingException {
        GeneralParameterFormatter ann = property.getAnnotation(GeneralParameterFormatter.class);
        if (ann == null) {
            ann = property.getContextAnnotation(GeneralParameterFormatter.class);
        }
        if(ann==null) {
            return new StringSerializer();
        }
        return new GeneralParameterSerializer(ann.type());
    }
}