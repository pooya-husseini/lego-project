package com.caspco.spl.wrench.utils.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/24/15
 *         Time: 8:37 PM
 */
public class EnumUtils {

    private EnumUtils() {
    }

    public static <E extends Enum<E>> List<Value> convertEnumToList(Enum<E>[] anEnum) {
        return Arrays.stream(anEnum).map(item -> new Value(item.name())).collect(Collectors.toList());
    }

    public static class Value {
        private String value;

        public Value() {
        }

        public Value(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}