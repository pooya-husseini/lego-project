package com.caspco.spl.wrench.locale;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya_hfp.ir
 *         Date: 24/09/14
 *         Time: 13:58
 */
public enum Locale {

    Arabic_United_Arab_Emirates("ar", "AE"),
    Arabic_Jordan("ar", "JO"),
    Arabic_Syria("ar", "SY"),
    Croatian_Croatia("hr", "HR"),
    French_Belgium("fr", "BE"),
    Spanish_Panama("es", "PA"),
    Maltese_Malta("mt", "MT"),
    Spanish_Venezuela("es", "VE"),
    Bulgarian("bg", ""),
    Chinese_Taiwan("zh", "TW"),
    Italian("it", ""),
    Korean("ko", ""),
    Ukrainian("uk", ""),
    Latvian("lv", ""),
    Danish_Denmark("da", "DK"),
    Spanish_Puerto_Rico("es", "PR"),
    Vietnamese_Vietnam("vi", "VN"),
    English_United_States("en", "US"),
    Serbian_Montenegro("sr", "ME"),
    Swedish_Sweden("sv", "SE"),
    Spanish_Bolivia("es", "BO"),
    English_Singapore("en", "SG"),
    Arabic_Bahrain("ar", "BH"),
    Portuguese("pt", ""),
    Arabic_Saudi_Arabia("ar", "SA"),
    Slovak("sk", ""),
    Arabic_Yemen("ar", "YE"),
    Hindi_India("hi", "IN"),
    Irish("ga", ""),
    English_Malta("en", "MT"),
    Finnish_Finland("fi", "FI"),
    Estonian("et", ""),
    Swedish("sv", ""),
    Czech("cs", ""),
    Greek("el", ""),
    Ukrainian_Ukraine("uk", "UA"),
    Hungarian("hu", ""),
    French_Switzerland("fr", "CH"),
    Indonesian("in", ""),
    Spanish_Argentina("es", "AR"),
    Arabic_Egypt("ar", "EG"),
    Spanish_El_Salvador("es", "SV"),
    Portuguese_Brazil("pt", "BR"),
    Belarusian("be", ""),
    Icelandic_Iceland("is", "IS"),
    Czech_Czech_Republic("cs", "CZ"),
    Spanish("es", ""),
    Polish_Poland("pl", "PL"),
    Turkish("tr", ""),
    Catalan_Spain("ca", "ES"),
    Serbian_Serbia_and_Montenegro("sr", "CS"),
    Malay_Malaysia("ms", "MY"),
    Croatian("hr", ""),
    Lithuanian("lt", ""),
    Spanish_Spain("es", "ES"),
    Spanish_Colombia("es", "CO"),
    Bulgarian_Bulgaria("bg", "BG"),
    Albanian("sq", ""),
    French("fr", ""),
    Japanese("ja", ""),
    Serbian_Bosnia_and_Herzegovina("sr", "BA"),
    Icelandic("is", ""),
    Spanish_Paraguay("es", "PY"),
    German("de", ""),
    Spanish_Ecuador("es", "EC"),
    Spanish_United_States("es", "US"),
    Arabic_Sudan("ar", "SD"),
    English("en", ""),
    Romanian_Romania("ro", "RO"),
    English_Philippines("en", "PH"),
    Catalan("ca", ""),
    Arabic_Tunisia("ar", "TN"),
    Spanish_Guatemala("es", "GT"),
    Slovenian("sl", ""),
    Korean_South_Korea("ko", "KR"),
    Greek_Cyprus("el", "CY"),
    Spanish_Mexico("es", "MX"),
    Russian_Russia("ru", "RU"),
    Spanish_Honduras("es", "HN"),
    Chinese_Hong_Kong("zh", "HK"),
    Hungarian_Hungary("hu", "HU"),
    Thai_Thailand("th", "TH"),
    Arabic_Iraq("ar", "IQ"),
    Spanish_Chile("es", "CL"),
    Finnish("fi", ""),
    Arabic_Morocco("ar", "MA"),
    Irish_Ireland("ga", "IE"),
    Macedonian("mk", ""),
    Turkish_Turkey("tr", "TR"),
    Estonian_Estonia("et", "EE"),
    Arabic_Qatar("ar", "QA"),

    Portuguese_Portugal("pt", "PT"),
    French_Luxembourg("fr", "LU"),
    Arabic_Oman("ar", "OM"),
    Thai("th", ""),
    Albanian_Albania("sq", "AL"),
    Spanish_Dominican_Republic("es", "DO"),
    Spanish_Cuba("es", "CU"),
    Arabic("ar", ""),
    Russian("ru", ""),
    English_New_Zealand("en", "NZ"),
    Serbian_Serbia("sr", "RS"),
    German_Switzerland("de", "CH"),
    Spanish_Uruguay("es", "UY"),
    Malay("ms", ""),
    Greek_Greece("el", "GR"),
    Hebrew_Israel("iw", "IL"),
    English_South_Africa("en", "ZA"),
    Hindi("hi", ""),
    French_France("fr", "FR"),
    German_Austria("de", "AT"),
    Dutch("nl", ""),
    Norwegian_Norway("no", "NO"),
    English_Australia("en", "AU"),
    Vietnamese("vi", ""),
    Dutch_Netherlands("nl", "NL"),
    French_Canada("fr", "CA"),
    Latvian_Latvia("lv", "LV"),
    German_Luxembourg("de", "LU"),
    Spanish_Costa_Rica("es", "CR"),
    Arabic_Kuwait("ar", "KW"),
    Serbian("sr", ""),
    Arabic_Libya("ar", "LY"),
    Maltese("mt", ""),
    Italian_Switzerland("it", "CH"),
    Danish("da", ""),
    German_Germany("de", "DE"),
    Arabic_Algeria("ar", "DZ"),
    Slovak_Slovakia("sk", "SK"),
    Lithuanian_Lithuania("lt", "LT"),
    Italian_Italy("it", "IT"),
    English_Ireland("en", "IE"),
    Chinese_Singapore("zh", "SG"),
    Romanian("ro", ""),
    English_Canada("en", "CA"),
    Dutch_Belgium("nl", "BE"),
    Norwegian("no", ""),
    Polish("pl", ""),
    Chinese_China("zh", "CN"),
    Japanese_Japan("ja", "JP"),
    German_Greece("de", "GR"),
    Hebrew("iw", ""),
    English_India("en", "IN"),
    Arabic_Lebanon("ar", "LB"),
    Spanish_Nicaragua("es", "NI"),
    Chinese("zh", ""),
    Macedonian_Macedonia("mk", "MK"),
    Belarusian_Belarus("be", "BY"),
    Slovenian_Slovenia("sl", "SI"),
    Spanish_Peru("es", "PE"),
    Indonesian_Indonesia("in", "ID"),
    English_United_Kingdom("en", "GB"),
    Persian_Iran("fa", "IR");


    final static Map<String, Locale> cache = new HashMap<>();
    private final String langTag;
    private final String country;

    Locale(String langTag, String country) {
        this.langTag = langTag;
        this.country = country;
    }

    static String formatLocale(String langTag, String country) {
        return String.format("%s_%s", langTag, country);
    }

    public java.util.Locale getJavaLocale() {
        return new java.util.Locale(langTag, country);
    }

    public String getLocale() {
        return formatLocale(langTag, country);
    }
}