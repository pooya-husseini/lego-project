package com.caspco.spl.wrench.beans.transformer;

import org.springframework.core.GenericTypeResolver;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 30/04/14
 *         Time: 18:28
 */
public abstract class SimpleBeanTransformer<S, D> {

    private Class<S> inboundTypeArgument = (Class<S>) GenericTypeResolver.resolveTypeArguments(this.getClass(), SimpleBeanTransformer.class)[0];
    private Class<D> outboundTypeArgument = (Class<D>) GenericTypeResolver.resolveTypeArguments(this.getClass(), SimpleBeanTransformer.class)[1];

    public abstract D convert(S source, Object parent);

    public D convert(S source) {
        return convert(source, null);
    }

    public List<D> convert(List<S> sList) {
        return convert(sList, null);
    }

    public List<D> convert(List<S> sList, Object parent) {
        if (sList == null) {
            return null;
        }
        return sList.stream().map(s -> convert(s, parent)).collect(Collectors.toList());
    }

    public Class<S> getInboundTypeArgument() {
        return inboundTypeArgument;
    }

    public Class<D> getOutboundTypeArgument() {
        return outboundTypeArgument;
    }
}