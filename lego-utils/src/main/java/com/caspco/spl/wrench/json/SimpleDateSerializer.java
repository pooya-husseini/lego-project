package com.caspco.spl.wrench.json;


import com.caspco.spl.wrench.annotations.data.DateFormatter;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 16/09/14
 *         Time: 18:44
 */
public class SimpleDateSerializer extends JsonSerializer<Date> implements ContextualSerializer {
    private final SimpleDateFormat dateFormat;
    public SimpleDateSerializer(String s) {
        this.dateFormat = new SimpleDateFormat(s);

    }

    public SimpleDateSerializer() {
        dateFormat = new SimpleDateFormat("MM-yyyy-dd");
    }

    @Override
    public void serialize(Date date, JsonGenerator gen, SerializerProvider provider) throws IOException {
        String formattedDate = dateFormat.format(date);
        gen.writeString(formattedDate);
    }

    @Override
    public Class<Date> handledType() {
        return Date.class;
    }

    @Override
    public JsonSerializer<Date> createContextual(SerializerProvider prov, BeanProperty property) throws
            JsonMappingException {

        DateFormatter ann = property.getAnnotation(DateFormatter.class);
        if (ann == null) {
            ann = property.getContextAnnotation(DateFormatter.class);
        }

        String format = "yyyy-MM-dd";
        if(ann==null) {
            return new DateSerializer();
        }
        return new SimpleDateSerializer(format);
    }
}