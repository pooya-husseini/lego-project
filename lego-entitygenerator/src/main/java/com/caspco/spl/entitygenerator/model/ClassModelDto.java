package com.caspco.spl.entitygenerator.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 1/6/16
 *         Time: 1:51 PM
 */
public class ClassModelDto {
    private Map<String, String> properties = new HashMap<>();
    private List<ClassFieldDto> fields;


    public ClassModelDto() {
    }

    public ClassModelDto(List<ClassFieldDto> fields) {
        this.fields = fields;
    }

    public List<ClassFieldDto> getFields() {
        return fields;
    }

    public void setFields(List<ClassFieldDto> fields) {
        this.fields = fields;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public void setProperty(String key, String value) {
        this.properties.put(key, value);
    }
}