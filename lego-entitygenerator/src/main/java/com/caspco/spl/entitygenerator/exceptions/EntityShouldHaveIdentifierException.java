package com.caspco.spl.entitygenerator.exceptions;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 12/28/15
 *         Time: 7:09 PM
 */
public class EntityShouldHaveIdentifierException extends RuntimeException {

    public EntityShouldHaveIdentifierException() {
        super();
    }

    public EntityShouldHaveIdentifierException(String message) {
        super(message);
    }

    public EntityShouldHaveIdentifierException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityShouldHaveIdentifierException(Throwable cause) {
        super(cause);
    }

    protected EntityShouldHaveIdentifierException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
