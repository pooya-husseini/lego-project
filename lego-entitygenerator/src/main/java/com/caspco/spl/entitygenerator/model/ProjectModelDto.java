package com.caspco.spl.entitygenerator.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 1/6/16
 *         Time: 1:51 PM
 */
public class ProjectModelDto {
    private Map<String, String> properties = new HashMap<>();
    private List<ClassModelDto> classes;


    public ProjectModelDto() {
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public List<ClassModelDto> getClasses() {
        return classes;
    }

    public void setClasses(List<ClassModelDto> classes) {
        this.classes = classes;
    }
}