package com.caspco.spl.entitygenerator.model;

import java.util.Map;

public class ClassFieldDto {
    private String type;
    private Map<String, String> properties;

    public ClassFieldDto() {
    }

    public ClassFieldDto(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
}