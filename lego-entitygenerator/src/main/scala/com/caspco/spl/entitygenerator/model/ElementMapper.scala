package com.caspco.spl.entitygenerator.model

import com.caspco.spl.cg.model.datamodel._

/**
  * @author : Pooya Hosseini
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: 12/31/15
  *         Time: 1:35 PM
  */
object ElementMapper {
  lazy val GpPattern = "^generalparamter\\(\"(\\w+)\"\\)$".r

  def apply(elemType: String): Option[AbstractElement] = Option(elemType match {
    case "integer" => new IntegerType
    case "boolean" => new BooleanType
    case "list" => new ListType
    case "map" => new MapType
    case "composition" => new CompositionType(Map.empty)
    case "timestamp" => new DateTimeType(Map("type" -> "timestamp", "autofilldate" -> "true"))
    //    case "datamodel" => new DataModelType(None,Map.empty[String, String])
    case "datetime" => new DateTimeType(Map("type" -> "datetime"))
    case "time" => new DateTimeType(Map("type" -> "time"))
    case "date" => new DateTimeType(Map("type" -> "date"))
    case "currency" => new CurrencyType
    case "decimal" => new DecimalType
    case "file" => new FileType(Map("fileId" -> "true"))
    case "double" => new DoubleType
    case "long" => new LongType
    case "string" => new StringType(Map.empty[String, String])
    case GpPattern(t) =>
      new StringType(Map("generalParameter" -> t))
    case _ => null
  })
}