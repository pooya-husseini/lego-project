package com.caspco.spl.entitygenerator.model

import com.caspco.spl.cg.model.datamodel.DataModel

import scala.collection.JavaConverters._

/**
  * @author : Pooya Hosseini
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: 1/2/16
  *         Time: 4:49 PM
  */
class EntityDataModel() extends DataModel(false) {
  lazy val getTimeStampElements = filterElements(_.containsAttribute("autofilldate"))
  lazy val getTimeStampElementsAsJava = getTimeStampElements.asJava
}
