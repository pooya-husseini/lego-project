package com.caspco.spl.entitygenerator.xml.vpsuite

import java.io.InputStream
import java.util

import com.caspco.spl.cg.util.io.FileOutputStream
import com.caspco.spl.entitygenerator.model.{ClassModelDto, ProjectModelDto}

import scala.collection.JavaConverters._

/**
  * @author : Pooya Hosseini
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: 12/28/15
  *         Time: 8:18 PM
  */
object VpSuiteModelProjectMaker {

  def getClassesAsJava(is: InputStream): util.List[ClassModelDto] = {
    getClasses(is).asJava
  }

  def getClasses(is: InputStream): List[ClassModelDto] = {
    XmlParser.getModel(is)
  }

  def doMakeProject(projectModel: ProjectModelDto): Set[String] = {

    implicit val formatOutput = true
    val model: Map[String, String] = XmlParser.generateModel(projectModel.getClasses.asScala.toList)

    val artifactId = projectModel.getProperties.get("artifactId")
    val groupId = projectModel.getProperties.get("groupId")

    val destinationPath = System.getProperty("java.io.tmpdir") + "/" + groupId + "/" + artifactId + "/project/src/main/java/"

    model.foreach(x => {
      val dest = destinationPath + x._1.replace(".", "/") + "Entity.java"
      new FileOutputStream(dest) <# x._2
    })

    PackageNames(model.keys)
  }

  object PackageNames {
    def apply(classNames: Iterable[String]): Set[String] = {
      classNames.map(x => x.substring(0, x.lastIndexOf("."))).toSet
    }
  }
}