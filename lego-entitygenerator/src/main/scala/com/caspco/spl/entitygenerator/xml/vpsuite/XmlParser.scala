package com.caspco.spl.entitygenerator.xml.vpsuite

import java.io.InputStream

import com.caspco.spl.cg.model.datamodel._
import com.caspco.spl.cg.util.enums.DynamicClassManipulation.MakeClass
import com.caspco.spl.cg.util.template.TemplateEngine
import com.caspco.spl.entitygenerator.exceptions.{EntityShouldHaveIdentifierException, PackageShouldHaveNameException}
import com.caspco.spl.entitygenerator.model._

import scala.collection.JavaConverters._
import scala.xml.{NodeSeq, XML}

object XmlParser {

  def getModel(is: InputStream): List[ClassModelDto] = {
    val xml = XML.load(is)
    val models: NodeSeq = xml \ "Models"
    val relations: NodeSeq = models \ "ModelRelationshipContainer" \ "ModelChildren" \ "ModelRelationshipContainer" \ "ModelChildren"

    val associations: List[(String, String)] = (relations \ "Association").map(asc => {
      ((asc \ "FromEnd" \ "AssociationEnd").map(fe => {
        fe.attribute("EndModelElement").get.text
      }).head
        ,
        (asc \ "ToEnd" \ "AssociationEnd").map(fe => {
          fe.attribute("EndModelElement").get.text
        }).head)
    }).toList

    val dependencies: List[(String, String)] = ((relations \ "Dependency") ++ (models \ "Dependency")).map(dep => {
      (dep.attribute("From").get.text, dep.attribute("To").get.text)
    }).toList

    val modelsMap = (models \ "Package").flatMap(p => {
      val packageName = p.attribute("Name") match {
        case Some(pName) => pName.text
        case None => throw new PackageShouldHaveNameException()
      }

      val classes: NodeSeq = p \ "ModelChildren" \ "Class"
      val entityClasses = classes.filter(x => (x \ "Stereotypes").isEmpty)
      val stereotypedClasses = classes.filter(x => (x \ "Stereotypes").nonEmpty)

      val entityModels = entityClasses.map(x => {
        val model = new EntityDataModel()
        val atts = x.attributes

        val rawClassName = atts.get("Name") match {
          case Some(n) => n.text
          case None => ""
        }

        val className = rawClassName match {
          case y if y.endsWith("Entity") => y.substring(0, y.length - 6)
          case n => n
        }

        model +=("name", className)
        val id = atts.get("Id").get.text

        model +=("domain", packageName)

        (x \ "ModelChildren" \ "Attribute").foreach(c => {
          val attType = c.attribute("Type") match {
            case Some(t) => t.text
            case None => (c \ "Type" \ "Class").head.attribute("Name").get.text
          }

          val element = ElementMapper(attType.toLowerCase) match {
            case Some(e) =>
              e
            case None =>
              throw new ClassNotFoundException(attType.toLowerCase)
          }

          val text: String = c.attribute("Name").get.text
          element +=("name", text)
          if (text.equals("id")) {
            element +=("identifier", "true")
          } else {
            element match {
              case FileType(_) =>
              case _ =>
                element +=("searchable", "true")
            }
          }
          model += element
        })
        if (model.filterElements(x => x ?? "identifier").isEmpty) {
          throw new EntityShouldHaveIdentifierException(model.getUniqueName)
        }
        (id, model)
      })

      val enumModels = stereotypedClasses.map(x => {
        val model = new EnumModel()
        val atts = x.attributes

        val id = atts.get("Id").get.text
        val rawClassName = atts.get("Name") match {
          case Some(n) => n.text
          case None => ""
        }

        model +=("name", rawClassName)
        model +=("domain", packageName)

        (x \ "ModelChildren" \ "EnumerationLiteral").foreach(l => {
          val name = l.attribute("Name").get.text
          model += CompositionType(Map("name" -> name))
        })
        (id, model)
      })
      entityModels ++ enumModels
    }).toMap

    associations.foreach((a) => {
      val from: DataModel = modelsMap(a._1)
      val to: DataModel = modelsMap(a._2)
      from += ListType(Map("relation" -> "OneToMany", "name" -> to.getAttribute("name").toLowerCase, "typename" -> (to.getUniqueName + "Entity"), "plaintypename" -> (to.getAttribute("name") + "Entity")))
      from.addRelatedDataModel(to)
    })

    fillDependencies(dependencies, modelsMap)
    modelsMap.values.map {
      case x: EnumModel =>
        val dto = new ClassModelDto()
        val fields = x.getElements.map(el => {
          val field = new ClassFieldDto(el.getElemType)
          field.setProperties(el.getAttributes().asJava)
          field
        })
        dto.setProperties((x.getAttributes() ++ Map("enumerated" -> "true")).asJava)
        dto.setFields(fields.asJava)
        dto

      case x: EntityDataModel =>
        val dto = new ClassModelDto()

        val fields = x.getElements.map(el => {
          val field = new ClassFieldDto(el.getElemType)
          field.setProperties(el.getAttributes().asJava)
          field
        })
        dto.setFields(fields.asJava)
        dto.setProperties(x.getAttributes().asJava)
        dto
    }.toList
  }

  def fillDependencies(dependencies: List[(String, String)], modelsMap: Map[String, DataModel]): Unit = {
    dependencies.foreach(dep => {
      val from = modelsMap(dep._1)
      val to = modelsMap(dep._2)

      to match {
        case x: EntityDataModel =>
          from += CompositionType(Map("relation" -> "OneToOne", "name" -> to.getAttribute("name").toLowerCase, "typename" -> (to.getUniqueName + "Entity"), "plaintypename" -> (to.getAttribute("name") + "Entity")))
        case x: EnumModel =>
          from += StringType(Map("enumeratedWith" -> (to.getUniqueName + "Entity"), "name" -> to.getAttribute("name").toLowerCase))
      }

      from.addRelatedDataModel(to)
    })
  }

  def getModel_(is: InputStream): List[ClassModelDto] = {
    Nil
    //    val xml = XML.load(is)
    //    val models: NodeSeq = xml \ "Models"
    //
    //    (models \ "Package").flatMap(p => {
    //      val packageName = p.attribute("Name") match {
    //        case Some(pName) => pName.text
    //        case None => throw new PackageShouldHaveNameException()
    //      }
    //      (p \ "ModelChildren" \ "Class").map(clazz => {
    //        val atts = clazz.attributes
    //        val rawClassName = atts.get("Name") match {
    //          case Some(n) => n.text
    //          case None => ""
    //        }
    //
    //        val className = rawClassName match {
    //          case y if y.endsWith("Entity") => y.substring(0, y.length - 6)
    //          case n => n
    //        }
    //
    //        val fields = (clazz \ "ModelChildren" \ "Attribute").map(c => {
    //          val attType = c.attribute("Type") match {
    //            case Some(t) => t.text
    //            case None => (c \ "Type" \ "Class").head.attribute("Name").get.text
    //          }
    //
    //          new ClassFieldDto(attType,c.attribute("Name").get.text)
    //
    //        }).toList
    //
    //        new ClassModelDto(packageName, className, fields.asJava)
    //      })
    //    }).toList

  }

  def generateModel(models: List[ClassModelDto]): Map[String, String] = {

    //    val xml = XML.loadFile(path)
    //    val models: NodeSeq = xml \ "Models"
    //    val relations: NodeSeq = models \ "ModelRelationshipContainer" \ "ModelChildren" \ "ModelRelationshipContainer" \ "ModelChildren"
    //
    //    val associations: List[(String, String)] = (relations \ "Association").map(asc => {
    //      ((asc \ "FromEnd" \ "AssociationEnd").map(fe => {
    //        fe.attribute("EndModelElement").get.text
    //      }).head
    //        ,
    //        (asc \ "ToEnd" \ "AssociationEnd").map(fe => {
    //          fe.attribute("EndModelElement").get.text
    //        }).head)
    //    }).toList
    //
    //    val dependencies: List[(String, String)] = ((relations \ "Dependency") ++ (models \ "Dependency")).map(dep => {
    //      (dep.attribute("From").get.text, dep.attribute("To").get.text)
    //    }).toList
    //
    //    val modelsMap = (models \ "Package").flatMap(p => {
    //      val packageName = p.attribute("Name") match {
    //        case Some(pName) => pName.text
    //        case None => throw new PackageShouldHaveNameException()
    //      }
    //
    //      val classes: NodeSeq = p \ "ModelChildren" \ "Class"
    //      val entityClasses = classes.filter(x => (x \ "Stereotypes").isEmpty)
    //      val stereotypedClasses = classes.filter(x => (x \ "Stereotypes").nonEmpty)
    //
    //      val entityModels = entityClasses.map(x => {
    //        val model = new EntityDataModel(false)
    //        val atts = x.attributes
    //
    //        val rawClassName = atts.get("Name") match {
    //          case Some(n) => n.text
    //          case None => ""
    //        }
    //
    //        val className = rawClassName match {
    //          case y if y.endsWith("Entity") => y.substring(0, y.length - 6)
    //          case n => n
    //        }
    //
    //        model +=("name", className)
    //        val id = atts.get("Id").get.text
    //
    //        model +=("domain", packageName)
    //
    //        (x \ "ModelChildren" \ "Attribute").foreach(c => {
    //          val attType = c.attribute("Type") match {
    //            case Some(t) => t.text
    //            case None => (c \ "Type" \ "Class").head.attribute("Name").get.text
    //          }
    //
    //          val element = ElementMapper(attType.toLowerCase) match {
    //            case Some(e) =>
    //              e
    //            case None =>
    //              throw new ClassNotFoundException(attType.toLowerCase)
    //          }
    //
    //          val text: String = c.attribute("Name").get.text
    //          element +=("name", text)
    //          if (text.equals("id")) {
    //            element +=("identifier", "true")
    //          } else {
    //            element match {
    //              case FileType(_) =>
    //              case _ =>
    //                element +=("searchable", "true")
    //            }
    //
    //          }
    //          model += element
    //        })
    //        if (model.filterElements(x => x ?? "identifier").isEmpty) {
    //          throw new EntityShouldHaveIdentifierException(model.getUniqueName)
    //        }
    //        (id, model)
    //      })
    //
    //      val enumModels = stereotypedClasses.map(x => {
    //        val model = new EnumModel()
    //        val atts = x.attributes
    //
    //        val id = atts.get("Id").get.text
    //        val rawClassName = atts.get("Name") match {
    //          case Some(n) => n.text
    //          case None => ""
    //        }
    //
    //        model +=("name", rawClassName)
    //        model +=("domain", packageName)
    //
    //        (x \ "ModelChildren" \ "EnumerationLiteral").foreach(l => {
    //          val name = l.attribute("Name").get.text
    //          model += CompositionType(Map("name" -> name))
    //        })
    //        (id, model)
    //      })
    //      entityModels ++ enumModels
    //    }).toMap
    //
    //    associations.foreach((a) => {
    //      val from: DataModel = modelsMap(a._1)
    //      val to: DataModel = modelsMap(a._2)
    //      from += ListType(Map("relation" -> "OneToMany", "name" -> to.getAttribute("name").toLowerCase, "typename" -> (to.getUniqueName + "Entity"), "plaintypename" -> (to.getAttribute("name") + "Entity")))
    //      from.addRelatedDataModel(to)
    //    })
    //
    //    fillDependencies(dependencies, modelsMap)

    val convertedModels = models.map {
      case x =>
        val model: DataModel = if (x.getProperties.containsKey("enumerated")) {
          val name: String = x.getProperties.get("domain") + "." + x.getProperties.get("name") + "Entity"
          MakeClass(name).makeClass()
          new EnumModel()
        } else {
          new EntityDataModel()
        }

        x.getProperties.asScala.foreach(model += _)

        x.getFields.asScala.foreach {
          case f =>
            val element = ElementMapper(f.getType.toLowerCase) match {
              case Some(e) =>
                e
              case None =>
                throw new ClassNotFoundException(f.getType.toLowerCase)
            }
            f.getProperties.asScala.foreach(element += _)
            model += element
        }
        model

      //
      //
      //        val fields=x.getElements.map(el => {
      //          val field=new ClassFieldDto(el.getElemType, el.getUniqueName)
      //          field.setProperties(el.getAttributes().asJava)
      //          field
      //        })
      //        dto.setProperties((x.getAttributes() ++ Map("enumerated" -> "true")).asJava)
      //        dto.setFields(fields.asJava)
      //        dto

    }



    convertedModels.map {
      case x: EnumModel =>
        val enumText: String = TemplateEngine("/templates/java/enum.vm", Map("dm" -> new EnumClass(x)))

        (x.getUniqueName, enumText)
      case x: EntityDataModel =>
        (x.getUniqueName, TemplateEngine("/templates/java/entity.vm", Map("dm" -> new EntityClass(x))))
    }.toMap

  }
}