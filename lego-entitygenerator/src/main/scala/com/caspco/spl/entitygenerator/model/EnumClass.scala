package com.caspco.spl.entitygenerator.model

import java.lang.annotation.Annotation

import com.caspco.spl.cg.model.BaseModel
import com.caspco.spl.cg.model.datamodel._

import scala.collection.JavaConverters._

/**
  * @author : Pooya Hosseini
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: 12/28/15
  *         Time: 7:50 PM
  */
class EnumClass(dm: DataModel) extends BaseModel {

  lazy val fieldsAsJava={
    dm.getElements.map(x=>{
      val field=EnumField()
      field+=("name",x("name"))
      field
    }).asJava
  }

  def getPackageName: String = dm.getAttribute("domain")

  def getName=dm("name")
}

case class EnumField(annotations: List[String] = List.empty[String]) extends AbstractElement("EnumField") {

  def mkString: String = {
    toString
  }

  def getFieldName: String = getAttributes().getOrElse("name", "")

  def getAnnotationAsJava = annotations.asJavaCollection

  def isEnumerated = true

  def getAnnotationsAsJava = getAnnotations.asJava

  def getAnnotations[A <: Annotation]: List[_] = getAttributes().toList.map({
    case x => ""
  })
}
