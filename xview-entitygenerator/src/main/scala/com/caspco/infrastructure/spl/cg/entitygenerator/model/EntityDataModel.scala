package com.caspco.infrastructure.spl.cg.entitygenerator.model

import com.caspco.infrastructure.xview.cg.model.datamodel.DataModel

/**
  * @author : Pooya Hosseini
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: 1/2/16
  *         Time: 4:49 PM
  */
class EntityDataModel(collection: Boolean) extends DataModel(collection){

}
