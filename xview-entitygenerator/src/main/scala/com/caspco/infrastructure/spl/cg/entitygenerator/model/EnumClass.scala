package com.caspco.infrastructure.spl.cg.entitygenerator.model

import java.lang.annotation.Annotation

import com.caspco.infrastructure.xview.cg.model.BaseModel
import com.caspco.infrastructure.xview.cg.model.datamodel._
import com.caspco.xview.wrench.annotations.proxy.CgAnnotationMaker
import com.caspco.xview.wrench.locale.Locale

import scala.collection.JavaConverters._

/**
  * @author : Pooya Hosseini
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: 12/28/15
  *         Time: 7:50 PM
  */
class EntityClass(dm: DataModel) extends BaseModel {

  lazy val fields: List[EntityField] = {
    dm.getElements.map(elem => convertElement(elem))
  }

  lazy val notGeneratedValueFields: List[EntityField] = {
    fields.filterNot(_.containsAttribute("generatedValue"))
  }

  lazy val identifier: EntityField = convertElement(dm.getIdentifiers.head)
  lazy val fieldsAsJava = fields.asJava
  lazy val fieldsWithRelation: List[EntityField] = fields.filter(p => p.containsAttribute("relation"))
  lazy val fieldsWithoutRelation: List[EntityField] = fields.filterNot(p => p.containsAttribute("relation"))
  lazy val relatedTypes: Set[String] = simpleRelatedTypes ++ genericRelatedTypes
  lazy val relatedTypesAsJava = relatedTypes.asJava
  lazy val fieldsWithRelationAsJava = fieldsWithRelation.asJava
  lazy val fieldsWithoutRelationAsJava = fieldsWithoutRelation.asJava
  lazy val simpleFieldsWithRelation: List[EntityField] = fieldsWithRelation.filter(p => p.genericType.isEmpty)
  lazy val simpleRelatedTypes: Set[String] = simpleFieldsWithRelation.map(_.getFieldType).toSet
  lazy val simpleRelatedTypesAsJava = simpleRelatedTypes.asJava
  lazy val simpleFieldsWithRelationAsJava = simpleFieldsWithRelation.asJava
  lazy val simpleUniqueRelationTypes = simpleFieldsWithRelation.map(_.fieldType).toSet
  lazy val simpleUniqueRelationTypesAsJava = simpleUniqueRelationTypes.asJava

  lazy val genericFieldsWithRelation: List[EntityField] = fieldsWithRelation.filter(p => p.genericType.isDefined)
  lazy val genericRelatedTypes: Set[String] = genericFieldsWithRelation.map(_.genericType.get).toSet
  lazy val genericRelatedTypesAsJava = genericRelatedTypes.asJava
  lazy val genericFieldsWithRelationAsJava = genericFieldsWithRelation.asJava
  lazy val autoFillDateFields = fields.filter(_.containsAttribute("autofilldate"))
  lazy val autoFillDateFieldsAsJava = autoFillDateFields.asJava


  lazy val searchableWithRelationFields: List[EntityField] = {
    val elements: List[AbstractElement] = dm.filterElements(p => p.containsAttribute("relation") && p.containsAttribute("searchable"))
    elements.map(convertElement)
  }

  lazy val searchableWithRelationFieldsAsJava = searchableWithRelationFields.asJava

  def getName: String = dm.getAttribute("name")

  def getPackageName: String = dm.getAttribute("domain")

  def convertElement(e: AbstractElement): EntityField = {
    val attributes = e.getAttributes()

    val entityField = e match {
      case BooleanType(_) => EntityField("java.lang.Boolean")
      case CompositionType(_) =>
        e.getAttributeOption("typename") match {
          case Some(x) =>
            val classType = x
            EntityField(classType)
          case None =>
            EntityField("java.lang.Class[_]")
        }

      case CurrencyType(_) => EntityField("java.util.Currency")
      case DateTimeType(_) => {
        e.getAttribute("type") match{
          case "time"=>EntityField("java.sql.Time")
          case "timestamp"=>EntityField("java.sql.Timestamp")
          case "date"=>EntityField("java.sql.Date")
          case "datetime"=>EntityField("java.util.Date")
        }
      }
      case DecimalType(_) => EntityField("java.math.BigDecimal")
      case DoubleType(_) => EntityField("java.lang.Double")
      case FileType(_) => EntityField("java.lang.Long")
      case IntegerType(_) => EntityField("java.lang.Integer")
      case LongType(_) => EntityField("java.lang.Long")
      case FloatType(_) => EntityField("java.lang.Float")
      case ListType(_) => e.getAttributeOption("typename") match {
        case None =>
          EntityField("java.util.List")
        case Some(genericType) =>
          EntityField("java.util.List", Some(genericType))
      }
      case MapType(_) => EntityField("java.util.Map[java.lang.Object,java.lang.Object]")
      case StringType(_) => EntityField("java.lang.String")
    }
    entityField += attributes
    entityField
  }
}

case class EntityField(fieldType: String, genericType: Option[String] = None,
                       annotations: List[String] = List.empty[String]) extends AbstractElement("EntityField") {

  def mkString: String = {
    toString
  }

  override def toString: String = {
    s"Field name : $getFieldName Type : $fieldType, Generic Type : $genericType, Annotations : $annotations"
  }

  def getFieldName: String = getAttributes().getOrElse("name", "")

  def getFieldType = fieldType

  def getGenericType = genericType

  def getAnnotationAsJava = annotations.asJavaCollection

  def isEnumerated = getAttributes().contains("enumerated")

  def getAnnotationsAsJava = getAnnotations.asJava

  def getAnnotations[A <: Annotation]: List[_] = getAttributes().toList.map({
    case ("generalParameter", x) => {
      val translation: Annotation = CgAnnotationMaker.createTranslation(Locale.English_United_States, "label")
      CgAnnotationMaker.createGeneralParameter(x, Array(translation))
    }
    case ("identifier", _) => CgAnnotationMaker.createId()
    case ("fileId", _) => CgAnnotationMaker.createFileId()
    case ("relation", "OneToMany") => CgAnnotationMaker.createOneToMany()
    case ("relation", "OneToOne") => CgAnnotationMaker.createOneToOne()
    case ("searchable", _) => CgAnnotationMaker.createSearchable()
    case x => ""
  })
}
