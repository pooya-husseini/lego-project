package com.caspco.infrastructure.spl.cg.entitygenerator.model;

import java.util.Map;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 1/6/16
 *         Time: 1:51 PM
 */
public class ClassModelDto {
    private String packageName;
    private String name;
    private Map<String,String> fields;

    public ClassModelDto(String packageName, String name, Map<String, String> fields) {
        this.packageName = packageName;
        this.name = name;
        this.fields = fields;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getFields() {
        return fields;
    }

    public void setFields(Map<String, String> fields) {
        this.fields = fields;
    }
}
