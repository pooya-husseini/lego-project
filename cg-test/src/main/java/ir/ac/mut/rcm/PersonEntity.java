package ir.ac.mut.rcm;


import com.caspco.xview.wrench.annotations.cg.*;
import com.caspco.xview.wrench.locale.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 22/07/14
 *         Time: 13:09
 */
@Entity
@Table(name = "Person")
@Audited
@Translations({
        @Translation(label = "Person", locale = Locale.English_United_States),
        @Translation(label = "پرسنل", locale = Locale.Persian_Iran)
})
public class PersonEntity {

    private Long id;
    private String personCode;
    private String personDegree;
    private String personStudyFieldName;
    private String personStudyFieldTopic;
    private String personName;
    private String personFamily;
    private Integer personExperiencedYears;
    private Long cv;
    private ResearchCenterEntity researchCenter;
    private String personPosition;
    private String personAcademicRank;
//    private Integer version;


    @Translations({
            @Translation(label = "Degree", locale = Locale.English_United_States),
            @Translation(label = "مقطع تحصیلی", locale = Locale.Persian_Iran)
    })
    @Searchable
    @GeneralParameter(type = "DEGREE", translations = {
            @Translation(label = "Degree", locale = Locale.English_United_States),
            @Translation(label = "مقطع تحصیلی", locale = Locale.Persian_Iran)
    })
    @NotNull
    @Column(name = "degree")
    public String getPersonDegree() {
        return personDegree;
    }

    public void setPersonDegree(String degree) {
        this.personDegree = degree;
    }


    @Translations({
            @Translation(label = "Academic Rank", locale = Locale.English_United_States),
            @Translation(label = "مرتبه علمی", locale = Locale.Persian_Iran)
    })
    @Searchable
    @GeneralParameter(type = "ACADEMIC_RANK", translations = {
            @Translation(label = "", locale = Locale.English_United_States),
            @Translation(label = "مرتبه علمی", locale = Locale.Persian_Iran)
    })
    @Column(name = "academicRank")
    public String getPersonAcademicRank() {
        return personAcademicRank;
    }

    public void setPersonAcademicRank(String academicRank) {
        this.personAcademicRank = academicRank;
    }

    @Searchable
    @Translations({
            @Translation(label = "Study field", locale = Locale.English_United_States),
            @Translation(label = "رشته تحصیلی", locale = Locale.Persian_Iran)
    })
    @GeneralParameter(type = "STUDY_FIELD", translations = {
            @Translation(label = "Study field", locale = Locale.English_United_States),
            @Translation(label = "رشته تحصیلی", locale = Locale.Persian_Iran)
    })
    public String getPersonStudyFieldName() {
        return personStudyFieldName;
    }

    public void setPersonStudyFieldName(String studyFieldName) {
        this.personStudyFieldName = studyFieldName;
    }

    @Searchable
    @Translations({
            @Translation(label = "Study topic", locale = Locale.English_United_States),
            @Translation(label = "گرایش", locale = Locale.Persian_Iran)
    })
    @GeneralParameter(type = "STUDY_FIELD_TOPIC", translations = {
            @Translation(label = "Study topic", locale = Locale.English_United_States),
            @Translation(label = "گرایش", locale = Locale.Persian_Iran)
    })
    @Column(name = "studyFieldTopic")
    public String getPersonStudyFieldTopic() {
        return personStudyFieldTopic;
    }

    public void setPersonStudyFieldTopic(String studyFieldTopic) {
        this.personStudyFieldTopic = studyFieldTopic;
    }


    @Searchable
    @Translations({
            @Translation(label = "Name", locale = Locale.English_United_States),
            @Translation(label = "نام", locale = Locale.Persian_Iran)
    })
    @NotNull
    @Column(name = "name")
    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String name) {
        this.personName = name;
    }


    @Translations({
            @Translation(label = "Family", locale = Locale.English_United_States),
            @Translation(label = "نام خانوادگی", locale = Locale.Persian_Iran)
    })
    @Searchable
    @NotNull
    @Column(name = "family")
    public String getPersonFamily() {
        return personFamily;
    }

    public void setPersonFamily(String family) {
        this.personFamily = family;
    }


    @Translations({
            @Translation(label = "Exp years", locale = Locale.English_United_States),
            @Translation(label = "سال سابقه", locale = Locale.Persian_Iran)
    })
    @Column(name = "experiencedYears")
    public Integer getPersonExperiencedYears() {
        return personExperiencedYears;
    }

    public void setPersonExperiencedYears(Integer experiencedYears) {
        this.personExperiencedYears = experiencedYears;
    }

    @Translations({
            @Translation(label = "CV", locale = Locale.English_United_States),
            @Translation(label = "رزومه", locale = Locale.Persian_Iran)
    })
    @FileId
    public Long getCv() {
        return cv;
    }

    public void setCv(Long cv) {
        this.cv = cv;
    }

    @Translations({
            @Translation(label = "Code", locale = Locale.English_United_States),
            @Translation(label = "کد پرسنلی", locale = Locale.Persian_Iran)
    })
    @Searchable
    @Column(name = "code")
    public String getPersonCode() {
        return personCode;
    }

    public void setPersonCode(String code) {
        this.personCode = code;
    }

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @Translations({
            @Translation(label = "Research center", locale = Locale.English_United_States),
            @Translation(label = "مرکز پژوهشی", locale = Locale.Persian_Iran)
    })
    @NotNull
    @AutoInject
    public ResearchCenterEntity getResearchCenter() {
        return researchCenter;
    }

    public void setResearchCenter(ResearchCenterEntity researchCenter) {
        this.researchCenter = researchCenter;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Searchable
    @Translations({
            @Translation(label = "Position", locale = Locale.English_United_States),
            @Translation(label = "سمت", locale = Locale.Persian_Iran)
    })
    @GeneralParameter(type = "POSITION", translations = {
            @Translation(label = "Position", locale = Locale.English_United_States),
            @Translation(label = "سمت", locale = Locale.Persian_Iran)
    })
    @Column(name = "position")
    public String getPersonPosition() {
        return personPosition;
    }

    public void setPersonPosition(String position) {
        this.personPosition = position;
    }
//    @Version
//    public Integer getVersion() {
//        return version;
//    }
//
//    public void setVersion(Integer version) {
//        this.version = version;
//    }
}