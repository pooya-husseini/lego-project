package ir.ac.mut.rcm;

import com.caspco.xview.wrench.annotations.cg.AutoInject;
import com.caspco.xview.wrench.annotations.cg.Searchable;
import com.caspco.xview.wrench.annotations.cg.Translation;
import com.caspco.xview.wrench.annotations.cg.Translations;
import com.caspco.xview.wrench.locale.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 22/07/14
 *         Time: 13:17
 */
@Entity
@Table(name = "RelativeResearchCenter")
@Audited
@Translations({
        @Translation(label = "مراکز توسعه فناوری متناظر", locale = Locale.Persian_Iran),
        @Translation(label = "Relative Research center", locale = Locale.English_United_States)
})
public class RelativeResearchCenterEntity {

    private Long id;
    private String relativeResearchCenterTitle;
    private String relativeResearchCenterManagingGroup;
    private String relativeResearchCenterOrganization;

    //    private List<DesignCenterEntity> designCenters;
    private ResearchCenterEntity researchCenter;
//    private Integer version;


    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Translations({
            @Translation(label = "عنوان", locale = Locale.Persian_Iran),
            @Translation(label = "Title", locale = Locale.English_United_States)
    })
    @Searchable
    @Column(name = "title")
    public String getRelativeResearchCenterTitle() {
        return relativeResearchCenterTitle;
    }

    public void setRelativeResearchCenterTitle(String title) {
        this.relativeResearchCenterTitle = title;
    }

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @AutoInject
    @NotNull
    public ResearchCenterEntity getResearchCenter() {
        return researchCenter;
    }

    public void setResearchCenter(ResearchCenterEntity researchCenter) {
        this.researchCenter = researchCenter;
    }

    @Translations({
            @Translation(label = "گروه", locale = Locale.Persian_Iran),
            @Translation(label = "Group", locale = Locale.English_United_States)
    })
    @Column(name = "managingGroup")
    public String getRelativeResearchCenterManagingGroup() {
        return relativeResearchCenterManagingGroup;
    }

    public void setRelativeResearchCenterManagingGroup(String group) {
        this.relativeResearchCenterManagingGroup = group;
    }

    @Translations({
            @Translation(label = "سازمان", locale = Locale.Persian_Iran),
            @Translation(label = "Organization", locale = Locale.English_United_States)
    })
    @Searchable
    @Column(name = "organization")
    public String getRelativeResearchCenterOrganization() {
        return relativeResearchCenterOrganization;
    }

    public void setRelativeResearchCenterOrganization(String organization) {
        this.relativeResearchCenterOrganization = organization;
    }

//    @Version
//    public Integer getVersion() {
//        return version;
//    }
//
//    public void setVersion(Integer version) {
//        this.version = version;
//    }
}