package ir.ac.mut.rcm;


import com.caspco.xview.wrench.annotations.cg.*;
import com.caspco.xview.wrench.locale.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 22/07/14
 *         Time: 13:15
 */
@Entity
@Table(name = "ResearchProject")
@Translations({
        @Translation(label = "Research Projects", locale = Locale.English_United_States),
        @Translation(label = "پروژه های تحقیقاتی", locale = Locale.Persian_Iran)
})
@Audited
public class ResearchProjectEntity {
    private Long id;
    private String researchProjectTitle;
    private String researchProjectLevel;
    private String researchProjectDescription;
    private String researchProjectStatus;

    private ResearchCenterEntity researchCenter;
//    private Integer version;


    @Translations({
            @Translation(label = "Description", locale = Locale.English_United_States),
            @Translation(label = "توضیحات", locale = Locale.Persian_Iran)
    })
    @Column(name = "description")
    public String getResearchProjectDescription() {
        return researchProjectDescription;
    }

    public void setResearchProjectDescription(String description) {
        this.researchProjectDescription = description;
    }


    @Translations({
            @Translation(label = "Status", locale = Locale.English_United_States),
            @Translation(label = "وضعیت", locale = Locale.Persian_Iran)
    })
    @GeneralParameter(type = "PROJECT_STATUS", translations = {
            @Translation(label = "Status", locale = Locale.English_United_States),
            @Translation(label = "وضعیت پروژه", locale = Locale.Persian_Iran)
    })
    @Searchable
    @NotNull
    @Column(name = "status")
    public String getResearchProjectStatus() {
        return researchProjectStatus;
    }

    public void setResearchProjectStatus(String status) {
        this.researchProjectStatus = status;
    }

    @Translations({
            @Translation(label = "Title", locale = Locale.English_United_States),
            @Translation(label = "عنوان", locale = Locale.Persian_Iran)
    })
    @Searchable
    @NotNull
    @Column(name = "title")
    public String getResearchProjectTitle() {
        return researchProjectTitle;
    }

    public void setResearchProjectTitle(String title) {
        this.researchProjectTitle = title;
    }

    @Translations({
            @Translation(label = "Level", locale = Locale.English_United_States),
            @Translation(label = "سطح", locale = Locale.Persian_Iran)
    })
    @Searchable
    @GeneralParameter(type = "LEVEL", translations = {
            @Translation(label = "Level", locale = Locale.English_United_States),
            @Translation(label = "سطح", locale = Locale.Persian_Iran)
    })
    @NotNull
    @Column(name = "level")
    public String getResearchProjectLevel() {
        return researchProjectLevel;
    }

    public void setResearchProjectLevel(String level) {
        this.researchProjectLevel = level;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @NotNull
    @Translations({
            @Translation(label = "Research center", locale = Locale.English_United_States),
            @Translation(label = "مرکز پژوهشی", locale = Locale.Persian_Iran)
    })
    @AutoInject
    public ResearchCenterEntity getResearchCenter() {
        return researchCenter;
    }

    public void setResearchCenter(ResearchCenterEntity researchCenter) {
        this.researchCenter = researchCenter;
    }
//    @Version
//    public Integer getVersion() {
//        return version;
//    }
//
//    public void setVersion(Integer version) {
//        this.version = version;
//    }

}