package ir.ac.mut.rcm;

import com.caspco.xview.wrench.annotations.cg.AutoInject;
import com.caspco.xview.wrench.annotations.cg.Searchable;
import com.caspco.xview.wrench.annotations.cg.Translation;
import com.caspco.xview.wrench.annotations.cg.Translations;
import com.caspco.xview.wrench.locale.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 22/07/14
 *         Time: 13:19
 */
@Entity
@Table(name = "DesignCenter")
@Audited
@Translations({
        @Translation(label = "Design center", locale = Locale.English_United_States),
        @Translation(label = "دفاتر طراحی متناظر", locale = Locale.Persian_Iran)
})
public class DesignCenterEntity {

    private Long id;
    private String designCenterManagingGroup;
    private String designCenterOrganization;
    private String designCenterTitle;
    //    private List<DesignCenterEntity> designCenters;
    private ResearchCenterEntity researchCenter;
//    private Integer version;


    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(cascade = CascadeType.MERGE)
    @NotNull
    @AutoInject

    public ResearchCenterEntity getResearchCenter() {
        return researchCenter;
    }

    public void setResearchCenter(ResearchCenterEntity researchCenter) {
        this.researchCenter = researchCenter;
    }

    @Translations({
            @Translation(label = "گروه", locale = Locale.Persian_Iran),
            @Translation(label = "Group", locale = Locale.English_United_States)
    })
    @Searchable
    @Column(name = "managingGroup")
    public String getDesignCenterManagingGroup() {
        return designCenterManagingGroup;
    }

    public void setDesignCenterManagingGroup(String group) {
        this.designCenterManagingGroup = group;
    }

    @Translations({
            @Translation(label = "سازمان", locale = Locale.Persian_Iran),
            @Translation(label = "Organization", locale = Locale.English_United_States)
    })
    @Searchable
    @Column(name = "organization")
    public String getDesignCenterOrganization() {
        return designCenterOrganization;
    }

    public void setDesignCenterOrganization(String organization) {
        this.designCenterOrganization = organization;
    }


    @Translations({
            @Translation(label = "عنوان", locale = Locale.Persian_Iran),
            @Translation(label = "Title", locale = Locale.English_United_States)
    })
    @Searchable
    @Column(name = "title")
    public String getDesignCenterTitle() {
        return designCenterTitle;
    }

    public void setDesignCenterTitle(String title) {
        this.designCenterTitle = title;
    }


    //    @Version
//    public Integer getVersion() {
//        return version;
//    }
//
//    public void setVersion(Integer version) {
//        this.version = version;
//    }
}