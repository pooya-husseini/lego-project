package ir.ac.mut.rcm;


import com.caspco.xview.wrench.annotations.cg.*;
import com.caspco.xview.wrench.annotations.cg.security.Security;
import com.caspco.xview.wrench.locale.Locale;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 22/07/14
 *         Time: 13:06
 */
@Entity
@Table(name = "ResearchCenter")
@Translations({
        @Translation(label = "مرکز پژوهشی", locale = Locale.Persian_Iran),
        @Translation(label = "Research center", locale = Locale.English_United_States)
})
@Audited
@ClientSideCacheable(retainClean = false)
@Security(roles = "ROLE_ADMIN")
public class ResearchCenterEntity {

    private Long id;
    private String researchCenterTitle;
    private String researchCenterMission;
    private String researchCenterUniversity;
    private String researchCenterCampus;

    @Searchable
    @Translations({
            @Translation(label = "Title", locale = Locale.English_United_States),
            @Translation(label = "عنوان مرکز پژوهشی", locale = Locale.Persian_Iran)
    })
    @NotNull
    @Title
    @Column(name = "title")
    public String getResearchCenterTitle() {
        return researchCenterTitle;
    }

    public void setResearchCenterTitle(String title) {
        this.researchCenterTitle = title;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Searchable
    @Translations({
            @Translation(label = "Mission", locale = Locale.English_United_States),
            @Translation(label = "ماموریت", locale = Locale.Persian_Iran)
    })
    @Column(name = "mission")
    public String getResearchCenterMission() {
        return researchCenterMission;
    }

    public void setResearchCenterMission(String mission) {
        this.researchCenterMission = mission;
    }

//    @ManyToOne
//    @Translations({
//            @Translation(label = "Manager", locale = Locale.English_United_States),
//            @Translation(label = "رییس", locale =Locale.Persian_Iran)
//    })
//    public PersonEntity getManager() {
//        return manager;
//    }
//
//    public void setManager(PersonEntity manager) {
//        this.manager = manager;
//    }

    @GeneralParameter(type = "UNIVERSITY",translations = {
            @Translation(label = "University", locale = Locale.English_United_States),
            @Translation(label = "مجتمع دانشگاهی", locale = Locale.Persian_Iran)
    })
    @Searchable
    @Translations({
            @Translation(label = "University", locale = Locale.English_United_States),
            @Translation(label = "مجتمع دانشگاهی", locale = Locale.Persian_Iran)
    })
    @Column(name = "university")
    public String getResearchCenterUniversity() {
        return researchCenterUniversity;
    }

    public void setResearchCenterUniversity(String university) {
        this.researchCenterUniversity = university;
    }

    @GeneralParameter(type = "CAMPUS",translations = {
            @Translation(label = "Campus", locale = Locale.English_United_States),
            @Translation(label = "پژوهشکده/دانشکده", locale = Locale.Persian_Iran)
    })
    @Searchable
    @NotNull
    @Translations({
            @Translation(label = "Campus", locale = Locale.English_United_States),
            @Translation(label = "پژوهشکده/دانشکده", locale = Locale.Persian_Iran)
    })
    @Column(name = "campus")
    public String getResearchCenterCampus() {
        return researchCenterCampus;
    }

    public void setResearchCenterCampus(String campus) {
        this.researchCenterCampus = campus;
    }




}