package ir.ac.mut.rcm;



import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 22/07/14
 *         Time: 13:14
 */
@Entity
@Table(name = "Equipment")
@Audited
@Translations({
        @Translation(label = "Equipments", locale = Locale.English_United_States),
        @Translation(label = "تجهیزات", locale = Locale.Persian_Iran)
})
public class EquipmentEntity {
    private Long id;
    private String equipmentCode;
    private String equipmentTitle;
    private Integer equipmentQuantity;
    private String equipmentStatus;
    private String equipmentType;
    private ResearchCenterEntity researchCenter;
//    private Integer version;

    @Translations({
            @Translation(label = "Quantity", locale = Locale.English_United_States),
            @Translation(label = "تعداد", locale = Locale.Persian_Iran)
    })
    @NotNull
    @Column(name = "quantity")
    public Integer getEquipmentQuantity() {
        return equipmentQuantity;
    }

    public void setEquipmentQuantity(Integer quantity) {
        this.equipmentQuantity = quantity;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Translations({
            @Translation(label = "Title", locale = Locale.English_United_States),
            @Translation(label = "عنوان", locale = Locale.Persian_Iran)
    })
    @Searchable
    @NotNull
    @Column(name = "title")
    public String getEquipmentTitle() {
        return equipmentTitle;
    }

    public void setEquipmentTitle(String title) {
        this.equipmentTitle = title;
    }

    @Searchable
    @Translations({
            @Translation(label = "Code", locale = Locale.English_United_States),
            @Translation(label = "کد", locale = Locale.Persian_Iran)
    })
    @Column(name = "code")
    public String getEquipmentCode() {
        return equipmentCode;
    }

    public void setEquipmentCode(String code) {
        this.equipmentCode = code;
    }


    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @NotNull
    @Translations({
            @Translation(label = "Research Center", locale = Locale.English_United_States),
            @Translation(label = "مرکز پژوهشی", locale = Locale.Persian_Iran)
    })
    @AutoInject
    @JoinColumn(name = "researchCenter_id")
    public ResearchCenterEntity getResearchCenter() {
        return researchCenter;
    }

    public void setResearchCenter(ResearchCenterEntity researchCenter) {
        this.researchCenter = researchCenter;
    }

    @Searchable
    @Translations({
            @Translation(label = "Status", locale = Locale.English_United_States),
            @Translation(label = "وضعیت", locale = Locale.Persian_Iran)
    })
    @GeneralParameter(type = "EQUIPMENT_STATUS", translations = {
            @Translation(label = "Status", locale = Locale.English_United_States),
            @Translation(label = "وضعیت", locale = Locale.Persian_Iran)
    })
    @Column(name = "status")
    public String getEquipmentStatus() {
        return equipmentStatus;
    }

    public void setEquipmentStatus(String status) {
        this.equipmentStatus = status;
    }

    public String getEquipmentType() {
        return equipmentType;
    }


    @Searchable
    @Translations({
            @Translation(label = "Equipment type", locale = Locale.English_United_States),
            @Translation(label = "نوع تجهیزات", locale = Locale.Persian_Iran)
    })
    @GeneralParameter(type = "EQUIPMENT_TYPE", translations = {
            @Translation(label = "Equipment Type", locale = Locale.English_United_States),
            @Translation(label = "نوع تجهیزات", locale = Locale.Persian_Iran)
    })
    @Column(name = "equipmentType")
    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }


    //    @Version
//    public Integer getVersion() {
//        return version;
//    }
//
//    public void setVersion(Integer version) {
//        this.version = version;
//    }
}