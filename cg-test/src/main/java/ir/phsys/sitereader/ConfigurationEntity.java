package ir.phsys.sitereader;

import com.caspco.xview.wrench.annotations.cg.Searchable;
import com.caspco.xview.wrench.annotations.cg.Translation;
import com.caspco.xview.wrench.annotations.cg.Translations;
import com.caspco.xview.wrench.locale.Locale;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 8/12/15
 *         Time: 1:45 PM
 */

@Entity
@Table
@Translations({@Translation(label = "Configuration", locale = Locale.English_United_States), @Translation(label = "تنظیمات", locale = Locale.Persian_Iran)})
public class ConfigurationEntity {
    private String name;
    private Long id;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Searchable
    @Translations({@Translation(locale = Locale.English_United_States,label = "Name"),@Translation(locale = Locale.Persian_Iran,label = "نام")})
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
