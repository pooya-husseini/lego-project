package ir.phsys.sitereader;

import com.caspco.xview.wrench.annotations.cg.Translation;
import com.caspco.xview.wrench.annotations.cg.Translations;
import com.caspco.xview.wrench.locale.Locale;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 8/12/15
 *         Time: 1:45 PM
 */

@Entity
@Table
@Translations({@Translation(label = "Search News", locale = Locale.English_United_States), @Translation(label = "جستجوی اخبار", locale = Locale.Persian_Iran)})
public class SearchNewsEntity {

    private Long id;
    private String title;
    private String text;
    private String category;
    private String publisher;
    private Date pubDate;

    @GeneratedValue
    @Id
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Translations({@Translation(label = "Title", locale = Locale.English_United_States), @Translation(label = "عنوان", locale = Locale.Persian_Iran)})
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Translations({@Translation(label = "Text", locale = Locale.English_United_States), @Translation(label = "متن خبر", locale = Locale.Persian_Iran)})
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Translations({@Translation(label = "Category", locale = Locale.English_United_States), @Translation(label = "گروه خبری", locale = Locale.Persian_Iran)})
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Translations({@Translation(label = "Publisher", locale = Locale.English_United_States), @Translation(label = "ناشر", locale = Locale.Persian_Iran)})
    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Translations({@Translation(label = "Publication date", locale = Locale.English_United_States), @Translation(label = "تاریخ انتشار", locale = Locale.Persian_Iran)})
    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }
}
