package ir.phsys.sitereader;

import com.caspco.xview.wrench.annotations.cg.EnumEntity;
import com.caspco.xview.wrench.annotations.cg.Translation;
import com.caspco.xview.wrench.annotations.cg.Translations;
import com.caspco.xview.wrench.locale.Locale;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 29/12/14
 *         Time: 12:04
 */

@EnumEntity
public enum UserManagementRole {
    @Translations({
            @Translation(locale = Locale.English_United_States, label = "User"),
            @Translation(locale = Locale.Persian_Iran, label = "کاربر")
    })
    ROLE_USER,

    @Translations({
            @Translation(locale = Locale.English_United_States, label = "Admin"),
            @Translation(locale = Locale.Persian_Iran, label = "مدیر سیستم")
    })
    ROLE_ADMIN
}