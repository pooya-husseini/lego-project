package ir.phsys.sitereader;

import com.caspco.xview.wrench.annotations.cg.Searchable;
import com.caspco.xview.wrench.annotations.cg.Translation;
import com.caspco.xview.wrench.annotations.cg.Translations;
import com.caspco.xview.wrench.locale.Locale;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 8/12/15
 *         Time: 1:45 PM
 */

@Entity
@Table
@Translations({@Translation(label = "Logs", locale = Locale.English_United_States), @Translation(label = "مشاهده لاگ ها", locale = Locale.Persian_Iran)})
public class LogsEntity {
    private String level;
    private Long id;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Searchable
    @Translations({@Translation(locale = Locale.English_United_States,label = "Level"),@Translation(locale = Locale.Persian_Iran,label = "سطح")})
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
