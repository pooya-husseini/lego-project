
import org.junit.Assert;
import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.w3c.tidy.Tidy;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */
public class CompressJs {

    private static final String BEAUTIFY_SCRIPT_PATH = "js/html-beautify.js";
    private static final char intToBase64[] = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
    };

    private static String byteArrayToBase64(byte[] a) {
        int aLen = a.length;
        int numFullGroups = aLen / 3;
        int numBytesInPartialGroup = aLen - 3 * numFullGroups;
        int resultLen = 4 * ((aLen + 2) / 3);
        StringBuffer result = new StringBuffer(resultLen);
        char[] intToAlpha = intToBase64;

        // Translate all full groups from byte array elements to Base64
        int inCursor = 0;
        for (int i = 0; i < numFullGroups; i++) {
            int byte0 = a[inCursor++] & 0xff;
            int byte1 = a[inCursor++] & 0xff;
            int byte2 = a[inCursor++] & 0xff;
            result.append(intToAlpha[byte0 >> 2]);
            result.append(intToAlpha[(byte0 << 4) & 0x3f | (byte1 >> 4)]);
            result.append(intToAlpha[(byte1 << 2) & 0x3f | (byte2 >> 6)]);
            result.append(intToAlpha[byte2 & 0x3f]);
        }

        // Translate partial group if present
        if (numBytesInPartialGroup != 0) {
            int byte0 = a[inCursor++] & 0xff;
            result.append(intToAlpha[byte0 >> 2]);
            if (numBytesInPartialGroup == 1) {
                result.append(intToAlpha[(byte0 << 4) & 0x3f]);
                result.append("==");
            } else {
                // assert numBytesInPartialGroup == 2;
                int byte1 = a[inCursor++] & 0xff;
                result.append(intToAlpha[(byte0 << 4) & 0x3f | (byte1 >> 4)]);
                result.append(intToAlpha[(byte1 << 2) & 0x3f]);
                result.append('=');
            }
        }
        // assert inCursor == a.length;
        // assert result.length() == resultLen;
        return result.toString();
    }

    @Test
    public void prettifyJs() throws IOException {
        URL url = this.getClass().getClassLoader().getResource(BEAUTIFY_SCRIPT_PATH);
        File file = new File(url.getFile());
        Assert.assertTrue(file.exists());
        String source = new String(Files.readAllBytes(Paths.get(file.toURI())));
        String s = "/home/pooya/projects/cg-test/src/main/webapp/app/view/ir/ac/mut/rcm/CreateDesignCenter.html";
        String bytes = new String(Files.readAllBytes(Paths.get(s)));

        Context cx = Context.enter();
        Scriptable scope = cx.initStandardObjects();

        cx.evaluateString(scope, "var global = {};", "global", 1, null);
        cx.evaluateString(scope, source, "beautify", 1, null);
        scope.put("source", scope, bytes);

        cx.evaluateString(scope, "result = global.html_beautify(source);", "beautify", 1, null);


        String inline = (String) scope.get("result", scope);
        System.out.println(inline);
    }

    @Test
    public void doSomething() throws IOException {
        InputStream resource = this.getClass().getClassLoader().getResourceAsStream("db.properties");
        System.out.println(resource);
        Properties properties = new Properties();
        properties.load(resource);

        for (Object o : properties.keySet()) {
            System.out.println(o);
        }

//        System.out.println(file.exists());
    }

    private String cleanData(String data) throws UnsupportedEncodingException {
        Tidy tidy = new Tidy();
//        tidy.setInputEncoding("UTF-8");
//        tidy.setOutputEncoding("UTF-8");
//        tidy.setWraplen(Integer.MAX_VALUE);
        tidy.setQuiet(true);
        tidy.setPrintBodyOnly(true);
        tidy.setXmlOut(false);
//        tidy.setSmartIndent(true);
        tidy.setXHTML(false);

        ByteArrayInputStream inputStream = new ByteArrayInputStream(data.getBytes("UTF-8"));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        tidy.parseDOM(inputStream, outputStream);
        return outputStream.toString("UTF-8");
    }

    @Test
    public void beautifyHtml() throws IOException {

//        Tidy tidy = new Tidy();
//        tidy.setInputEncoding("UTF-8");
//        tidy.setOutputEncoding("UTF-8");
//        tidy.setWraplen(Integer.MAX_VALUE);
//        tidy.setPrintBodyOnly(true);
//        tidy.setXmlOut(true);
//        tidy.setSmartIndent(true);
////        tidy.setLiteralAttribs();
        File file = new File("/home/pooya/projects/cg-test/src/main/webapp/app/view/ir/ac/mut/rcm");
        File[] files = file.listFiles();
        for (File file1 : files != null ? files : new File[0]) {
            byte[] allBytes = Files.readAllBytes(Paths.get(file1.getPath()));


            String s = HTMLSanitiser.stripInvalidMarkup(new String(allBytes), false);
            System.out.println(s);
        }
    }

    @Test
    public void sanitizeHtml() throws TransformerException {
        String s = " <ui:input-text orientation=\"horizontal\" label-size=\"3\" id='password'\n" +
                "                           model='Model.password' not-null='true'\n" +
                "                           label='{{translate(\"app.newPassword\")}}'\n" +
                "                           ng-model='Model.password' ui-validation='required();'\n" +
                "                           type='password'></ui:input-text>";


        System.out.println(HTMLSanitiser.stripInvalidMarkup(s, true));

    }

    public String format(String xml) {

        try {
            final InputSource src = new InputSource(new StringReader(xml));
            final Node document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src).getDocumentElement();
            final Boolean keepDeclaration = xml.startsWith("<?xml");

            //May need this: System.setProperty(DOMImplementationRegistry.PROPERTY,"com.sun.org.apache.xerces.internal.dom.DOMImplementationSourceImpl");


            final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
            final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
            final LSSerializer writer = impl.createLSSerializer();

            writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE); // Set this to true if the output needs to be beautified.
            writer.getDomConfig().setParameter("xml-declaration", keepDeclaration); // Set this to true if the declaration is needed to be outputted.

            return writer.writeToString(document);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void makeBase64() throws IOException {
        String s = byteArrayToBase64(Files.readAllBytes(Paths.get("/home/pooya/projects/cg-test/src/main/webapp/app/fonts/xb-zar.ttf")));
        FileWriter writer=new FileWriter("/home/pooya/projects/cg-test/src/main/webapp/app/custom-font.js",true);
        writer.write(s);
        writer.close();
        System.out.println(s.length());
    }
}
