//package com.caspco.auditmanagement.dto.rest;
//
//
//import com.caspco.auditmanagement.parameter.UserEntity;
//import com.caspco.auditmanagement.parameter.UserRole;
//import com.caspco.auditmanagement.parameter.dto.UserDto;
//import com.caspco.auditmanagement.parameter.service.UserService;
//import com.caspco.auditmanagement.parameter.transformer.UserTransformer;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Date;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = "classpath*:test-context.xml")
//
//@Transactional(noRollbackFor = Exception.class)
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
//public class UserRestServiceTest {
//    @Autowired
//    private UserTransformer transformer;
//
//    @Autowired
//    private UserService service;
//
//
//    @Test
//
//    public void testAddUser() {
//        UserDto userDto = new UserDto();
//        userDto.setActive(Boolean.FALSE);
//        userDto.setExpireDate(new Date());
//        userDto.setFirstName("Pooya");
//        userDto.setLastName("Husseini");
//        userDto.setLockFinishDate(new Date());
//        userDto.setPassword("123");
//        userDto.setRole(UserRole.ROLE_ADMIN);
//
//        for (int i = 1001; i < 10000; i++) {
//            userDto.setUserName("User" + i);
//            final UserEntity entity = transformer.toEntity(transformer.dtoToDomainObject(userDto));
//            service.insert(entity);
//        }
//    }
//
//    @Test
//    public void testGetAll() throws Exception {
//
//    }
//
//    @Test
//    public void testGetUserCount() throws Exception {
//
//    }
//
//    @Test
//    public void testSearchUser() throws Exception {
//
//    }
//
//    @Test
//    public void testReport() throws Exception {
//
//    }
//
//    @Test
//    public void testUpdateUser() throws Exception {
//
//    }
//
//    @Test
//    public void testDeleteUserById() throws Exception {
//
//    }
//
//    @Test
//    public void testHandle() throws Exception {
//
//    }
//}