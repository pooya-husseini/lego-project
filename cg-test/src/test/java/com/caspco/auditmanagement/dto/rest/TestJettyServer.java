//package com.caspco.auditmanagement.dto.rest;
//
//import org.eclipse.jetty.server.Request;
//import org.eclipse.jetty.server.Server;
//import org.eclipse.jetty.server.handler.AbstractHandler;
//import org.junit.Test;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//
///**
// * @author : Пуя Гуссейни
// *         Email : info@pooya-hfp.ir
// *         Date: 24/01/15
// *         Time: 14:08
// */
//public class TestJettyServer {
//    @Test
//    public void initializeJetty() throws Exception {
//        Server server = new Server(8081);
//        server.setHandler(new HelloHandler());
//        server.start();
//        server.join();
//    }
//}
//
//class HelloHandler extends AbstractHandler {
//    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
//            throws IOException, ServletException {
//        response.setStatus(HttpServletResponse.SC_OK);
//
//
//        baseRequest.setHandled(true);
//
//        String path = "target/cg-test-1.0-SNAPSHOT/" + target;
//        if (path.endsWith("//")) {
//            path = path.substring(0, path.length() - 1) + "index.html";
//        }
//        if (!new File(path).exists()) {
//            System.out.println("path not found = " + path);
//            return;
//        }
//        String contentType = Files.probeContentType(Paths.get(path));
//        response.setContentType(contentType + ";charset=utf-8");
//
//
//        byte[] content = Files.readAllBytes(Paths.get(path));
//        response.getOutputStream().write(content);
////        for (String string : strings) {
////            response.getWriter().println(string);
////        }
//
//
//    }
//}