package com.caspco.malek;

import java.util.HashMap;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 5/26/15
 *         Time: 11:12 AM
 */
public class Test {
    public static void main(String[] args) {
        IllegalArgumentException exception=new IllegalArgumentException();
        testException(exception);
    }

    private static void testException(Exception e) {
        HashMap<Class<? extends Exception>,Integer> map = new HashMap<>();
        map.put(IllegalArgumentException.class, 12);
        map.put(Exception.class, 14345);

        Exception x = new IllegalArgumentException();

        System.out.println(map.get(x.getClass()));

    }


}
