//package com.caspco.malek;
//
//
//import com.caspco.xview.data.DataAccess;
//import ir.ac.mut.rcm.GeneralParameterEntity;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.util.StopWatch;
//
//import java.util.List;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = "classpath*:test-context.xml")
//@Transactional
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
//public class GPOperationsTest {
//
//    @Autowired
//    private DataAccess dataAccess;
//
//    private StopWatch stopWatch = new StopWatch();
//
//
//
//    @After
//    public void giveUp() {
////        stopWatch.stop();
////        System.out.println("StopWatch");
////        System.out.println(stopWatch.prettyPrint());
//    }
//
////    @Before
////    public void delete(){
////        GeneralParameterEntity gp=new GeneralParameterEntity();
////        gp.setMainType("Version");
////        dataAccess.delete(gp);
////    }
//
//    @Test
//    public void insert() {
//        for (int i = 0; i < 100; i++) {
//            GeneralParameterEntity gp = new GeneralParameterEntity();
//            gp.setCode(String.valueOf(i));
//            gp.setMainType("Version");
//            gp.setDescription("Version" + i);
//
//            dataAccess.insert(gp);
//        }
//
//    }
//
//
//    @Test
//    public void testUpdate() {
//        GeneralParameterEntity gp = new GeneralParameterEntity();
//        gp.setMainType("Version");
//        List<GeneralParameterEntity> version = dataAccess.find(gp);
//        for (int i = 0; i < 100; i++) {
//            gp = version.get(i);
//            gp.setCode(String.valueOf(i*2));
//            dataAccess.update(gp);
//        }
//    }
//}