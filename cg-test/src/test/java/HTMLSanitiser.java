/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */

import net.htmlparser.jericho.*;

import java.util.List;
import java.util.Objects;

public class HTMLSanitiser {
    private static final Object VALID_MARKER = new Object();


    private HTMLSanitiser() {
    } // not instantiable

    public static String encodeInvalidMarkup(String pseudoHTML) {
        return encodeInvalidMarkup(pseudoHTML, false);
    }

    public static String encodeInvalidMarkup(String pseudoHTML, boolean formatWhiteSpace) {
        return sanitise(pseudoHTML, formatWhiteSpace, false);
    }

    public static String stripInvalidMarkup(String pseudoHTML) {
        return stripInvalidMarkup(pseudoHTML, false);
    }

    public static String stripInvalidMarkup(String pseudoHTML, boolean formatWhiteSpace) {
        return sanitise(pseudoHTML, formatWhiteSpace, true);
    }

    private static String sanitise(String pseudoHTML, boolean formatWhiteSpace, boolean stripInvalidElements) {
        Source source = new Source(pseudoHTML);
        source.fullSequentialParse();
        OutputDocument outputDocument = new OutputDocument(source);
        List<Tag> tags = source.getAllTags();
        int pos = 0;
        for (Tag tag : tags) {
            if (processTag(tag, outputDocument)) {
                tag.setUserData(VALID_MARKER);
            } else {
                if (!stripInvalidElements) continue; // element will be encoded along with surrounding text
                outputDocument.remove(tag);
            }
            reencodeTextSegment(source, outputDocument, pos, tag.getBegin(), formatWhiteSpace);
            pos = tag.getEnd();
        }
        reencodeTextSegment(source, outputDocument, pos, source.getEnd(), formatWhiteSpace);
        return outputDocument.toString();
    }

    private static boolean processTag(Tag tag, OutputDocument outputDocument) {
        String elementName = tag.getName();
//        if (!VALID_ELEMENT_NAMES.contains(elementName)) return false;
        if (tag.getTagType() == StartTagType.NORMAL) {
            Element element = tag.getElement();
            if (HTMLElements.getEndTagRequiredElementNames().contains(elementName)) {
                if (element.getEndTag() == null) return false; // reject start tag if its required end tag is missing
            } else if (HTMLElements.getEndTagOptionalElementNames().contains(elementName)) {
                if (Objects.equals(elementName, HTMLElementName.LI) && !isValidLITag(tag))
                    return false; // reject invalid LI tags
                if (element.getEndTag() == null)
                    outputDocument.insert(element.getEnd(), getEndTagHTML(elementName)); // insert optional end tag if it is missing
            }
            outputDocument.replace(tag, getStartTagHTML(element.getStartTag()));
        } else if (tag.getTagType() == EndTagType.NORMAL) {
            if (tag.getElement() == null) return false; // reject end tags that aren't associated with a start tag
            if (Objects.equals(elementName, HTMLElementName.LI) && !isValidLITag(tag))
                return false; // reject invalid LI tags
            outputDocument.replace(tag, getEndTagHTML(elementName));
        } else {
            return false; // reject abnormal tags
        }
        return true;
    }

    private static boolean isValidLITag(Tag tag) {
        Element parentElement = tag.getElement().getParentElement();
        if (parentElement == null) return false; // ignore LI elements without a parent
        // ignore LI elements who's parent is not valid
        return parentElement.getStartTag().getUserData() == VALID_MARKER && (Objects.equals(parentElement.getName(), HTMLElementName.UL) || Objects.equals(parentElement.getName(), HTMLElementName.OL));
    }

    private static void reencodeTextSegment(Source source, OutputDocument outputDocument, int begin, int end, boolean formatWhiteSpace) {
        if (begin >= end) return;
        Segment textSegment = new Segment(source, begin, end);
        String decodedText = CharacterReference.decode(textSegment);
        String encodedText = formatWhiteSpace ? CharacterReference.encodeWithWhiteSpaceFormatting(decodedText) : CharacterReference.encode(decodedText);
        outputDocument.replace(textSegment, encodedText);
    }

    private static CharSequence getStartTagHTML(StartTag startTag) {
        // tidies and filters out non-approved attributes
        StringBuilder sb = new StringBuilder();
        sb.append('<').append(startTag.getName());
        for (Attribute attribute : startTag.getAttributes()) {
//            if (VALID_ATTRIBUTE_NAMES.contains(attribute.getKey())) {
            sb.append(' ').append(attribute.getName());
            if (attribute.getValue() != null) {
                sb.append("=\"");
                sb.append(CharacterReference.encode(attribute.getValue()));
                sb.append('"');
            }
//            }
        }
        if (startTag.getElement().getEndTag() == null && !HTMLElements.getEndTagOptionalElementNames().contains(startTag.getName()))
            sb.append(" /");
        sb.append('>');
        return sb;
    }

    private static String getEndTagHTML(String tagName) {
        return "</" + tagName + '>';
    }
}