/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.formatter.CodeFormatter;
import org.eclipse.jdt.internal.formatter.DefaultCodeFormatter;
import org.eclipse.jdt.internal.formatter.DefaultCodeFormatterOptions;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Formats Java code using Eclipse's code formatter.
 * <p/>
 * Based on: https://ssscripting.wordpress.com/2009/06/10/how-to-use-the-eclipse-code-formatter-from-your-code/
 */
public class Formatter {

    private Map<String, Object> codeFormatterOptionsMap =
            new java.util.HashMap<String, Object>();

    /**
     * Main method. Arguments: &lt;options file&gt; [&lt;file or directory&gt;]
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) throws Exception {
        Formatter f = new Formatter();
//        String cfofn = "/home/pooya/projects/cg-test/src/test/java/config.properties";
//        List<String> cfoLines = FileUtils.readLines(new File(cfofn));
//        f.setCodeFormatterOptionsMap
//                (Formatter.parseCodeFormatterOptions(cfoLines));

        String formattedCode;

        File file = new File("/home/pooya/projects/cg-test/src/main/java/ir/ac/mut/rcm/rest/DesignCenterRestService.java");

        formattedCode = f.format(FileUtils.readFileToString(file));
        System.out.println(formattedCode);


    }


    // For tab_char, 1 = TAB, 2 = SPACE, 4 = MIXED. Use 2 because tabs are evil.

    /**
     * Parses code formatter options. Each line specifies an option as:
     * &lt;field name&gt;:&lt;type&gt;:&lt;value&gt;. Each option references
     * a public field in Eclipse's <code>DefaultCodeFormatterOptions</code>
     * class. Supported types are boolean, int, long, and string. Comment lines
     * starting with '#' and blank lines are ignored.
     *
     * @param opts code formatter options, one per string
     * @return map of field names to values (as Objects, possibly wrappers)
     */
    public static Map<String, Object> parseCodeFormatterOptions(List<String> opts) {
        Map<String, Object> m = new java.util.HashMap<String, Object>();
        for (String opt : opts) {
            if (opt.trim().equals("") || opt.startsWith("#")) {
                continue;
            }
            String[] optParts = opt.split(":", 3);  // field:type:value
            String fieldName = optParts[0];
            String type = optParts[1];
            String valueString = optParts[2];
            Object value;
            switch (type.charAt(0)) {
                case 'b':
                    value = Boolean.valueOf(valueString);
                    break;
                case 'i':
                    value = Integer.valueOf(valueString);
                    break;
                case 'l':
                    value = Long.valueOf(valueString);
                    break;
                case 'd':
                    value = Double.valueOf(valueString);
                    break;
                default:
                    value = valueString;
                    break;
            }
            m.put(fieldName, value);
        }
        return m;
    }

//    void modifyCFOptions(DefaultCodeFormatterOptions cfOptions) {
//        for (Map.Entry<String, Object> e : codeFormatterOptionsMap.entrySet()) {
//            String fieldName = e.getKey();
//            try {
//                Field f =
//                        DefaultCodeFormatterOptions.class.getField(fieldName);
//                // System.out.println (fieldName + " = " + e.getValue());
//                f.set(cfOptions, e.getValue());
//            } catch (NoSuchFieldException exc) {
//                throw new IllegalArgumentException("Invalid option " +
//                        fieldName);
//            } catch (IllegalAccessException exc) {
//                throw new IllegalArgumentException("Option inaccessible: " +
//                        fieldName);
//            }
//        }
//    }

    private String getVersion() {
        String version = System.getProperty("java.version");
        Integer pos = version.indexOf('.', version.indexOf('.') + 1);
        return version.substring(0, pos);
    }

    /**
     * Formats the given source code.
     *
     * @param code unformatted code
     * @return formatted code
     */
    public String format(String code)
            throws MalformedTreeException, BadLocationException {
        Map options = new java.util.HashMap();
        options.put(JavaCore.COMPILER_SOURCE, "1.5");
        options.put(JavaCore.COMPILER_COMPLIANCE, "1.5");
        options.put(JavaCore.COMPILER_CODEGEN_TARGET_PLATFORM, "1.5");

        DefaultCodeFormatterOptions cfOptions =
                DefaultCodeFormatterOptions.getDefaultSettings();
        cfOptions.insert_new_line_after_annotation_on_type = true;
        cfOptions.insert_new_line_after_annotation_on_method = true;
        cfOptions.insert_new_line_after_annotation_on_field = true;
        cfOptions.insert_new_line_after_annotation_on_local_variable = true;
        cfOptions.insert_new_line_after_annotation_on_package = true;
        cfOptions.alignment_for_parameters_in_method_declaration = 1;
        cfOptions.alignment_for_arguments_in_method_invocation = 1;
        cfOptions.comment_insert_new_line_for_parameter = true;

        cfOptions.blank_lines_before_method = 1;
        cfOptions.number_of_empty_lines_to_preserve = 1;

//        cfOptions.setEclipseDefaultSettings();
//        modifyCFOptions(cfOptions);
        cfOptions.tab_char = DefaultCodeFormatterOptions.SPACE;

        CodeFormatter cf = new DefaultCodeFormatter(cfOptions, options);

        TextEdit te = cf.format(CodeFormatter.K_UNKNOWN, code, 0,
                code.length(), 0, null);
        IDocument dc = new Document(code);

        te.apply(dc);
        return dc.get();
    }
}