package com.caspco.spl.data.dataaccess.swift;


import com.caspco.spl.wrench.data.SwiftRepository;
import com.caspco.spl.wrench.utils.reflection.TypeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/7/15
 *         Time: 3:59 PM
 */
public class SwiftRepositoryFactoryBean<R extends QueryDslJpaRepository<T, I>, T, I extends Serializable> extends JpaRepositoryFactoryBean<R, T, I> {

    @Autowired(required = false)
    private TypeUtils classUtilsBean;

    protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
        return new MyRepositoryFactory(entityManager,classUtilsBean);
    }

    private static class MyRepositoryFactory extends JpaRepositoryFactory {
        private final EntityManager entityManager;
        private final TypeUtils classUtilsBean;

        public MyRepositoryFactory(EntityManager entityManager, TypeUtils classUtilsBean) {
            super(entityManager);
            this.entityManager = entityManager;
            this.classUtilsBean = classUtilsBean;
        }

        @Override
        protected Object getTargetRepository(RepositoryInformation information) {
            return new SwiftRepositoryImpl((JpaEntityInformation) this.getEntityInformation(information.getDomainType()), entityManager, classUtilsBean) {};
        }

        protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
            return SwiftRepository.class;
        }
    }
}