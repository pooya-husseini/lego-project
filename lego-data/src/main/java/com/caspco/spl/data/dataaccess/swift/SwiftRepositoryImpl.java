package com.caspco.spl.data.dataaccess.swift;

import com.caspco.spl.wrench.annotations.cg.ExactOnSearch;
import com.caspco.spl.wrench.annotations.data.FillWithCurrentDate;
import com.caspco.spl.wrench.annotations.data.HasEvents;
import com.caspco.spl.wrench.data.HistoricalEntity;
import com.caspco.spl.wrench.data.SwiftRepository;
import com.caspco.spl.wrench.utils.date.DateRange;
import com.caspco.spl.wrench.utils.reflection.ClassUtils;
import com.caspco.spl.wrench.utils.reflection.TypeUtils;
import com.google.common.collect.Lists;
import com.mysema.query.jpa.JPQLQuery;
import com.mysema.query.types.EntityPath;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.path.PathBuilder;
import com.mysema.query.types.path.StringPath;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.data.jpa.repository.support.CrudMethodMetadata;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/7/15
 *         Time: 2:04 PM
 */
@NoRepositoryBean
public class SwiftRepositoryImpl<T, ID extends Serializable> extends QueryDslJpaRepository<T, ID> implements SwiftRepository<T, ID> {

    private final EntityPath<T> path;
    private final PathBuilder<T> builder;
    private final EntityManager em;
    private final Querydsl querydsl;
    private final Class<T> typeArgument;
    private final TypeUtils typeUtils;


    public SwiftRepositoryImpl(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager, TypeUtils typeUtils) {
        this(entityInformation, entityManager, SimpleEntityPathResolver.INSTANCE, typeUtils);
    }

    public SwiftRepositoryImpl(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager,
                               EntityPathResolver resolver, TypeUtils classUtilsBean) {
        super(entityInformation, entityManager, resolver);
        this.typeUtils = classUtilsBean;
        this.typeArgument = entityInformation.getJavaType();
        this.path = resolver.createPath(typeArgument);

        this.builder = new PathBuilder<>(path.getType(), path.getMetadata());
        this.querydsl = new Querydsl(entityManager, builder);
        this.em = entityManager;

    }

    public JPQLQuery select() {
        return createQuery();
    }

    @Transactional()
    public int delete(Predicate predicate) throws InterruptedException {
        List<T> all = findAll(predicate);
        for (T t : all) {
            super.delete(t);
        }
        return all.size();
    }

    public T getEntityAtVersion(Number revision) {
        return (T) AuditReaderFactory.get(em).createQuery().forEntitiesAtRevision(typeArgument, revision).add(AuditEntity.revisionNumber().eq(revision)).getSingleResult();
    }

    public Number getLastRevisionNumber() {
        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
    }

    @Override
    public Number getLastDeletedRevisionNumber() {
        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionType().eq(RevisionType.DEL)).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
    }

    @Override
    public Number getLastModifiedRevisionNumber() {
        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionType().eq(RevisionType.MOD)).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
    }

    @Override
    public Number getLastInsertedRevisionNumber() {
        return (Number) AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionType().eq(RevisionType.ADD)).addProjection(AuditEntity.revisionNumber().max()).getSingleResult();
    }

    @Override
    public List<HistoricalEntity> getRevisionNumberModificationType(Number revision) {
        List<Object> result = AuditReaderFactory.get(em).createQuery().forRevisionsOfEntity(typeArgument, true, true).add(AuditEntity.revisionNumber().eq(revision)).addProjection(AuditEntity.revisionType()).addProjection(AuditEntity.id()).getResultList();
        List<HistoricalEntity> historicalEntities = new ArrayList<>();
        for (Object o : result) {
            if (o instanceof Object[]) {
                Object[] objects = (Object[]) o;
                if (objects[0] instanceof RevisionType) {
                    RevisionType revisionType = (RevisionType) objects[0];
                    HistoricalEntity.ModificationType modificationType = null;
                    switch (revisionType) {
                        case ADD:
                            modificationType = HistoricalEntity.ModificationType.INSERTED;
                            break;
                        case MOD:
                            modificationType = HistoricalEntity.ModificationType.MODIFIED;
                            break;
                        case DEL:
                            modificationType = HistoricalEntity.ModificationType.DELETED;
                            break;
                    }
                    historicalEntities.add(new HistoricalEntity(objects[1], modificationType));
                }
            }
        }
        return historicalEntities;
    }

    @Override
    public List<T> find(T e) {
        return findAll(makeEqualExpression(e));
    }

    private BooleanExpression makeEqualExpression(Object e) {
        return makeEqualExpression(null, e);
    }

    private BooleanExpression makeEqualExpression(String prefix, Object e) {
        Map<String, Object> map = typeUtils.objectToMap(e);
        Deque<Map.Entry<String, Object>> entries = new ArrayDeque<>(map.entrySet());
        BooleanExpression expression = null;
        if (entries.size() > 0) {
            Map.Entry<String, Object> entry = entries.pop();
            expression = getBooleanExpression(e.getClass(), prefix, entry);
            while ((entry = entries.poll()) != null) {
                if (expression != null) {
                    expression = expression.and(getBooleanExpression(e.getClass(), prefix, entry));
                } else {
                    expression = getBooleanExpression(e.getClass(), prefix, entry);
                }
            }
        }
        return expression;
    }

    private BooleanExpression getBooleanExpression(Class<?> type, String prefix, Map.Entry<String, Object> entry) {
        return getBooleanExpression(type, prefix, entry.getKey(), entry.getValue());
    }

    private BooleanExpression getBooleanExpression(Class<?> type, String prefix, String name, Object value) {
        String propertyName = (prefix == null || prefix.isEmpty() ? "" : prefix + ".") + name;
        BooleanExpression expression = null;
        ClassUtils classUtils = typeUtils.resolveType(type);
        if (classUtils.isPropertyAnnotatedWithAny(name, Arrays.asList(ManyToMany.class, OneToMany.class))) {
            Iterable<?> iterable = (Iterable<?>) value;
            ArrayDeque<Object> entries = new ArrayDeque<>(Lists.newArrayList(iterable));
            if (entries.size() > 0) {
                Object entry = entries.pop();
                expression = makeEqualExpression(propertyName, entry);
                while ((entry = entries.poll()) != null) {
                    expression = expression.or(makeEqualExpression(propertyName, entry));
                }
            }
        } else if (classUtils.isPropertyAnnotatedWithAny(name, Arrays.asList(ManyToOne.class, OneToOne.class))) {
            expression = getBooleanExpression(value, propertyName, value);
        } else if (classUtils.isPropertyAnnotatedWith(name, ExactOnSearch.class)) {
            expression = this.builder.get(propertyName).eq(value);
        } else {
            if (value instanceof Date) {
                Date date = (Date) value;
                DateRange dateRange = new DateRange(date);
                expression = this.builder.getDate(propertyName, Date.class).between(dateRange.getDayStart(), dateRange.getDayEnd());
            } else if (value instanceof String) {
                String s = (String) value;
                expression = this.builder.getString(propertyName).lower().like("%" + s.toLowerCase() + "%");
            } else {
                expression = this.builder.get(propertyName).eq(value);
            }
        }
        return expression;
    }

    private BooleanExpression getBooleanExpression(Object value, String propertyName, Object item) {
        BooleanExpression expression = null;
        Map<String, Object> objectMap = typeUtils.objectToMap(item);
        Deque<Map.Entry<String, Object>> entries = new ArrayDeque<>(objectMap.entrySet());
        if (entries.size() > 0) {
            Map.Entry<String, Object> entry = entries.pop();
            expression = getBooleanExpression(item.getClass(), propertyName, entry.getKey(), entry.getValue());

            while ((entry = entries.poll()) != null) {
                expression = expression.and(getBooleanExpression(value.getClass(), propertyName, entry.getKey(), entry.getValue()));
            }
        }
        return expression;
    }

    @Override
    public boolean exists(T entity) {
        return select().from(path).where(makeEqualExpression(entity)).exists();
    }

    @Override
    public long count(T entity) {
        return select().where(makeEqualExpression(entity)).count();
    }

    public void setRepositoryMethodMetadata(CrudMethodMetadata crudMethodMetadata) {
        throw new IllegalStateException();
    }

    @Override
    public <S extends T> S save(S entity) {
//        SwiftPreInsert.doEvent(entity);
        setPrePersist(entity);
        return super.save(entity);
    }

    @Override
    public <S extends T> List<S> save(Iterable<S> entities) {
        return super.save(entities);
    }

    public <S extends T> S saveAndFlush(S entity) {
        throw new IllegalStateException();
    }

    public void flush() {
        throw new IllegalStateException();
    }

    @Override
    public List<String> searchColumn(String column, String value) {
        StringPath string = builder.getString(column);
        return  select().where(string.like(value)).distinct().list(string);
    }

    public void setPrePersist(Object entity) {

        if (typeArgument.isAnnotationPresent(HasEvents.class)) {
            ClassUtils classUtils = typeUtils.resolveType(typeArgument);
            classUtils.findPropertiesAnnotatedWith(FillWithCurrentDate.class).forEach(s ->
                    typeUtils.setPropertyValue(entity, s, new Date())
            );
        }
    }

}