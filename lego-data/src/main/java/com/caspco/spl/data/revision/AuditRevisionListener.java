package com.caspco.spl.data.revision;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

/**
 * @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 11/9/15
 *         Time: 9:54 PM
 */
public class AuditRevisionListener implements RevisionListener {

    @Override
    public void newRevision(Object revisionEntity) {
        String userName;
        String remoteAddress = null;

        AuditRevisionEntity exampleRevEntity = (AuditRevisionEntity) revisionEntity;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
        if(auth.getDetails() instanceof WebAuthenticationDetails) {
            remoteAddress = ((WebAuthenticationDetails) auth.getDetails()).getRemoteAddress();
        }
        if (principal instanceof User) {
            userName = ((User) principal).getUsername();
        }else {
            userName = principal.toString();
        }
        exampleRevEntity.setIpAddress(remoteAddress);
        exampleRevEntity.setUsername(userName);
    }
}
