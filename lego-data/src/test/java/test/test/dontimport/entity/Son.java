package test.test.dontimport.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 09/12/14
 *         Time: 10:02
 */

@Entity
@Table(name = "Son")
@DiscriminatorValue("1")
public class Son extends Parent{

//    private Long id;
    private String family;

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column
//    public Long getId() {
//        return this.id;
//    }
//
//    public void setId(final Long id) {
//        this.id = id;
//    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return "Son{" +
                "family='" + family + '\'' +
                '}';
    }
}