package test.test.dontimport.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 09/12/14
 *         Time: 10:02
 */

@Entity
@Table(name = "Parent")
@Inheritance(strategy = InheritanceType.JOINED)
public class Parent {

    private Long id;
    @NotNull
    private String mainType;
    @NotNull
    private String code;
    @NotNull
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column
    public String getCode() {
        return this.code;
    }

    public void setCode(final String code) {
        this.code = code;
    }


    @Column
    public String getDescription() {

        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Column
    public String getMainType() {
        return this.mainType;
    }

    public void setMainType(final String mainType) {
        this.mainType = mainType;
    }

    @Override
    public String toString() {
        return "Parent{" +
                "id=" + id +
                ", mainType='" + mainType + '\'' +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}