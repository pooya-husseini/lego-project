package test.test.dontimport.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 04/01/15
 *         Time: 12:32
 */

@Entity
@Table(name = "DateEntity")
public class DateEntity {
    private Long id;

    @Temporal(TemporalType.DATE)
    private Date date;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "DateEntity{" +
                "id=" + id +
                ", date=" + date +
                '}';
    }
}
