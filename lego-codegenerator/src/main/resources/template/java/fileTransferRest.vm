package ${packageName}.rest;

#parse("/template/util/java-license.vm")

#parse("/template/util/string-utils.vm")
#parse("/template/util/model-utils.vm")


import ${packageName}.*;
import ${packageName}.repository.*;
import com.caspco.spl.wrench.annotations.cg.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Map;

#set($restUrl="/$packageName.replace('.','/')/fileuploader")
@RestController
@RequestMapping("$restUrl")
@Transactional
@Generated(comments = "Author of code generator is Pooya Husseini (pooya.husseini@gmail.com)")
public class FileTransferRestService {

    @Autowired
    private FileTransferRepository repository;

    @RequestMapping(method = RequestMethod.POST)
    public Long upload(MultipartHttpServletRequest request) throws IOException  {

        Map<String, MultipartFile> fileMap = request.getFileMap();
        for (Map.Entry<String, MultipartFile> item : fileMap.entrySet()) {
            MultipartFile file = item.getValue();
            String fileName = file.getOriginalFilename();

            if (!item.getValue().isEmpty()) {
                FileTransferEntity entity = new FileTransferEntity();
                entity.setContent(file.getBytes());
                entity.setName(fileName);
                entity.setContentType(file.getContentType());
                FileTransferEntity result = repository.save(entity);

                return result.getId();
            }
        }
        return null;
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public void download(@PathVariable Long id, HttpServletResponse response) throws IOException  {
        FileTransferEntity entity = repository.findOne(id);
        response.reset();
        response.setContentType(entity.getContentType());
        response.setHeader("Content-disposition", "attachment; filename=\"" + entity.getName() + "\"");
        response.addHeader("filename", entity.getName());
        response.getOutputStream().write(entity.getContent());
        response.getOutputStream().flush();
    }

    @ExceptionHandler
    public void handle(HttpMessageNotReadableException e) {
        // handle exception
    }
}