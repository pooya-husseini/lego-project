package com.caspco.spl.cg.bootstrap;

import java.util.ArrayList;
import java.util.List;

public class CodeGenerationConfiguration {

    private CGConfig cgConfig;
    private List<Class<?>> classes = new ArrayList<>();
    private List<Class<?>> enums = new ArrayList<>();

    private CodeGeneratorJobType task = CodeGeneratorJobType.GENERATE;

    public List<Class<?>> getClasses() {
        return classes;
    }

    public void setClasses(List<Class<?>> classes) {
        this.classes = classes;
    }

    public List<Class<?>> getEnums() {
        return enums;
    }

    public void setEnums(List<Class<?>> enums) {
        this.enums = enums;
    }

    public CodeGeneratorJobType getTask() {
        return task;
    }

    public void setTask(CodeGeneratorJobType task) {
        this.task = task;
    }

    public CGConfig getCgConfig() {
        return cgConfig;
    }

    public void setCgConfig(CGConfig cgConfig) {
        this.cgConfig = cgConfig;
    }
}