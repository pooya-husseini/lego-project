package com.caspco.spl.cg.bootstrap;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 17/12/14
 *         Time: 10:26
 */
public class CGConfig {
    private Map<BootstrapAttribute, Object> attributeMap = new HashMap<>();
    private String prefix;

    public void addConfiguration(BootstrapAttribute configurationType, Object value) {
        if (value != null) {
            attributeMap.put(configurationType, value);
        }
    }

    public <E> E getConfiguration(BootstrapAttribute attribute, Class<E> outputType) {
        return (E) getConfiguration(attribute);
    }

    public Object getConfiguration(BootstrapAttribute attributeType) {
        Object s = getSimpleConfiguration(attributeType);
        if (s != null) {
            if (s instanceof String) {
                if (("" + s).isEmpty()) {
                    return null;
                } else {
                    return prefix + s;
                }
            } else {
                return s;
            }
        } else {
            return null;
        }
    }

    public boolean containsConfiguration(BootstrapAttribute attributeType) {
        return attributeMap.containsKey(attributeType);
    }

    private String makeValidPath(String path) {
        path += File.separator;
        return path.replace(File.separator + File.separator, File.separator);
    }

    public Object getSimpleConfiguration(BootstrapAttribute attributeType) {
        return attributeMap.get(attributeType);
    }

    public Map<BootstrapAttribute, Object> getAttributeMap() {
        return attributeMap;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = makeValidPath(prefix);
    }

    public enum BootstrapAttribute {
        INPUT_PATH,
        SERVER_OUTPUT_PATH,
        SCALA_SERVER_OUTPUT_PATH,
        CLIENT_OUTPUT_PATH,
        SERVER_SIDE_RESOURCE_PATH,
        VALIDATE,
        MAIN_PAGE_TYPE_AHEAD,
        FORMAT_OUTPUT,
        TARGET_PATH,
        COMPRESS_JS
    }
}