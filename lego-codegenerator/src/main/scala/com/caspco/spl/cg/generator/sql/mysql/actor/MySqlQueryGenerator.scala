package com.caspco.spl.cg.generator.sql.mysql.actor

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 25/06/14
  *         Time: 14:29
  */
//object MySqlQueryGenerator {
//  def props: Props = Props(new MySqlQueryGenerator)
//
//
//}
//class MySqlQueryGenerator extends CodeGeneratorActor {
//  override def generateProject(p: Project, path: String): Unit = {
//    generateCreateTable(p, path)
//  }
//
//  def generateCreateTable(p: Project, path: String): Unit = {
//    p.getDataModelContainer.entityModels.foreach {
//      case dm =>
//        val result = TemplateEngine.velocity(CreateTablePath, Map("dm" -> dm.convert()))
//        val outputDir = makeValidPath(s"$path${/}$SqlPathPrefix${/}", dm.getAttributes.get("domain"))
//        new File(s"${outputDir}Create${dm.getAttribute("name")}Table.sql") <# result
//    }
//    p.getDataModelContainer.enumModels.foreach {
//      case dm =>
//        val result = TemplateEngine.velocity(CreateCollectionPath, Map("dm" -> dm.convert()))
//        val outputDir = makeValidPath(s"$path${/}$SqlPathPrefix${/}", dm.getAttributes.get("domain"))
//        new File(s"${outputDir}Create${dm.getAttribute("name")}Query.sql") <# result
//    }
//  }
//  def generateViewModels(project: Project, path: String): Unit={}
//  override def getGenerationUnits: Future[List[CodeGenerationUnit]] = ??? //todo needs revise
//}