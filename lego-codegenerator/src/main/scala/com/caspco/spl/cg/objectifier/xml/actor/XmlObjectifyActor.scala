package com.caspco.spl.cg.objectifier.xml.actor

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 1/29/14
 *         Time: 11:43 AM
 */

import akka.actor.{Actor, Props}
import com.caspco.spl.cg.model.datamodel._
import com.caspco.spl.cg.model.layout.{Cell, GridType, Row, _}
import com.caspco.spl.cg.model.project.Project
import com.caspco.spl.cg.model.view.{AbstractPage, AbstractWidget}
import com.caspco.spl.cg.objectifier.actor.ObjectifierActor.Objectify
import com.caspco.spl.cg.objectifier.xml.util.DomUtils.DomUtils
import org.slf4j.LoggerFactory

import scala.util.{Failure, Success, Try}
import scala.xml._




class XmlObjectifyActor extends Actor {
  val logger = LoggerFactory.getLogger(this.getClass)
  import java.io.File


  //  private var path = ""
  //  def apply(path: String): Future[Project] = {
  //    this.path = path
  //    val f = Future {
  //      {
  //        recursiveIterateDirectory(path)
  //        project
  //      }
  //    }
  //    f
  //  }
  def generateProject(path: String): Try[Project] = {
    val project = new Project

    def recursiveIterateDirectory(path: String): Unit = {
      logger.info(s"Path is $path")
      val src = new File(path)
      if (!src.exists()) {
        throw new IllegalArgumentException(s"Invalid input path $path")
      }

      src.listFiles().filter(p => p.getName.endsWith(".xml")).foreach {
        case x if x.isDirectory => recursiveIterateDirectory(x.getCanonicalPath)
        case x if !x.isDirectory =>
          logger.info(s"In path is ${x.getAbsolutePath}")
          val loadFile = XML.loadFile(x)

          loadFile.label match {
            case "DataModel" =>
              logger.info(s"${loadFile.label} found in $path")
              for (dm <- loadFile \\ loadFile.label) {
                val datamodel = generateDataModelGraph(dm)
                project += datamodel
              }
            case "application" =>
              logger.info(s"${loadFile.label} found in $path")
              for (dm <- loadFile \\ loadFile.label) {
                val app = generatePageModel(dm)
                project.getPageContainer.setApplication(app)
              }
            case "layout" =>
              logger.info(s"${loadFile.label} found in $path")
              for (dm <- loadFile \\ loadFile.label) {
                val layout = generateLayoutModelGraph(dm)
                project += layout
              }
            case "page" =>
              logger.info(s"${loadFile.label} found in $path")
              for (dm <- loadFile \\ loadFile.label) {
                val app = generatePageModel(dm)
                project += app
              }
            case _ =>
              logger.info(s"Unknown tag was found in $path")
          }
      }
    }
    Try({
      recursiveIterateDirectory(path)
      logger.info("Graph model objectified successfully!")
      project
    })
  }

  def generateDataModelGraph(dataModelNode: Node): DataModel = {

    val dm = new DataModel(false)
    dm += dataModelNode.getAttributeAsMap
    for (defType <- dataModelNode \ "type") {
      dm += generateDataModelGraph(defType)
    }

    for (e <- dataModelNode \ "elements"; domElem <- e.child) domElem match {
      case x: Elem =>
        val element = AbstractElement(x.label)

        element += x.getAttributeAsMap
        //        element.elemType = x.label

        for (r <- x \ "restriction"; attrs = r.getAttributeAsMap) {
          val restriction = new Restriction()
          restriction += attrs
          for (rest <- r.child) rest match {
            case x: Elem =>
              restriction += RestrictionValue(rest.label, rest.text)
            case _ =>
          }
          element += restriction
        }
        dm += element
      case x =>
    }
    dm
  }

  def generatePageModel(domApp: Node): AbstractPage = {

    val app = new AbstractPage
    app += domApp.getAttributeAsMap
    for (l <- domApp \ "layout") {
      app += generateLayoutModelGraph(l)
    }

    // Generates widgets that are without layout
    domApp.child.filter(p => p.label != "layout" && p.isInstanceOf[Elem]).foreach(w => {
      val widget = AbstractWidget(w.label)
      widget += w.getAttributeAsMap
      //      widget.widgetType = w.label
      for (l <- w \ "layout") {
        widget += generateLayoutModelGraph(l)
      }
      app += widget
    })
    app
  }

  def makeWidget(w: Node): AbstractWidget = {
    val widget = AbstractWidget(w.label)
    widget += w.getAttributeAsMap
    //    widget.widgetType = w.label
    val children = w \ "_"
    for (l <- children filter (_.label == "layout")) {
      widget += generateLayoutModelGraph(l)
    }

    for (inner <- children filterNot (_.label == "layout")) {
      widget += makeWidget(inner)
    }
    widget
  }

  def generateLayoutModelGraph(node: Node): Layout = {
    val layout = new Layout
    layout += node.getAttributeAsMap

    for (g <- node \ "grid") {
      val gridType = new GridType
      gridType += g.getAttributeAsMap

      for (r <- g \ "row") {
        val row = new Row
        row += r.getAttributeAsMap
        for (c <- r \ "cell") {
          val cell = new Cell
          cell += c.getAttributeAsMap
          for (w <- c.child.filter(p => p.isInstanceOf[Elem] && p.label!="layout")) {
            val widget = makeWidget(w)
            cell += widget
          }
          for (l <- c \ "layout") {
            cell += generateLayoutModelGraph(l)
          }
          row += cell
        }
        gridType += row
      }
      layout += gridType
    }
    layout
  }

  //  def getWidget(w: Node): Widget = {
  //    val widget = new Widget
  //    widget.attributes = w.getAttributeAsMap
  //    widget.widgetType = w.label
  //    for (ch <- w.child.filter(p => p.isInstanceOf[Elem])) {
  //      widget.innerWidgets :+= getWidget(ch)
  //    }
  //    widget
  //  }

  def receive: Actor.Receive = {
    case Objectify(path, jid) =>
      val project = generateProject(path)
      project match {
        case Success(s) =>
        case Failure(f)=>
//          sender ! ObjectifySuccess(s, jid)
//        case Failure(f) => sender ! ObjectifyFailure(f, jid)
      }
    //    case OperationSuccess =>
    //      logger.info("Success in objectifier actor")
    //      sender ! OperationSuccess
  }
}

object XmlObjectifyActor {
  def props=Props(new XmlObjectifyActor)
}