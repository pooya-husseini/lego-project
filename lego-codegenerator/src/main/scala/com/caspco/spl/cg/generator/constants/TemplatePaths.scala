package com.caspco.spl.cg.generator.constants

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 01/07/14
  *         Time: 16:11
  */
object TemplatePaths {

  lazy val TemplateFileExtension = ".vm"
  lazy val JAVAClassPath = ".vm"
  lazy val WebTemplatePath = "/template/web/bootstrap/"
  lazy val DtoTemplatePath = "/template/java/dto.vm"
  lazy val PasswordDtoTemplatePath = "/template/java/passwordDto.vm"
  lazy val DoTemplatePath = "/template/java/domainobject.vm"
  lazy val PrincipalToDtoTemplatePath = "/template/java/spring/principalToDtoTransformer.vm"
  lazy val PrincipalToEntityTemplatePath = "/template/java/spring/principalToEntityTransformer.vm"
  lazy val BusinessServiceTemplatePath = "/template/java/repository.vm"
  lazy val DomainServiceTemplatePath = "/template/java/service.vm"
  lazy val RestServiceTemplatePath = "/template/java/restService.vm"
  lazy val EntityTransformerTemplatePath = "/template/java/entitytransformer.vm"
  //  lazy val CacheFillerTemplatePath = "/template/java/cacheFiller.vm"
  lazy val CacheFillerTemplatePath = "/template/java/gpResolver.vm"
  lazy val DtoTransformerTemplatePath = "/template/java/dtotransformer.vm"
  lazy val TransformerTemplatePath = "/template/java/transformer.vm"
  lazy val JasperReportTemplatePath = "/template/report/main-page-jasper.vm" //    Locale.fa_IR.name() -> "/template/report/main-page-jasper.fa_IR.vm"

  lazy val GeneralParameterTemplatePath = "/template/java/generalParameter.vm"
  lazy val GeneralParameterTypeEnumTemplatePath = "/template/java/enums.vm"

  lazy val CreateTablePath = "/template/sql/mysql/createTable.vm"
  lazy val CreateCollectionPath = "/template/sql/mysql/createCollection.vm"
  lazy val SqlPathPrefix = s"sql"

  lazy val PageTemplate = "/template/web/bootstrap/page.vm"
  lazy val AppTemplate = "/template/web/bootstrap/app.vm"
  //  lazy val WIDGET_TEMPLATE = "/template/web/bootstrap/widget.ssp"
  lazy val DataModelTemplate = "/template/web/bootstrap/controller.vm"
  lazy val ServiceTemplate = "/template/web/bootstrap/service.vm"
  lazy val TranslationTemplate = "/template/web/bootstrap/translations.vm"
  lazy val ClientRestServiceTemplate = "/template/web/bootstrap/clientRestService.vm"
  lazy val FilterTemplate = "/template/web/bootstrap/filter.vm"
  lazy val AutoInjectorControllerTemplate = "/template/web/bootstrap/autoinjectorController.vm"
  lazy val ClientHttpServiceTemplate = "/template/web/bootstrap/httpservice.vm"
  lazy val ClientCrudServiceTemplate = "/template/web/bootstrap/crudService.vm"
  lazy val ClientCrudControllersTemplate = "/template/web/bootstrap/crudController.vm"
  lazy val FileUploaderServiceTemplate = "/template/web/bootstrap/fileUploadService.vm"

  lazy val FileTransferEntityTemplate = "/template/java/fileTransferEntity.vm"
  lazy val FileTransferRepositoryTemplate = "/template/java/fileTransferRepository.vm"
  lazy val FileTransferRestTemplate = "/template/java/fileTransferRest.vm"

  lazy val MenuTemplate = "/template/web/bootstrap/menu.vm"
  lazy val MainPage = "/template/web/bootstrap/mainPage.vm"
  lazy val MainSearchPage = "/template/web/bootstrap/mainSearchPage.vm"
  lazy val EditPage = "/template/web/bootstrap/editPage.vm"
  lazy val CreatePage = "/template/web/bootstrap/createPage.vm"
  lazy val SearchPage = "/template/web/bootstrap/searchPage.vm"
  lazy val ShowPage = "/template/web/bootstrap/showPage.vm"
  lazy val CrudPage = "/template/web/bootstrap/crudPage.vm"
  lazy val ChangePasswordPage = "/template/web/bootstrap/changePasswordPage.vm"

  lazy val SlickModelPath = "/template/scala/slickTable.vm"
}