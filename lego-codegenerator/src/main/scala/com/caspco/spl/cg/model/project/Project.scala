package com.caspco.spl.cg.model.project

import com.caspco.spl.cg.model.datamodel.{DataModel, DataModelContainer}
import com.caspco.spl.cg.model.layout.{Layout, LayoutContainer}
import com.caspco.spl.cg.model.view.{AbstractPage, PageContainer}


/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 2/2/14
 *         Time: 12:08 PM
 */
class Project {

  private val layouts = new LayoutContainer
  private val pages = new PageContainer
  private val dataModels = new DataModelContainer
  private var plainJavaObjects=List.empty[Class[_]]


  def getLayoutContainer = layouts

  def getDataModelContainer = dataModels

  def getPageContainer = pages

  def getPlainJavaObjects=plainJavaObjects

  def +=(l: Layout): Unit = layouts += l

  def +=(p: (AbstractPage,Option[List[String]])): Unit = pages += p
  def +=(p: AbstractPage): Unit = pages += p

  def +=(d: DataModel): Unit = dataModels += d

  def +=(c:Class[_]):Unit=plainJavaObjects+:=c
}