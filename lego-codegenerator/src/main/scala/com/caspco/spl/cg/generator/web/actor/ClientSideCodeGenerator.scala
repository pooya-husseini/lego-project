package com.caspco.spl.cg.generator.web.actor

import com.caspco.spl.cg.bootstrap.CGConfig
import com.caspco.spl.cg.bootstrap.CGConfig.BootstrapAttribute
import com.caspco.spl.cg.generator._
import com.caspco.spl.cg.generator.constants.OutputPathPrefixes._
import com.caspco.spl.cg.generator.constants.TemplatePaths._
import com.caspco.spl.cg.model.datamodel.{DataModel, EntityModel, EnumModel}
import com.caspco.spl.cg.model.project.Project
import com.caspco.spl.cg.model.view.{AbstractPage, ModelDescription}
import com.caspco.spl.cg.util.constant.Constants
import com.caspco.spl.cg.util.io.OutputStreamFactory
import com.caspco.spl.cg.util.template.TemplateEngine
import com.caspco.spl.wrench.annotations.cg.IgnoreType

import scala.collection.JavaConverters._


/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 2/12/14
  *         Time: 5:28 PM
  */

object ClientSideCodeGenerator extends CodeGenerationCompanion {
  //  lazy val locales = Locale.values().map(p => p.name()).toList
  override def apply(p: Project, config: CGConfig, writeInFile: Boolean): CodeGeneration = {
    new ClientSideGenerator(p, config, writeInFile)
  }
}

class ClientSideGenerator(p: Project, config: CGConfig, writeInFile: Boolean) extends CodeGeneration(config, writeInFile) {

  import com.caspco.spl.cg.util.io.StreamUtils._

  override lazy val path: String = config.getConfiguration(BootstrapAttribute.CLIENT_OUTPUT_PATH).asInstanceOf[String]
  //  val javaCodeGeneratorActor = JavaCodeGenerator.props

  override lazy val generationUnits: List[CodeGenerationUnit] = {
    Option(path) match {
      case Some(x) =>
        List(
          GenerateView,
          GenerateTranslations,
          GenerateController,
          GenerateMenu,
          GenerateClientRestService,
          GenerateAutoInjectProperties,
          GenerateService,
          //          GenerateControllerProperties,
          GenerateEnumModelsProperties,
          GenerateTranslations,
          GenerateClientCrudService,
          GenerateClientCrudControllers,
          GenerateFileTransferServices,
          GenerateAutoInjectService,
          GenerateFilters,
          GenerateChangePasswordPage
        )
      case None =>
        Nil
    }
  }

  //  object GenerateApplication extends CodeGenerationUnit {
  //    override lazy val modelAndPath: List[(AbstractPage, String)] = {
  //
  //      p.getPageContainer.getApplication match {
  //        case Some(app) =>
  //          List((app,s"${makeValidPath(path)}index.html"))
  //        case None =>
  //          List((null,s"${makeValidPath(path)}index.html"))
  //
  //      }
  //
  //    }
  //
  //    override def generate(): Unit = {
  //      logger.info("Started generating application")
  //      project.getPageContainer.getApplication match {
  //        case Some(app) =>
  //          val result = TemplateEngine.velocity(AppTemplate, Map("pages" -> project.getPageContainer))
  //          File(s"${makeValidPath(path)}index.html") <#! result
  //        case None =>
  //          val result = TemplateEngine.velocity(AppTemplate, Map("pages" -> project.getPageContainer))
  //          File(s"${makeValidPath(path)}index.html") <#! result
  //      }
  //      logger.info("Generating application done!")
  //    }
  //  }

  //  def getFilesWithLocale(prefix: String, postFix: String, locales: List[String]): List[java.io.File] = {
  //    locales.map(p => File(s"$prefix.$p.$postFix"))
  //  }

  object GenerateFileTransferServices extends CodeGenerationUnit {

    lazy val fileIdModels = p.getDataModelContainer.allModels.filter(_.getElements.exists(_.containsAttribute("fileId")))

    lazy val hasFileId = fileIdModels.nonEmpty

    lazy val packageName = p.getDataModelContainer.getPrecedencePackageName(fileIdModels.map(_.getAttribute("domain")))

    override lazy val modelAndPath: List[(Any, String)] = {
      if (hasFileId) {
        val outputDir = makeValidPath(s"$path${/}$IoServicePathPrefix", Some(packageName))
        List((AnyRef, s"${outputDir}FileTransferService.js"))
      } else {
        Nil
      }
    }

    override def generate(): Unit = {
      implicit val formatOutput = compressJs
      if (hasFileId) {
        modelAndPath.foreach {
          case (_, outPath) =>
            val result = TemplateEngine.velocity(FileUploaderServiceTemplate, Map("packageName" -> packageName))
            OutputStreamFactory(outPath) <# result
        }
      }
    }
  }

  object GenerateMenu extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      List((Nil, s"${makeValidPath(path)}app${/}conf${/}menu.json"),
        ("menu", s"$path${/}app${/}conf${/}translations${/}menu-%s.json"))
    }

    override def generate(): Unit = {
      implicit val formatOutput = compressJs
      val menus = p.getPageContainer.categorizeByPrefix
      val menusTranslation = p.getDataModelContainer.categorizeByPrefix
      val javaMap = new java.util.HashMap[String, java.util.List[ModelDescription]]()

      menus.foreach {
        case (k, v) => javaMap.put(k, v.asJava)
      }

      val result = TemplateEngine.velocity(MenuTemplate, Map("menus" -> javaMap))
      OutputStreamFactory(s"${makeValidPath(path)}app${/}conf${/}menu.json") <# result

      def generateMenuProperties(path: String): Unit = {
        //   val files = locales.map(p => (p, File(s"$path${/}app${/}resources${/}i18n${/}menu.$p.properties")))

        modelAndPath.foreach {
          case ("menu", outPath) =>
            val items = menusTranslation.toList.flatMap {
              case (k, v) =>
                v.flatMap {
                  case x =>
                    x.getTranslations.map {
                      case (locale, translation) =>
                        (locale, s""""${x.domain}.${x.name}":"$translation"""")
                    }
                }
            }
            val mapped = items.groupBy(_._1).map(a => (a._1, a._2.map(_._2))).map(a => (a._1, a._2 ++ menusTranslation.keys.map(k => s""""$k.header":"Menu"""")))

            mapped.foreach {
              case (locale, entries) =>
                val formattedPutPath = outPath.format(locale)
                val result = TemplateEngine.velocity(TranslationTemplate, Map("entries" -> entries.asJava, "locale" -> locale))
                OutputStreamFactory(formattedPutPath) <# result
            }
          case _ =>
        }
      }
      generateMenuProperties(path)
    }

  }
  object GenerateClientRestService extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.CLIENT_SIDE_REST)).map {
        case dm =>
          val outputDir = makeValidPath(s"$path${/}$RestServicePathPrefix", dm.getAttributeOption("domain"))
          val str = s"$outputDir${dm.getAttribute("name")}RestService.json"
          (dm, str)
      }
    }
    implicit val formatOutput = compressJs

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: DataModel, str) =>
          val relations = dm.searchableRelations.asJava
          val result = TemplateEngine.velocity(ClientRestServiceTemplate, Map("dm" -> dm, "relations" -> relations))
          OutputStreamFactory(str) <# result
      }
    }
  }

  object GenerateChangePasswordPage extends CodeGenerationUnit {

    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.allModels.find(p => p.containsAttribute("principal")) match {
        case None => Nil
        case Some(x) =>
          val outputDir = makeValidPath(s"$path${/}$WebPagesPathPrefix", x.getAttributeOption("domain"))
          val str = s"${outputDir}ChangePassword.html"
          List((Nil, str))
      }

    }

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (_, outPath: String) =>
          val result = TemplateEngine.velocity(ChangePasswordPage, Map.empty)
          OutputStreamFactory(outPath) <# result
      }
    }
  }

  object GenerateFilters extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.filter(p => p.containsAttribute("clientSideCacheable") && p.cacheKeyElements.nonEmpty).map {
        dm =>
          val outputDir = makeValidPath(s"$path${/}$FilterPathPrefix", dm.getAttributeOption("domain"))
          val str = s"$outputDir${dm.getAttribute("name")}Filter.js"
          (dm, str)
      }
    }
    implicit val formatOutput = compressJs

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: DataModel, str) =>
          val result = TemplateEngine.velocity(FilterTemplate, Map("dm" -> dm))
          OutputStreamFactory(str) <# result
      }
    }
  }


  object GenerateAutoInjectProperties extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      val models: List[EntityModel] = p.getDataModelContainer.entityModels.filter(p => p.containsAttribute("clientSideCacheable") && p.cacheKeyElements.isEmpty)
      p.getDataModelContainer.entityModels.filter(p => p.containsAttribute("clientSideCacheable") && p.cacheKeyElements.isEmpty)

      List((models, s"$path${/}${I18NResourcePath}AutoInject.%s.properties"))
    }

    override def generate(): Unit = {
      modelAndPath.foreach {
        //        case ((entities:List[EntityModel],Locale.fa_IR),outPath:String)=>
        //          entities.foreach(x=>{
        //            val str=new StringBuilder
        //            str ++= " لطفا"
        //            str ++= x.getAttribute("translation_fa_IR")
        //            str++= " را انتخاب نمایید "
        //            File(outPath) <<# s"${x.getUniqueName}:$str"
        //          })
        case (entities: List[EntityModel], outPath: String) =>
          entities.foreach(x => {
            x.getAttributes(Constants.TranslationPattern, removePattern = true).foreach {
              case (locale, translation) =>
                val str = s"""Please select the $translation"""
                OutputStreamFactory(outPath.format(locale)) <<# s"${x.getUniqueName}:$str"
            }
          })
      }
    }
  }

  object GenerateAutoInjectService extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      val models: List[EntityModel] = p.getDataModelContainer.entityModels.filter(p => p.containsAttribute("clientSideCacheable") && p.cacheKeyElements.isEmpty)
      if (models.nonEmpty) {
        val outputDir = makeValidPath(s"$path${/}$ControllerPathPrefix", models.head.getAttributeOption("domain"))
        val str = s"${outputDir}AutoInjectController.js"
        List((models, str))
      } else {
        Nil
      }
    }
    implicit val formatOutput = compressJs

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: List[DataModel], str) =>
          val result = TemplateEngine.velocity(AutoInjectorControllerTemplate, Map("models" -> dm.asJava))
          OutputStreamFactory(str) <# result
        case _ =>
      }
    }
  }

  object GenerateClientCrudControllers extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.map {
        dm =>
          val outputDir = makeValidPath(s"$path${/}$CrudControllersPathPrefix", dm.getAttributeOption("domain"))
          val str = s"$outputDir${dm.getAttribute("name")}CrudController.js"
          (dm, str)
      }
    }
    implicit val formatOutput = compressJs

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: DataModel, outPath) =>
          val result = TemplateEngine.velocity(ClientCrudControllersTemplate, Map("dm" -> dm))
          OutputStreamFactory(outPath) <# result
      }
    }
  }


  object GenerateClientCrudService extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.map {
        case dm =>
          val outputDir = makeValidPath(s"$path${/}$CrudServicePathPrefix", dm.getAttributeOption("domain"))
          val str = s"$outputDir${dm.getAttribute("name")}CrudService.json"
          (dm, str)
      }
    }
    implicit val formatOutput = compressJs

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: DataModel, outPath) =>
          val result = TemplateEngine.velocity(ClientCrudServiceTemplate, Map("dm" -> dm))
          OutputStreamFactory(outPath) <# result
      }
    }
  }

  object GenerateView extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getPageContainer.allPages.filterNot(p => p._1.isIgnoredWith(IgnoreType.UI)).map {
        case (page, templates) =>
          val outputDir = makeValidPath(s"$path${/}$WebPagesPathPrefix", page.getAttributeOption("domain"))
          val str = s"$outputDir${page.getAttribute("name")}.html"

          ((page, templates), str)
      }
    }

    override def generate(): Unit = {
      modelAndPath.foreach {

        case (pt: (AbstractPage, Option[List[String]]), xPath) =>
          val (page, templates) = pt
          val fullClassName = page.getAttributeOption("dataModel")
          val className = fullClassName match {
            case Some(x) if x.contains(".") => x.substring(x.lastIndexOf(".") + 1)
            case Some(x) => x
            case None => ""
          }

          val results = templates match {
            case Some(x) =>
              x.map {
                case template => TemplateEngine.velocity(s"$template",
                  Map("page" -> page, "className" -> className, "fullClassName" -> fullClassName.get))
              }
            case None =>
              List(TemplateEngine.velocity(PageTemplate, Map("page" -> page, "className" -> className,
                "fullClassName" -> fullClassName.get)))
          }

          results.foreach {
            case result => OutputStreamFactory(xPath) <# result
          }
        case _ =>
      }
    }
  }

  object GenerateController extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.filterNot(x => x.isIgnoredWith(IgnoreType.CLIENT_SIDE_CONTROLLERS)).map {
        case dm =>
          val outputDir = makeValidPath(s"$path${/}$ControllerPathPrefix", dm.getAttributeOption("domain"))
          val str = s"$outputDir${dm.getAttribute("name")}Controller.js"
          (dm, str)
      }
    }
    implicit val formatOutput = compressJs

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: EntityModel, s) =>
          val result = TemplateEngine.velocity(DataModelTemplate, Map("dm" -> dm))
          OutputStreamFactory(s) <# result
      }
    }
  }

  object GenerateService extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.filterNot(x => x.isIgnoredWith(IgnoreType.CLIENT_SIDE_SERVICES)).map {
        case dm =>
          val outputDir = makeValidPath(s"$path${/}$ServicePathPrefix", dm.getAttributeOption("domain"))
          val str = s"$outputDir${dm.getAttribute("name")}Service.js"
          (dm, str)
      }
    }
    implicit val formatOutput = compressJs

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: EntityModel, s) =>
          val result = TemplateEngine.velocity(ServiceTemplate, Map("dm" -> dm))
          OutputStreamFactory(s) <# result
      }
    }
  }

  object GenerateI18N extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      List((Nil, s"$path${ResourcePath}i18n.properties"))
    }

    override def generate(): Unit = {
      val additionalPath = modelAndPath.head._2
      val file = OutputStreamFactory(s"$additionalPath")
      ////      val enumsProps=GenerateEnumModelsProperties.modelAndPath.map {
      ////        case (_,x) =>
      ////          x.replace(I18NResourcePath, "")
      ////      }.toSet.mkString(":\n")
      //      file <<# enumsProps
      val pageProps = GenerateI18N.modelAndPath.map {
        case (_, x) =>
          x.replace(s"$path${/}$I18NResourcePath", "").replace(".%s.properties", "")
        //          val str = locales.map(l => propertyFilePath.replace(s".$l.properties", "")).minBy(_.size)
        //          val replace =
      }.toSet.mkString(":\n")

      //      val autoInjectProperties=GenerateAutoInjectProperties.modelAndPath.map{
      //        case (_,x)=>
      //          val propertyFilePath = x.replace(s"$path${/}$I18NResourcePath", "")
      //          locales.map(l => propertyFilePath.replace(s".$l.properties", "")).minBy(_.size)
      //      }.toSet.mkString(":\n")

      file <<# s"$pageProps"
    }
  }


  object GenerateTranslations extends CodeGenerationUnit {

    //    override def apply(p: Project, path: String): Unit = {
    //      p.getPageContainer.popupPagesForCreation.foreach {
    //        case (page, _) =>
    //          val name=page.getUniqueName.replace("Create","") //todo change this crap
    //        val files = getFilesWithLocale(s"$path${/}${I18NResourcePath}view.$name",
    //            "properties", locales)
    //          page.getPropertyAttributes.foreach {
    //            case prop =>
    //              files <<# s"${prop._1}=${prop._2}"
    //          }
    //          //todo recursive widget property generation
    //          val widgets = page.getLayoutWidgets
    //          widgets.foreach {
    //            case w =>
    //              val props = w.getPropertyAttributes
    //              props.foreach {
    //                case prop =>
    //                  val prefix=w.getAttributeOrElse("name")
    //                  val propStr = s"$prefix.${prop._1}=$prefix" // todo Change right operand of = to _2
    //                  files <<# propStr
    //              }
    //          }
    //      }
    //    }


    override lazy val modelAndPath: List[(Any, String)] = {

      p.getDataModelContainer.allModels.map {
        case model =>
          //          (model, s"${I18NResourcePath}view.${model.getUniqueName}")
          val name = model.getUniqueName
          ((model, s"$name"), s"$path${/}${TranslationsPath}${name}-%s.json")
      }
    }


    override def generate(): Unit = {

      modelAndPath.foreach {
        case ((model: DataModel, keyPrefix), pathPattern) =>
          //          val files = getFilesWithLocale(, locales).map(p=>)
          //          val files = locales.map(p => (p, File(s"$path${/}${I18NResourcePath}view.$name.$p.properties"))).toMap
          //          model.getPropertyAttributes.foreach {
          //            case prop =>
          //              files <<# s"${prop._1}=${prop._2}"
          //          }
          val entries = model.getAttributes(Constants.TranslationPattern, removePattern = true).collect {
            case (locale, translation) =>
              (locale, s""""label":"$translation"""")
          }.toList ++ model.getElements.flatMap {
            case element =>
              val props = element.getPropertyAttributes
              props.flatMap {
                case prop =>
                  val prefix = element.getAttributeOrElse("name")

                  element.getAttributes(Constants.TranslationPattern, removePattern = true).map {
                    case (locale, translation) =>
                      (locale, s""""$prefix.${prop._1}":"$translation"""")
                    //                      OutputStreamFactory(pathPattern.format(locale)) <<# propStr
                  }
              }
          }


          val mapped = entries.groupBy(_._1).map(a => (a._1, a._2.map(_._2))) /*++ List(s"'prefix':'$keyPrefix'")*/

          mapped.foreach {
            case (locale, items) =>
              val result = TemplateEngine.velocity(TranslationTemplate, Map("entries" -> items.asJava, "locale" -> locale))
              OutputStreamFactory(pathPattern.format(locale)) <# result
          }


      }
    }

  }

  object GenerateEnumModelsProperties extends CodeGenerationUnit {

    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.enumModels.map {
        case enumModel =>
          //          locales.map(p => (enumModel,
          //            s"$path${/}${I18NResourcePath}view.${enumModel.getUniqueName}.$p.properties"))
          ((enumModel, s"${enumModel.getUniqueName}.%s.js"), s"$path${/}${TranslationsPath}${enumModel.getUniqueName}-%s.json")
        //        case _ =>Nil
      }
    }

    override def generate(): Unit = {
      modelAndPath.foreach {
        case ((enumModel: EnumModel, keyPrefix), pathPattern) =>
          val items = enumModel.getElements.flatMap {
            case e =>
              e.getAttributes(Constants.TranslationPattern, removePattern = true).map {
                case (locale, translation) =>
                  (locale,s""""${e.getAttribute("name")}"="$translation"""")
              }
          }.groupBy(_._1).map(a => (a._1, a._2.map(_._2)))
          items.foreach {
            case (locale, entries) =>
              val result = TemplateEngine.velocity(TranslationTemplate, Map("entries" -> entries.asJava, "prefix" -> keyPrefix, "locale" -> locale))
              OutputStreamFactory(pathPattern.format(locale)) <# result
          }
      }
    }
  }
}