package com.caspco.spl.cg.util.formatter

import com.caspco.spl.cg.util.io.StreamUtils
import org.mozilla.javascript.{Context, Scriptable}

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: ${Date}
  *         Time: ${time}
  */
object HtmlBeautifier extends CodeBeautifier {

  val BEAUTIFY_SCRIPT_PATH: String = "js/html-beautify.min.js"

  override def apply(input: String): String = {

    val source = StreamUtils.using(Thread.currentThread().getContextClassLoader.getResourceAsStream(BEAUTIFY_SCRIPT_PATH)) {
      case x => scala.io.Source.fromInputStream(x).getLines().mkString("\n")
    }
    val cx: Context = Context.enter
    val scope: Scriptable = cx.initStandardObjects

    cx.evaluateString(scope, "var global = {};", "global", 1, null)
    cx.evaluateString(scope, "var options = {max_preserve_newlines:2,wrap_line_length:120};", "options", 2, null)
    cx.evaluateString(scope, source, "beautify", 1, null)
    scope.put("source", scope, input)

    cx.evaluateString(scope, "result = global.html_beautify(source,options);", "beautify", 1, null)
    scope.get("result", scope).asInstanceOf[String]
  }
}