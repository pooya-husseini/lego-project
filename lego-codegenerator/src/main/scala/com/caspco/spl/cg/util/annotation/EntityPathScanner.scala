//
//import javax.persistence.Entity
//
//import grizzled.slf4j.Logger
//import org.reflections.Reflections
//import org.reflections.scanners.TypeAnnotationsScanner
//import org.reflections.util.{ClasspathHelper, ConfigurationBuilder, FilterBuilder}
//import org.springframework.beans.factory.config.BeanDefinition
//import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
//import org.springframework.core.`type`.filter.AnnotationTypeFilter
//import org.springframework.util.ClassUtils
//import com.caspco.infrastructure.xview.util.logging.StopWatch._
//
//import scala.collection.JavaConverters._
//
///**
// * @author : Пуя Гуссейни
// *         Email : info@pooya-hfp.ir
// *         Date: 19/04/14
// *         Time: 13:09
// */
//object EntityPathScanner {
//  private val logger = Logger[this.type]
//
//  def scanPath(packagePrefix: String): Iterable[Class[_]] = {
//    oldScanPath(packagePrefix)
//  }
//
//  def newScanPath(packagePrefix: String): Iterable[Class[_]] = {
//    val filters = new FilterBuilder().include(s"${packagePrefix}$$.*")
//    val reflections = new Reflections(
//      new ConfigurationBuilder().filterInputsBy(filters)
//      .setScanners(new TypeAnnotationsScanner().filterResultsBy(filters))
//      .setUrls(ClasspathHelper.forJavaClassPath())
//    )
//    //    val reflections = new Reflections(packagePrefix)
//    reflections.getTypesAnnotatedWith(classOf[Entity]).asScala
//  }
//
//  def oldScanPath(packagePrefix: String): List[Class[_]] = {
//    var beanDefinitions = Iterable.empty[BeanDefinition]
//    stopWatch {
//      val componentProvider = new ClassPathScanningCandidateComponentProvider(false)
//      componentProvider.addIncludeFilter(new AnnotationTypeFilter(classOf[javax.persistence.Entity]))
//      val result= componentProvider.findCandidateComponents(packagePrefix)
//      beanDefinitions=result.asScala
//      logger.info(s"beans are ${result.size()}")
//    } match {
//      case x => logger.info(s"Finding candidate proceed in $x milliseconds!")
//    }
//
//    var result = List.empty[Class[_]]
//    stopWatch {
//      result = beanDefinitions.map {
//        case beanDefinition: BeanDefinition =>
//          logger.info("bean name is " + beanDefinition.getBeanClassName)
//          ClassUtils.forName(beanDefinition.getBeanClassName, ClassUtils.getDefaultClassLoader)
//      }.toList
//    } match {
//      case x => logger.info(s"Converting result of bean scanning proceed in $x milliseconds")
//    }
//    result
//  }
//}