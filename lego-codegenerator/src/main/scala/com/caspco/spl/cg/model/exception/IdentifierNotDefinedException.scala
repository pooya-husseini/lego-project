package com.caspco.spl.cg.model.exception

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 2/3/14
 *         Time: 3:26 PM
 */
class IdentifierNotDefinedException (message:String) extends ModelException(message) {

}
