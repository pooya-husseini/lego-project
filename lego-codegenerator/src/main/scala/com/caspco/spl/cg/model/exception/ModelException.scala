package com.caspco.spl.cg.model.exception

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 2/2/14
 *         Time: 12:23 PM
 */
class ModelException(message: String, cause: Throwable) extends Exception(message, cause) {
  def this(m: String) = this(m, null)
}
