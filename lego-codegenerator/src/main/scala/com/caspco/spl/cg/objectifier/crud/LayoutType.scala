package com.caspco.spl.cg.objectifier.crud

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: 12.03.15
  *         Time: 20:16
  */
abstract class LayoutType(count: Int) {
  def getCount = count
}

case object OneColumn extends LayoutType(1)

case object TwoColumns extends LayoutType(2)

case object ThreeColumns extends LayoutType(3)

case object FourColumns extends LayoutType(4)
