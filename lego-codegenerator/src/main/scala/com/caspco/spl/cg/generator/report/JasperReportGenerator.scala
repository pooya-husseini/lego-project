package com.caspco.spl.cg.generator.report

import com.caspco.spl.cg.bootstrap.CGConfig
import com.caspco.spl.cg.bootstrap.CGConfig.BootstrapAttribute
import com.caspco.spl.cg.generator.constants.TemplatePaths._
import com.caspco.spl.cg.generator.{CodeGeneration, CodeGenerationCompanion, CodeGenerationUnit}
import com.caspco.spl.cg.model.datamodel.EntityModel
import com.caspco.spl.cg.model.project.Project
import com.caspco.spl.cg.util.constant.Constants
import com.caspco.spl.cg.util.io.OutputStreamFactory
import com.caspco.spl.cg.util.io.StreamUtils._
import com.caspco.spl.cg.util.template.TemplateEngine

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 03/12/14
  *         Time: 13:28
  */
class JasperReportGenerator(p: Project, pathConfig: CGConfig, writeInFile: Boolean) extends CodeGeneration(pathConfig, writeInFile) {

  override lazy val path: String = pathConfig.getConfiguration(BootstrapAttribute.SERVER_SIDE_RESOURCE_PATH).asInstanceOf[String]

  override lazy val generationUnits: List[CodeGenerationUnit] = {
    Option(path) match {
      case Some(x) =>
        List(GenerateReport)
      case None => Nil
    }
  }

  object GenerateReport extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.map {
        case dm =>
          val prefixPath = s"${path}reports${/}"
          val outPath = makeValidPath(prefixPath, dm.getAttributeOption("domain"))
          (dm, s"$outPath${dm.getAttribute("name")}.jrxml")
      }
    }

    override def generate(): Unit = {
      modelAndPath foreach {
        case (dm: EntityModel, outPath) =>
          dm.getAttributes(Constants.TranslationPattern, removePattern = true).foreach {
            case (locale, translation) =>
              val result = TemplateEngine.velocity(JasperReportTemplatePath, Map("dm" -> dm))
              OutputStreamFactory(outPath) <# result
          }
      }
    }
  }

}

object JasperReportGenerator extends CodeGenerationCompanion {
  //  lazy val locales = Locale.values().map(p => p.name()).toList

  override def apply(p: Project, pathConfig: CGConfig, writeInFile: Boolean): CodeGeneration = {
    new JasperReportGenerator(p, pathConfig, writeInFile)
  }
}