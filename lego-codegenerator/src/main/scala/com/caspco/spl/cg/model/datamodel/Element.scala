package com.caspco.spl.cg.model.datamodel

import com.caspco.spl.cg.model.BaseModel

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 1/29/14
  *         Time: 12:35 PM
  */

//@deprecated("Use AbstractElement subtypes")
//class Element(var elemType: String = "",
//              var restrictions: Option[Restriction] = None,
//              @BeanProperty var attributes: Map[String, String] = Map.empty[String, String])
//  extends BaseModel {
//  override def containsElement(elemName: String): Boolean = ???
//
//  override def containsAttribute(attName: String): Boolean = attributes.contains(attName)
//}

abstract class AbstractElement(private var elemType: String) extends BaseModel {

  private var value: Option[String] = None
  private var restrictions: Option[Restriction] = None

  override def containsElement(elemName: String): Boolean = ???

  def +=(restriction: Restriction): Unit = {
    restrictions = Some(restriction)
  }

  def getElemType = elemType

  def isCompositeType = false

  def isIdentifier: Boolean = containsAttribute("identifier")

  def setIdentifier(b: Boolean): Unit = this +=("identifier", "")

  def isGeneratedValue: Boolean = containsAttribute("generatedValue")

  def setValue(value: Option[String]): AbstractElement = {
    this.value = value
    this
  }

  def getValue = value
}

object AbstractElement {
  def apply(elemType: String): AbstractElement = elemType match {
    case "integer" => new IntegerType
    case "boolean" => new BooleanType
    case "list" => new ListType
    case "map" => new MapType
    case "composition" => new CompositionType(Map.empty[String, String])
    //    case "datamodel" => new DataModelType(None,Map.empty[String, String])
    case "dateTime" => new DateTimeType
    case "currency" => new CurrencyType
    case "decimal" => new DecimalType
    case "file" => new FileType
    case "double" => new DoubleType
    case "long" => new LongType
    case "string" => new StringType(Map.empty[String, String])
  }
}


object AbstractElementMapper {
  lazy val GpPattern="^generalparamter\\(\"(\\w+)\"\\)$".r

  def apply(elemType: String): Option[AbstractElement] = Option(elemType match {
    case "integer" => new IntegerType
    case "boolean" => new BooleanType
    case "list" => new ListType
    case "map" => new MapType
    case "composition" => new CompositionType(Map.empty[String, String])
    //    case "datamodel" => new DataModelType(None,Map.empty[String, String])
    case "dateTime" => new DateTimeType
    case "currency" => new CurrencyType
    case "decimal" => new DecimalType
    case "file" => new FileType(Map())
    case "double" => new DoubleType
    case "long" => new LongType
    case "string" => new StringType(Map.empty[String, String])
    case GpPattern(t)=>
      new StringType(Map("generalParameter"->t))
    case _ => null
  })
}

//IntegerType,BooleanType,ListType,MapType,DateTimeType,CurrencyType,DecimalType,FileType,DoubleType,
//LongType,StringType,CompositionType,

case class IntegerType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement("integer") {
  this += initAttributes
}

case class BooleanType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement("boolean") {
  this += initAttributes
}

case class ListType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement("list") {
  this += initAttributes
}

case class MapType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement("map") {
  this += initAttributes
}

case class DateTimeType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement(
  "dateTime") {
  this += initAttributes
}

case class DateType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement(
  "date") {
  this += initAttributes
}

case class CurrencyType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement(
  "currency") {
  this += initAttributes
}

case class DecimalType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement(
  "decimal") {
  this += initAttributes
}

case class FileType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement("file") {
  this += initAttributes
}

case class DoubleType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement(
  "double") {
  this += initAttributes
}

case class LongType(initAttributes: Map[String, String] = Map.empty[String, String]) extends AbstractElement(
  "long") {
  this += initAttributes
}

case class StringType(initAttributes: Map[String, String]) extends AbstractElement("string") {
  this += initAttributes
}

case class FloatType(initAttributes: Map[String, String]) extends AbstractElement("float") {
  this += initAttributes
}

case class CompositionType(var initAttributes: Map[String, String]) extends AbstractElement("composition") {
  this += initAttributes

  override def isCompositeType: Boolean = true
}

//case class DataModelType(var dataModel:Option[DataModel], var initAttributes: Map[String,
//  String]) extends AbstractElement("datamodel") {
//  this += initAttributes
//  override def isCompositeType: Boolean = true
//  def getDataModel=dataModel
//}