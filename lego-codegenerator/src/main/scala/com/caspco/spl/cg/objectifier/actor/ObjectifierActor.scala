package com.caspco.spl.cg.objectifier.actor

import akka.actor.Actor
import com.caspco.spl.cg.bootstrap.CodeGenerationConfiguration
import com.caspco.spl.cg.model.project.Project
import com.caspco.spl.cg.objectifier.actor.ObjectifierActor._
import com.caspco.spl.cg.objectifier.crud.EntityObjectifier

import scala.util.{Failure, Success, Try}

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 02/12/14
  *         Time: 12:38
  */
class ObjectifierActor extends Actor {

  override def receive: Receive = {
    case ObjectifyClasses(conf, jid) =>

      val process: ObjectifierProcess = new EntityObjectifier(conf, false)
      Try {
        process.makeProject()
      } match {
        case Success(p) =>
          context.parent ! ObjectifySuccess(conf, p, jid)
        case Failure(f) =>
          context.parent ! ObjectifyFailure(f, jid)
      }
    case ObjectifyClassesForClean(conf, jid) =>

      val process: ObjectifierProcess = new EntityObjectifier(conf, true)
      Try {
        process.makeProject()
      } match {
        case Success(p) =>
          context.parent ! ObjectifySuccessForClean(conf, p, jid)
        case Failure(f) =>
          context.parent ! ObjectifyFailure(f, jid)
      }
  }
}

object ObjectifierActor {

  case class Objectify(path: String, jobId: Int)

  case class ObjectifyClasses(conf: CodeGenerationConfiguration, jobId: Int)

  case class ObjectifyClassesForClean(conf: CodeGenerationConfiguration, jobId: Int)

  case class ObjectifySuccess(conf: CodeGenerationConfiguration, p: Project, jobId: Int)

  case class ObjectifySuccessForClean(conf: CodeGenerationConfiguration, p: Project, jobId: Int)

  case class ObjectifyFailure(t: Throwable, jobId: Int)

}

abstract class ObjectifierProcess(conf: CodeGenerationConfiguration) {
  def makeProject(): Project
}