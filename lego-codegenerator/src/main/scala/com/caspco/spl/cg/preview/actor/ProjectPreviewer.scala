package com.caspco.spl.cg.preview.actor

import com.caspco.spl.cg.bootstrap.CGConfig
import com.caspco.spl.cg.generator.{CodeGeneration, CodeGenerationCompanion, CodeGenerationUnit}
import com.caspco.spl.cg.model.project.Project


/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 06/12/14
  *         Time: 15:38
  */

class ProjectPreviewer(p: Project, pathConfig: CGConfig) extends CodeGeneration(pathConfig, false) {

  override lazy val generationUnits = List.empty

  object StartServer extends CodeGenerationUnit {
    override def generate(): Unit = {

    }
  }

  //  override def receive: Receive = {
  //    case Preview(p,config)=>
  //
  //  }
}


object ProjectPreviewer extends CodeGenerationCompanion {

  override def apply(p: Project, config: CGConfig, writeInFile: Boolean): CodeGeneration = {
    new ProjectPreviewer(p, config)
  }

  case class Preview(p: Project, config: CGConfig, jId: Int)

  case class PreviewStopped(jId: Int)

}