package com.caspco.spl.cg.util.template

import java.io.StringWriter

import com.caspco.spl.cg.generator.web.conversion.WebWidget
import com.caspco.spl.cg.util.string.ConstantStringMaker
import com.caspco.spl.wrench.locale.LocaleService
import mojolly.inflector.Inflector
import org.apache.velocity.VelocityContext
import org.apache.velocity.app.VelocityEngine
import org.apache.velocity.runtime.RuntimeConstants
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader
import org.apache.velocity.tools.generic.DateTool


/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 2/12/14
  *         Time: 8:35 PM
  */
object TemplateEngine {

  private lazy val ve = new VelocityEngine()
  ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath")
  ve.setProperty("classpath.resource.loader.class", classOf[ClasspathResourceLoader].getName)
  ve.init()

  def apply(path: String, attributes: Map[String, Any]): String = {
    velocity(path, attributes)
  }

  def velocity(path: String, attributes: Map[String, Any]): String = {
    val t = ve.getTemplate(path, "UTF-8")

    val context = new VelocityContext()
    attributes.foreach {
      case (k, v) => context.put(k, v)
    }
    //    val e = new EscapeTool
    context.put("inflector", Inflector)
    context.put("WidgetConverter", WebWidget)
    context.put("ConstantMaker", ConstantStringMaker)
    context.put("Locale", new LocaleService())
    context.put("date", new DateTool())
    context.put("version", "2.5-SNAPSHOT")

    val writer = new StringWriter()
    t.merge(context, writer)
    writer.toString
  }
}