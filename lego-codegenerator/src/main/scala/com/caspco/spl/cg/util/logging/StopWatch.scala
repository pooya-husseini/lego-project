package com.caspco.spl.cg.util.logging

import org.slf4j.LoggerFactory

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 18/05/14
 *         Time: 11:33
 */
object StopWatch {
  val logger = LoggerFactory.getLogger(this.getClass)
  def stopWatch(body: => Unit): Long = {
    val start = System.currentTimeMillis
    body
    System.currentTimeMillis - start
  }

  def stopWatchWithResult[A](body: => A): (A, Long) = {
    val start = System.currentTimeMillis
    val result = body
    (result, System.currentTimeMillis - start)
  }
}