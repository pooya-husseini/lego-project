package com.caspco.spl.cg.generator.javacg.actor

import com.caspco.spl.cg.bootstrap.CGConfig
import com.caspco.spl.cg.bootstrap.CGConfig.BootstrapAttribute
import com.caspco.spl.cg.generator.constants.TemplatePaths._
import com.caspco.spl.cg.generator.javacg.conversion.JavaTypeConversion._
import com.caspco.spl.cg.generator.{CodeGeneration, CodeGenerationCompanion, CodeGenerationUnit}
import com.caspco.spl.cg.model.datamodel.{DataModel, EntityModel, EnumModel}
import com.caspco.spl.cg.model.project.Project
import com.caspco.spl.cg.util.constant.Constants
import com.caspco.spl.cg.util.io.OutputStreamFactory
import com.caspco.spl.cg.util.io.StreamUtils._
import com.caspco.spl.cg.util.template.{PojoClassToFile, TemplateEngine}
import com.caspco.spl.wrench.annotations.cg.IgnoreType
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 29/04/14
  *         Time: 14:36
  */


class JavaCodeGenerator(p: Project, pathConfig: CGConfig, writeInFile: Boolean) extends CodeGeneration(pathConfig, writeInFile) {

  override lazy val path: String = pathConfig.getConfiguration(BootstrapAttribute.SERVER_OUTPUT_PATH).asInstanceOf[String]
  override lazy val generationUnits: List[CodeGenerationUnit] = {

    Option(path) match {
      case Some(x) =>
        List(GenerateDto,
          GenerateRepositories,
          GenerateServices,
          GenerateRestServices,
          GenerateDtoTransformer,
          GenerateEntityTransformer,
          //          GenerateDomainObject,
          GenerateExtraEntities,
          GenerateExtraEnums,
          GenerateFileTransfer,
          GenerateCacheFiller,
          GenerateI18N,
          GeneratePrincipals,
          GeneratePasswordDto
        )
      case None => Nil
    }
  }
  val logger = LoggerFactory.getLogger(this.getClass)

  object GenerateI18N extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      Option(pathConfig.getConfiguration(BootstrapAttribute.SERVER_SIDE_RESOURCE_PATH).asInstanceOf[String]) match {
        case Some(i18nPath) =>
          p.getDataModelContainer.allModels.map {
            case model =>
              val name = model.getAttribute("name")
              val validPath = makeValidPath(s"${i18nPath}i18n", model.getAttributeOption("domain"))
              (model, s"$validPath${name}_%s.properties")
          } ++ List((p.getDataModelContainer.allModels, s"${i18nPath}i18n${/}GeneralLabels_%s.properties"))
        case None => Nil
      }
    }

    override def generate(): Unit = {
      modelAndPath.collect {
        case (models: List[DataModel], pathPattern) =>
          models.foreach {
            _.getAttributes(Constants.TranslationPattern, removePattern = true).foreach {
              case (locale, _) =>
                val propStr = s"report.main=Main Report"
                OutputStreamFactory(pathPattern.format(locale)) <# propStr //todo configure this type of making main properties
            }
          }
        case (model: DataModel, pathPattern) =>
          model.getAttributes(Constants.TranslationPattern, removePattern = true).foreach {
            case (locale, translation) =>
              val propStr = s"label=$translation"
              OutputStreamFactory(pathPattern.format(locale)) <<# propStr
          }
          model.getElements.foreach {
            case element =>
              val props = element.getPropertyAttributes
              props.foreach {
                case prop =>
                  val prefix = element.getAttributeOrElse("name")
                  element.getAttributes(Constants.TranslationPattern, removePattern = true).foreach {
                    case (locale, translation) =>
                      val propStr = s"$prefix.${prop._1}=$translation"
                      OutputStreamFactory(pathPattern.format(locale)) <<# propStr
                  }
              }
          }
      }
    }
  }

  object GenerateFileTransfer extends CodeGenerationUnit {

    lazy val fileIdModels = p.getDataModelContainer.allModels.filter(_.getElements.exists(_.containsAttribute("fileId")))

    lazy val hasFileId = fileIdModels.nonEmpty

    lazy val packageName = p.getDataModelContainer.getPrecedencePackageName(fileIdModels.map(_.getAttribute("domain")))

    override lazy val modelAndPath: List[(Any, String)] = {
      if (hasFileId) {
        val outPath = makeValidPath(path, Some(packageName))
        val fileTransferRepository = s"${outPath}repository${/}FileTransferRepository.java"
        val fileTransferRestService = s"${outPath}rest${/}FileTransferRestService.java"
        val fileTransferEntity = s"$outPath${/}FileTransferEntity.java"

        List(
          (FileTransferRepositoryTemplate, fileTransferRepository),
          (FileTransferRestTemplate, fileTransferRestService),
          (FileTransferEntityTemplate, fileTransferEntity)
        )
      } else {
        Nil
      }
    }

    override def generate(): Unit = {
      if (hasFileId) {
        modelAndPath.foreach {
          case (template: String, outPath: String) =>
            val result = TemplateEngine.velocity(template, Map("packageName" -> packageName))
            OutputStreamFactory(outPath) <# result
        }
      }
    }
  }

  object GenerateExtraEntities extends CodeGenerationUnit {

    override lazy val modelAndPath: List[(Any, String)] = {

      p.getPlainJavaObjects.filter(!_.isEnum).map(x => {
        val packageName = if (x.getPackage == null) x.getName.substring(0, x.getName.lastIndexOf("."))
        else x.getPackage.getName
        val outPath = makeValidPath(path, Some(packageName))
        val p = s"$outPath${x.getSimpleName}".replace(".", "/").concat(".java")
        (new DataModel(false), p)
      })

    }

    def generate(): Unit = {

      p.getPlainJavaObjects.filter(!_.isEnum).foreach(x => {
        makeJavaFileFromObject(x, path)
      })
    }

    private def makeJavaFileFromObject(x: Class[_], path: String): Unit = {
      val packageName = if (x.getPackage == null) x.getName.substring(0, x.getName.lastIndexOf("."))
      else x.getPackage.getName
      val outPath = makeValidPath(path, Some(packageName))
      val classPath = packageName.replace(".", /) + / + x.getSimpleName + ".class"
      PojoClassToFile.generateFromClassFile(classPath, s"$outPath${x.getSimpleName}.java")
    }
  }

  object GenerateRepositories extends CodeGenerationUnit {

    override lazy val modelAndPath: List[(Any, String)] = {

      p.getDataModelContainer.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.SERVER_SIDE_SERVICES))
        .map {
          case dm =>
            val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
            val s = s"${outPath}repository${/}${dm.getAttribute("name")}Repository.java"
            (dm, s)
        }

    }

    def generate(): Unit = {

      modelAndPath foreach {

        case (dm: EntityModel, dmPath) =>
          val result = TemplateEngine.velocity(BusinessServiceTemplatePath, Map("dm" -> dm.convert))
          OutputStreamFactory(dmPath) <# result

      }

    }
  }

  object GenerateServices extends CodeGenerationUnit {

    override lazy val modelAndPath: List[(Any, String)] = {

      p.getDataModelContainer.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.SERVER_SIDE_SERVICES)).map {
        case dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val s = s"${outPath}service${/}${dm.getAttribute("name")}Service.java"
          (dm, s)
      }
    }

    def generate(): Unit = {

      modelAndPath foreach {

        case (dm: EntityModel, dmPath) =>
          val result = TemplateEngine.velocity(DomainServiceTemplatePath, Map("dm" -> dm.convert))
          OutputStreamFactory(dmPath) <# result
      }

    }
  }

  object GenerateRestServices extends CodeGenerationUnit {

    override lazy val modelAndPath: List[(Any, String)] = {

      p.getDataModelContainer.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.SERVER_SIDE_CONTROLLER)).map {
        case dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val str = s"${outPath}rest${/}${dm.getAttribute("name")}RestService.java"
          (dm, str)
      }

    }

    def generate(): Unit = {
      modelAndPath foreach {

        case (dm: EntityModel, dmPath) =>
          val result = TemplateEngine.velocity(RestServiceTemplatePath, Map("dm" -> dm.convert,
            "enumTypes" -> dm.enumsAsJava))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

  object GeneratePasswordDto extends CodeGenerationUnit {

    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.find(p => p.containsAttribute("principal") && !p.isIgnoredWith(IgnoreType.DTO)) match {
        case None =>
          Nil
        case Some(dm) =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val p = s"${outPath}dto${/}PasswordDto.java"
          List((dm, p))
      }

    }

    override def generate(): Unit = {
      modelAndPath.foreach {
        case (dm: DataModel, outPath: String) =>
          val result = TemplateEngine.velocity(PasswordDtoTemplatePath, Map("dm" -> dm.convert))
          OutputStreamFactory(outPath) <# result
      }
    }
  }

  object GenerateEntityTransformer extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.TRANSFORMER)).map {
        case dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val p = s"${outPath}transformer${/}${dm.getAttribute("name")}EntityTransformer.java"
          (dm, p)
      }
    }

    def generate(): Unit = {

      modelAndPath foreach {

        case (dm: EntityModel, dmPath) =>
          val result = TemplateEngine.velocity(EntityTransformerTemplatePath, Map("dm" -> dm.convert))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

  object GenerateCacheFiller extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      val models: List[EntityModel] = p.getDataModelContainer.entityModels.filter(p => p.containsAttribute("serverSideCacheable") && p.cacheKeyElements.nonEmpty)
      if (models.nonEmpty) {
        val outPath = makeValidPath(path, models.head.getAttributeOption("domain"))
        val str = s"${outPath}cache${/}CacheFiller.java"
        List((models, str))
      } else {
        Nil
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (dm: List[DataModel], str) =>
          val packageName: String = dm.head.getAttribute("domain")
          val result = TemplateEngine.velocity(CacheFillerTemplatePath, Map("models" -> dm.asJava, "packageName" -> packageName))
          OutputStreamFactory(str) <# result
      }
    }
  }

  object GenerateDtoTransformer extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.TRANSFORMER)).map {
        case dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val p = s"${outPath}transformer${/}${dm.getAttribute("name")}DtoTransformer.java"
          (dm, p)
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (dm: EntityModel, dmPath) =>
          val result = TemplateEngine.velocity(DtoTransformerTemplatePath, Map("dm" -> dm.convert))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

  object GenerateExtraEnums extends CodeGenerationUnit {
    override lazy val modelAndPath: List[(Any, String)] = {

      p.getDataModelContainer.enumModels.find(p => p.getAttribute("name") == "GeneralParameterType") match {
        case None => Nil
        case Some(x) =>
          val outPath = makeValidPath(path, x.getAttributeOption("domain"))
          val p = s"$outPath${x.getAttribute("name")}.java"
          List((x, p))
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (x: EnumModel, xPath) =>
          val result = TemplateEngine.velocity(GeneralParameterTypeEnumTemplatePath, Map("dm" -> x))
          OutputStreamFactory(xPath) <# result
        case _ => throw new IllegalArgumentException
      }
    }
  }

  object GenerateDto extends CodeGenerationUnit {

    override lazy val modelAndPath: List[(Any, String)] = {
      p.getDataModelContainer.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.DTO)).map {
        case dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          (dm, s"${outPath}dto${/}${dm.getAttribute("name")}Dto.java")
      }
    }

    def generate(): Unit = {
      modelAndPath foreach {
        case (dm: EntityModel, dmPath) =>
          val result = TemplateEngine.velocity(DtoTemplatePath, Map("dm" -> dm.convert))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

  //  object GenerateDomainObject extends CodeGenerationUnit {
  //
  //    override lazy val modelAndPath: List[(Any, String)] = {
  //      p.getDataModelContainer.entityModels.filterNot(p => p.isIgnoredWith(IgnoreType.DOMAIN_OBJECT)).map {
  //        case dm =>
  //          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
  //          val p = s"${outPath}domainobjects${/}${dm.getAttribute("name")}.java"
  //          (dm, p)
  //      }
  //    }
  //
  //    def generate() = {
  //      modelAndPath foreach {
  //        case (dm: EntityModel, dmPath) =>
  //          val result = TemplateEngine.velocity(DoTemplatePath, Map("dm" -> dm.convert))
  //          OutputStreamFactory(dmPath) <# result
  //      }
  //    }
  //  }

  object GeneratePrincipals extends CodeGenerationUnit {

    override lazy val modelAndPath: List[(Any, String)] = {

      p.getDataModelContainer.entityModels.filter(p => p.containsAttribute("principal") && !p.isIgnoredWith(IgnoreType.TRANSFORMER)).flatMap {
        case dm =>
          val outPath = makeValidPath(path, dm.getAttributeOption("domain"))
          val p = s"${outPath}transformer${/}PrincipalToEntityTransformer.java"
          val p2 = s"${outPath}transformer${/}PrincipalToDtoTransformer.java"
          List(((dm, PrincipalToEntityTemplatePath), p), ((dm, PrincipalToDtoTemplatePath), p2))
      }
    }

    def generate() = {
      modelAndPath foreach {
        case ((dm: EntityModel, template: String), dmPath) =>
          val result = TemplateEngine.velocity(template, Map("dm" -> dm.convert))
          OutputStreamFactory(dmPath) <# result
      }
    }
  }

}

object JavaCodeGenerator extends CodeGenerationCompanion {
  def apply(p: Project, pathConfig: CGConfig, writeInFile: Boolean) = {
    new JavaCodeGenerator(p, pathConfig, writeInFile)
  }
}

//object JavaCodeGenerator {
//  def props(p: Project, path: String): Props = Props(new JavaCodeGenerator(p, path))
//}