package com.caspco.spl.cg.model.layout

import com.caspco.spl.cg.analyze.exception.LayoutCellNotFoundException
import com.caspco.spl.cg.model.exception.LayoutAlreadyDefinedException
import com.caspco.spl.cg.model.view.AbstractWidget
import com.caspco.spl.cg.model.{BaseModel, ModelContainer}
import com.caspco.spl.cg.util.collection.WidgetListCollector._

import scala.collection.JavaConverters._

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 2/1/14
 *         Time: 4:27 PM
 */
class Layout extends BaseModel {

  protected var gridType: Option[GridType] = None

  def putWidgetToCell(cellName: String, w: AbstractWidget):Unit= {
    gridType match {
      case None =>
      case Some(g) =>
        g.getRows.flatMap(c => c.getCells.filter(_.containsAttribute(cellName))) match {
          case Nil => throw new LayoutCellNotFoundException(s"Cell $cellName in layout $getUniqueName")
          case x :: xs => x += w
        }
    }
  }

  def +=(gt: GridType): Layout = {
    gridType = Some(gt)
    this
  }

  override def containsElement(elemName: String): Boolean = ???

  def getGridType = gridType

  protected def getFooterWidget: Option[AbstractWidget] = None

  protected def getHeaderWidget: Option[AbstractWidget] = None

  protected def +=(w: AbstractWidget): Unit = ???

  protected def +=(ws: List[AbstractWidget]): Unit = ???
}


class SimpleLayout extends Layout {


  gridType = Some(new GridType)

  override def +=(ws: List[AbstractWidget]): Unit = {
    getHeaderWidget match {
      case Some(x) => this += x
      case None =>
    }

    ws.foreach {
      case w => this += w
    }

    getFooterWidget match {
      case Some(x) => this += x
      case None =>
    }
  }

  override def +=(w: AbstractWidget): Unit = {
    val r = new Row
    val c = new Cell
    c += w
    r += c
    gridType.get += r
  }
}

class OneColumnLayout extends MultipleColumnLayout(1)
class TwoColumnLayout extends MultipleColumnLayout(2)
class FourColumnLayout extends MultipleColumnLayout(4)
//class MultipleColumnLayout extends Layout {
//
//  gridType = Some(new GridType)
//
//  def +=(ws: WidgetTuple): Unit = ws match {
//
//    case TwoWidgets(w1, w2) =>
//      val c1 = new Cell
//      c1 += w1
//      val c2 = new Cell
//      c2 += w2
//      val r = new Row
//      r += List(c1, c2)
//      gridType.get += r
//    case OneWidget(w) =>
//      this += w
//    case _ =>
//  }
//
//  override def +=(w: AbstractWidget): Unit = {
//    val r = new Row
//    val c = new Cell
//    c += w
//    r += c
//    gridType.get += r
//  }
//
//  override def +=(ws: List[AbstractWidget]): Unit = {
//
//    getHeaderWidget match {
//      case Some(x) => this += x
//      case None =>
//    }
//
//    ws.groupWidgets(2, sort = true).foreach {
//      case w => this += w
//    }
//
//    getFooterWidget match {
//      case Some(x) => this += x
//      case None =>
//    }
//  }
//}

class MultipleColumnLayout(cols:Int) extends Layout {

  gridType = Some(new GridType)

  override def +=(ws: List[AbstractWidget]): Unit = {

    getHeaderWidget match {
      case Some(x) => this += x
      case None =>
    }

    ws.groupWidgets(cols, sort = true).foreach {
      case w => this += w
    }

    getFooterWidget match {
      case Some(x) => this += x
      case None =>
    }
  }

  def +=(wt: WidgetTuple): Unit = wt match {

    case MultipleWidgets(wsx) =>
      val r = new Row
      val map: List[Cell] = wsx.map(x => {
        val cell = new Cell; cell += x; cell
      })
      r += map
      gridType.get += r
    case OneWidget(w) =>
      this += w
    case _ =>
  }

  override def +=(w: AbstractWidget): Unit = {
    val r = new Row
    val c = new Cell
    c += w
    r += c
    gridType.get += r
  }
}


//class CreateFormLayout extends TwoColumnLayout {
//
//  protected val footerWidget = new ButtonGroup(Map("name" -> "btnGroup"))
//
//  val saveButton = new Button(Map("name" -> "saveButton", "label" -> "Save", "action" -> "insert"))
//  saveButton += SaveAction
//  footerWidget += saveButton
//
//  val cancelButton = new Button(Map("name" -> "cancelButton", "label" -> "Cancel", "action" -> "cancel"))
//  cancelButton += CancelAction
//  footerWidget += cancelButton
//
//  //  override def +=(ws: List[AbstractWidget]): Unit = {
//  //    ws.foreach {
//  //      case w => this += w
//  //    }
//  //
//  //    this += footerWidget
//  //  }
//
//  override def getFooterWidget: Option[AbstractWidget] = Some(footerWidget)
//
//  override def getHeaderWidget: Option[AbstractWidget] = None
//}


//class SearchFormLayout extends SimpleLayout {
//
//}
//
//class EditFormLayout extends SimpleLayout {
//
//}

class GridType extends BaseModel {

  private var rows: List[Row] = List.empty[Row]

  def +=(r: Row): Unit = rows :+= r

  def getRowsAsJava = getRows.asJava

  def getRows = rows

}

class Row extends BaseModel {

  private var cells: List[Cell] = List.empty[Cell]

  def +=(c: Cell): Unit = cells :+= c

  def +=(cs: List[Cell]): Unit = cells ++= cs

  def getCellsAsJava = getCells.asJava

  def getCells = cells
}

class Cell extends BaseModel {

  private var widgets: List[AbstractWidget] = List.empty[AbstractWidget]
  private var layout: Option[Layout] = None

  def getLayout = layout

  def +=(l: Layout): Unit = layout = Some(l)

  def +=(w: AbstractWidget): Unit = widgets :+= w

  def getWidgetsAsJava = getWidgets.asJava

  def getWidgets = widgets

}

class LayoutContainer extends ModelContainer[String, Layout] {

  def +=(m: Layout): Unit = {
    // it does not check inner classes
    if (modelMap.contains(m.getUniqueName))
      throw new LayoutAlreadyDefinedException(s"Layout ${m.getUniqueName}")
    nameSet += m.getUniqueName
    modelMap ++= Map(m.getUniqueName -> m)
  }
}