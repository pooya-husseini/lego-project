package com.caspco.spl.cg.util.string

import scala.util.control.Breaks._

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 10/04/14
  *         Time: 19:17
  */
object StringGrouping {

  implicit class InnerClass(list: List[String]) {
    def groupByWithPrefix(): Map[String, List[String]] = list match {
      case Nil => Map.empty[String, List[String]]
      case xs =>
        var group = Map[String, List[String]]()
        var lastIndex = 0

        var tempLastIndex = 0
        breakable {
          while (group.size <= 1) {
            group = xs.groupBy(f => f.substring(0, {
              tempLastIndex = f.indexOf(".", lastIndex)

              if (tempLastIndex == lastIndex || tempLastIndex < 0)
                break()
              tempLastIndex
            }))

            lastIndex = tempLastIndex + 1
          }
        }
        group
    }
  }

}
