package com.caspco.spl.cg.generator.sql.mysql

import com.caspco.spl.cg.model.datamodel._

import scala.collection.JavaConverters._

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 25/06/14
 *         Time: 14:45
 */
object MySqlDataTypeConverter {

  implicit class EntityModelConverter(dm: EntityModel) {
    def convert(): MySqlTable = {

      val map = dm.getElements.map {
        case x =>
          val dataType = SqlTypeConverter(x)
          new MySqlColumn(x.getAttribute("name"), dataType, x.isIdentifier)
      }
      val tableName=if(dm.containsAttribute("tableName")){
        dm.getAttribute("tableName")
      }else{
        dm.getAttribute("name")
      }
      val table = new MySqlTable(tableName)
      table += map
      table
    }
  }

  implicit class EnumModelConverter(dm: EnumModel) {
    def convert(): MySqlCollection = {

      val map = dm.getElements.map {
        case x =>
          x.getValue.get
      }

      val collection = new MySqlCollection(dm.getAttribute("name"))
      collection += map
      collection
    }
  }

}

object SqlTypeConverter {
  def apply(m: AbstractElement): String = m match {

    case IntegerType(_) => "INT"
    case BooleanType(_) => "BIT"
    case ListType(_) => "???"
    case MapType(_) => "???"
    case DateTimeType(_) => "DATE"
    case CurrencyType(_) => "???"
    case DecimalType(_) => "DECIMAL"
    case FileType(_) => "???"
    case DoubleType(_) => "DOUBLE"
    case FloatType(_) => "FLOAT"
    case LongType(_) => "BIGINT"
    case StringType(_) => "VARCHAR(100)"
    case CompositionType(_) => "???"
//    case DataModelType(_,_) => "???"
  }
}

class MySqlCollection(name: String) {
  private var values = List.empty[String]

  def +=(value: String): Unit = {
    values :+= value
  }

  def +=(values: List[String]): Unit = {
    this.values ++= values
  }

  def getValuesAsJava = getValues.asJava

  def getValues = values

  def getName = name
}

class MySqlTable(name: String) {
  private var columns = List.empty[MySqlColumn]

  def +=(m: MySqlColumn): Unit = {
    columns +:= m
  }

  def +=(m: List[MySqlColumn]): Unit = {
    columns ++= m

  }

  def getName = name

  def getElementsAsJavaList = columns.asJava

  def getPrimaryKey = columns.find(_.isId)
}

class MySqlColumn(name: String, colType: String, id: Boolean) {
  def getName = name

  def getColType = colType

  def isId = id
}