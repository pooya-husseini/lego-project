package com.caspco.spl.cg.util.formatter
/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 27/04/14
 *         Time: 22:23
 */
trait CodeBeautifier {
  def apply(input: String): String
}