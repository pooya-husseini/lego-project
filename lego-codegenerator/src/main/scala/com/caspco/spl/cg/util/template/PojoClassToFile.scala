package com.caspco.spl.cg.util.template

import java.io.{File, FileOutputStream, OutputStreamWriter}

import com.caspco.spl.cg.util.io.StreamUtils
import com.strobel.decompiler.{Decompiler, DecompilerSettings, PlainTextOutput}


/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 06/10/14
  *         Time: 13:48
  */
object PojoClassToFile {
  def apply(c: Class[_], path: String): Unit = {
    apply(c.getName, path)
  }

  def apply(c: String, path: String): Unit = {
    val settings: DecompilerSettings = DecompilerSettings.javaDefaults
    val stream = new FileOutputStream(path)
    val writer = new OutputStreamWriter(stream)
    Decompiler.decompile(c, new PlainTextOutput(writer), settings)
    writer.flush()
    writer.close()
  }

  def generateFromClassFile(c: String, path: String): Unit = {
    apply(StreamUtils.tempPath + StreamUtils./ + c, path)
    val file: File = new File(c)
    if (file.exists()) {
      file.delete()
    }
  }
}