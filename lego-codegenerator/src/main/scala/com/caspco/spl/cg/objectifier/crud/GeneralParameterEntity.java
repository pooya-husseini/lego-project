package com.caspco.spl.cg.objectifier.crud;

import com.caspco.spl.wrench.annotations.cg.*;
import com.caspco.spl.wrench.locale.Locale;
import org.hibernate.envers.Audited;

import javax.annotation.Generated;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Translations({
        @Translation(label = "مقادیر پایه", locale = Locale.Persian_Iran),
        @Translation(label = "General Parameters", locale = Locale.English_United_States)
})
@Generated(value = "Spl code generator")
@Table(name = "GeneralParameter", uniqueConstraints = @UniqueConstraint(columnNames = {"mainType", "code"}))
@Audited
@ClientSideCacheable(retainClean = true)
@ServerSideCacheable(retainClean = true)
class GeneralParameterEntity {

    private Long id;
    private String mainType;
    private String code;
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column
    @Translations({@Translation(label = "کد", locale = Locale.Persian_Iran),
            @Translation(label = "Code", locale = Locale.English_United_States)
    })
    @NotNull
    @CacheKey(priority = 0)
    @ExactOnSearch
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column
    @Translations({@Translation(label = "توضیحات", locale = Locale.Persian_Iran),
            @Translation(label = "Description", locale = Locale.English_United_States)
    })
    @NotNull
    @CacheValue(priority = 0)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column
    @Translations({@Translation(label = "نوع", locale = Locale.Persian_Iran),
            @Translation(label = "Main type", locale = Locale.English_United_States)
    })
    @NotNull
    @CacheKey(priority = 1)
    @ExactOnSearch
    public String getMainType() {
        return mainType;
    }

    public void setMainType(String mainType) {
        this.mainType = mainType;
    }

}