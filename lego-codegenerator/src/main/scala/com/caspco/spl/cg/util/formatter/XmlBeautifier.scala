package com.caspco.spl.cg.util.formatter

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: ${Date}
 *         Time: ${time}
 */
object XmlBeautifier extends CodeBeautifier {
  override def apply(input: String): String = {
    val source = scala.io.Source.fromString(input)
    val parser = scala.xml.parsing.ConstructingParser.fromSource(source, preserveWS = false)
    val printer = new scala.xml.PrettyPrinter(80, 2)
    printer.format(parser.document().docElem)
  }
}
