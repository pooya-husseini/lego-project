package com.caspco.spl.cg.util.types

import javax.persistence._

import com.caspco.spl.wrench.annotations.cg._
import com.caspco.spl.wrench.annotations.cg.security.{Password, Security, UserName, UserPrincipal}
import com.caspco.spl.wrench.annotations.validation.Email

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 11/05/14
 *         Time: 14:18
 */
object Types {

  lazy val idType = classOf[Id]
  lazy val notUiType=classOf[NotUi]
  lazy val searchPageType=classOf[SearchPage]

  lazy val versionType=classOf[Version]
  lazy val ignoreType=classOf[Ignore]
  lazy val entityType = classOf[javax.persistence.Entity]
  lazy val generatedType = classOf[Generated]
  lazy val tableType = classOf[javax.persistence.Table]
  lazy val generatedValueType = classOf[GeneratedValue]
  lazy val searchableType = classOf[Searchable]
  lazy val notNullType = classOf[javax.validation.constraints.NotNull]
  lazy val maxType = classOf[javax.validation.constraints.Max]
  lazy val minType = classOf[javax.validation.constraints.Min]
  lazy val emailType = classOf[Email]
  lazy val numericType = classOf[com.caspco.spl.wrench.annotations.validation.Numeric]
  lazy val stringEqualType = classOf[com.caspco.spl.wrench.annotations.validation.StringEquals]
  lazy val patternType = classOf[javax.validation.constraints.Pattern]
  lazy val sizeType = classOf[javax.validation.constraints.Size]
  lazy val createOnSelectType = classOf[CreateOnSelect]
  lazy val passwordType = classOf[Password]
  lazy val userPrincipalType = classOf[UserPrincipal]
  lazy val usernameType = classOf[UserName]
  lazy val generalParameterType = classOf[GeneralParameter]
  lazy val lookupType = classOf[Lookup]
  lazy val textAreaType = classOf[TextArea]
  lazy val priorityType = classOf[Priority]
  lazy val translationsType = classOf[Translations]
  lazy val visibilityType = classOf[Visibility]
  lazy val disabilityType = classOf[Disability]
  lazy val autoInjectType = classOf[AutoInject]
  lazy val injectableType = classOf[Injectable]
  lazy val securityType = classOf[Security]
  lazy val enumeratedWithType = classOf[com.caspco.spl.wrench.annotations.cg.EnumeratedWith]
  lazy val oneToManyType = classOf[OneToMany]
  lazy val manyToOneType = classOf[ManyToOne]
  lazy val manyToManyType = classOf[ManyToMany]
  lazy val oneToOneType = classOf[OneToOne]
  lazy val titleType = classOf[Title]
  lazy val intType = classOf[java.lang.Integer]
  lazy val stringType = classOf[java.lang.String]
  lazy val booleanType = classOf[java.lang.Boolean]
  lazy val dateType = classOf[java.util.Date]
  lazy val sqlDateType = classOf[java.sql.Date]
  lazy val bigDecimalType = classOf[java.math.BigDecimal]
  lazy val currencyType = classOf[java.util.Currency]
  lazy val doubleType = classOf[java.lang.Double]
  lazy val listType = classOf[java.util.List[_]]
  lazy val longType =classOf[java.lang.Long]
  lazy val floatType =classOf[java.lang.Float]
  lazy val fatEntityType=classOf[FatEntity]
  lazy val crudWidgetType=classOf[CrudWidget]
  lazy val fileIdType=classOf[FileId]
  lazy val numberType=classOf[java.lang.Number]
//  lazy val pushToClientCacheType=classOf[PushToClientCache]

  lazy val clientSideCacheableType=classOf[ClientSideCacheable]
  lazy val serverSideCacheableType=classOf[ServerSideCacheable]
  lazy val cacheKeyType=classOf[CacheKey]
  lazy val cacheValueType=classOf[CacheValue]

}