package com.caspco.spl.cg.model.view

import com.caspco.spl.cg.model.exception.{ApplicationAlreadyDefinedException, PageAlreadyDefinedException}
import com.caspco.spl.cg.model.layout.Layout
import com.caspco.spl.cg.model.{BaseModel, ModelContainer}
import com.caspco.spl.cg.util.constant.Constants

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 2/1/14
 *         Time: 4:23 PM
 */

class MainPage(createPage: AbstractPage) extends AbstractPage {
  def getCreatePage = createPage
}

class AbstractPage extends BaseModel {

  private var layout: Option[Layout] = None
  private var widgets: List[AbstractWidget] = List.empty[AbstractWidget]

  def +=(w: AbstractWidget): Unit = {
    widgets :+= w
  }

  def +=(ws: List[AbstractWidget]): Unit = {
    widgets ++= ws
  }

  def +=(l: Layout): Unit = {
    layout = Some(l)
  }

  def getLayout = layout

  override def containsElement(elemName: String): Boolean =
    widgets.exists(p => p.getAttributes().getOrElse("name", "") == elemName)

  def getWidgetsWithAction = {
    getLayoutWidgets.filter(p => p.getAction.isDefined)
  }

  def getLayoutWidgets: List[AbstractWidget] = layout match {
    case None => Nil
    case Some(l) => l.getGridType match {
      case None => Nil
      case Some(g) => g.getRows.flatMap(r => r.getCells.map(c => c.getWidgets)).flatten
    }
  }
}

class PopupPage extends AbstractPage

class CreatePopupPage extends PopupPage

class CrudPopupPage(editPage: PopupPage, showPage: PopupPage, searchPage: PopupPage) extends PopupPage {
  //  def getCreatePage = createPage

  def getEditPage = editPage

  def getShowPage = showPage

  def getSearchPage = searchPage
}

class PageContainer extends ModelContainer[String, (AbstractPage, Option[List[String]])] {
  lazy val allPages: List[(AbstractPage, Option[List[String]])] = (if (isApplicationDefined) {
    Map(application.get._1.getUniqueName -> application.get) ++ modelMap
  } else {
    modelMap
  }).values.toList
  //  lazy val allPages: List[(AbstractPage, Option[List[String]])] = modelMap.values.toList
  lazy val popupPages = allPages.filter {
    case (x: PopupPage, _) => true
    case _ => false
  }


  lazy val popupPagesForCreation = allPages.filter {
    case (x: CreatePopupPage, _) => true
    case _ => false
  }

  lazy val mainPages = allPages.filter {
    case (x: PopupPage, _) => false
    case _ => true
  }

  lazy val categorizeByPrefix: Map[String, List[ModelDescription]] = {
    import com.caspco.spl.cg.util.string.StringGrouping._
    val mPages = modelMap.filter {
      case (_, (p: PopupPage, _)) => false
      case _ => true
    }
    val domains = mPages.keys.toList
    val group = domains.groupByWithPrefix()
    group.map {
      case (k, v) => k -> v.map(domain => {
        val page: AbstractPage = modelMap(domain)._1
        ModelDescription(domain.substring(0, domain.lastIndexOf(".")), page.getAttributeOrElse("name", ""),
          page.getAttributes(Constants.TranslationPattern).toList)
      })
    }
  }
  private var application: Option[(AbstractPage, Option[List[String]])] = None

  def +=(m: AbstractPage): Unit = {
    this +=(m, None)
  }

  def +=(m: (AbstractPage, Option[List[String]])): Unit = {
    // it does not check inner classes
    val pageName = m._1.getUniqueName
    if (modelMap.exists(p => p._1 == pageName) ||
      (application.isDefined && application.get._1.getUniqueName == pageName))
      throw new PageAlreadyDefinedException(s"Page $pageName")

    nameSet += pageName
    modelMap ++= Map(pageName -> m)
  }

  def isApplicationDefined = application.isDefined

  def getApplication = application

  def setApplication(m: AbstractPage): Unit = {
    setApplication((m, None))
  }

  def setApplication(m: (AbstractPage, Option[List[String]])): Unit = {
    val pageName = m._1.getUniqueName
    application match {
      case None =>

        if (modelMap.exists(p => p._1 == pageName))
          throw new PageAlreadyDefinedException(s"Application $pageName")
        application = Some(m)
      case Some(x) => throw new ApplicationAlreadyDefinedException(s"Application $pageName")
    }
  }
}

case class ModelDescription(var domain: String, var name: String, var translations: List[(String,String)]) {
  def getDomain = domain

  def getName = name

  def getTranslations = translations

  def getTranslation(key:String):Option[String]={
    translations.toMap.get(key)
  }
}