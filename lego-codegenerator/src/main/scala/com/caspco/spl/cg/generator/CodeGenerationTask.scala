package com.caspco.spl.cg.generator

import com.caspco.spl.cg.model.project.Project

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 06/08/14
  *         Time: 17:38
  */
trait CodeGenerationTask {
  def apply(p: Project, path: String): Unit

  def getPaths(p: Project): List[String]
}