package com.caspco.spl.cg.util.classutils

import java.net.URL


/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 20/10/14
  *         Time: 18:15
  */
object ClassUtils {

  def whereFrom(c: Class[_]): Option[URL] = {
    def highestClassLoader(loader: ClassLoader): Option[ClassLoader] = Option(loader) match {
      case None => None
      case Some(x) => Option(x.getParent) match {
        case None => Some(x)
        case Some(y) =>
          highestClassLoader(y)
      }
    }

    highestClassLoader(Option(c.getClassLoader) match {
      case None =>
        ClassLoader.getSystemClassLoader
      case Some(x) =>
        x
    }) match {
      case None => None
      case Some(x) =>

        val name = c.getCanonicalName

        val s: String = name.replace(".", "/") + ".class"
        val resource = x.getResource(s)
        Option(resource) match {
          case Some(y) => Some(y)
          case None =>
            Option(c.getClassLoader.getResource(s)) match {
              case None => None
              case Some(url) => Some(url)
            }
        }
    }
  }
}