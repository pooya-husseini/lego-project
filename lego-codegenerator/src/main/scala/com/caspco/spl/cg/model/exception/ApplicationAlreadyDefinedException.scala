package com.caspco.spl.cg.model.exception

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 2/2/14
  *         Time: 12:23 PM
  */
class ApplicationAlreadyDefinedException(message: String) extends ModelException(message) {

}
