package com.caspco.spl.cg.model

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 2/4/14
  *         Time: 3:05 PM
  */

trait ModelContainer[A, B] {
  protected var nameSet = Set.empty[A]
  protected var modelMap = Map.empty[A, B]

  def contains(name: A, recursive: Boolean = false): Boolean = modelMap.contains(name) match {
    case x =>
      if (x && recursive) x || nameSet.contains(name)
      else x
  }

  def getMap = modelMap

  def +=(m: B): Unit

  def values = modelMap.values

  def get(key: A): Option[B] = modelMap.get(key)
}