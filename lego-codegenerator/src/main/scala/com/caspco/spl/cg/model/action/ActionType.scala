package com.caspco.spl.cg.model.action

/**
 * @author : Пуя Гуссейни
 *         Email : info@pooya-hfp.ir
 *         Date: 27/04/14
 *         Time: 15:12
 */
abstract class ActionType
case object SaveAction extends ActionType
case object CancelAction extends ActionType
case object UpdateAction extends ActionType
case object DeleteAction extends ActionType
case object SelectAction extends ActionType
case object CustomAction extends ActionType