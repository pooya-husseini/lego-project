package com.caspco.spl.cg.util.string

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
  *         Date: ${Date}
  *         Time: ${time}
  */
object ConstantStringMaker {
  def apply(str: String): String = {
    str.zipWithIndex.map(x => if (x._1.isUpper && x._2 != 0) s"_${x._1}" else x._1.toUpper).mkString
  }
}
