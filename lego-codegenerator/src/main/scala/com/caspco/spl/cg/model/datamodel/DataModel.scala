package com.caspco.spl.cg.model.datamodel

import com.caspco.spl.cg.model.exception._
import com.caspco.spl.cg.model.view.ModelDescription
import com.caspco.spl.cg.model.{BaseModel, ModelContainer}
import com.caspco.spl.cg.util.constant.Constants

import scala.collection.JavaConverters._

/**
  * @author : Пуя Гуссейни
  *         Email : info@pooya-hfp.ir
  *         Date: 1/29/14
  *         Time: 12:25 PM
  */

class DataModel(collection: Boolean) extends BaseModel {
  // I've assume that the elements won't change during runtime
  lazy val getIdentifiers = {
    val result = elements.filter(_.isIdentifier)
    if (result.nonEmpty) {
      result
    } else {
      throw new IdentifierNotDefinedException(getUniqueName)
    }
  }

  lazy val relations = elements.filter(p => p ?? "relation" && !(p ?? "autoinject"))

  lazy val genericRelations = {
    relations filter {
      case x: ListType => true
      case _ => false
    }
  }
  // I've assumed that relations are fields with compositionType

  lazy val simpleRelations = {
    relations filter {
      case x: CompositionType if !(x ?? "autoinject") => true
      case _ => false
    }
  }

  lazy val nonRelatedElements = elements.filterNot(p => p ?? "relation")
  lazy val nonRelatedElementsAsJava = nonRelatedElements.asJava

  lazy val cacheKeyElements = elements.filter(p => p ?? "cacheKey").sortWith(_ ("cacheKey").toInt < _ ("cacheKey").toInt)
  lazy val cacheKeyElementsAsJava = cacheKeyElements.asJava
  lazy val cacheValueElements = elements.filter(p => p ?? "cacheValue").sortWith(_ ("cacheValue").toInt < _ ("cacheValue").toInt)
  lazy val cacheValueElementsAsJava = cacheValueElements.asJava
  lazy val nonRelatedElementsAsJavaReverse = nonRelatedElements.reverse.asJava
  lazy val genericRelationsAsJava = genericRelations.asJava
  lazy val simpleRelationsAsJava = simpleRelations.asJava
  lazy val searchableRelations = relations.filter(p => p ?? "searchable")
  lazy val relationsAsJava = relations.asJava
  lazy val relationsAsJavaReverse = relations.reverse.asJava
  lazy val relationNames = relations.map(p => p.getAttribute("name"))
  lazy val relatedModelNames = getRelatedDataModels.map(p => p.getUniqueName)
  lazy val searchableRelationsAsJava = searchableRelations.asJava
  lazy val relatedDataModelsAsJava = relatedDataModels.asJava
  lazy val uniqueRelatedDataModels = (relatedDataModels.map(p => (p.getAttribute("name"),
    p)).toMap ++ Map(getAttribute("name") -> this)).values.toList
  lazy val uniqueRelatedDataModelsAsJava = uniqueRelatedDataModels.asJava
  lazy val enumeratedElements = {
    elements.filter(_ ?? "enumerated")
  }
  lazy val enumeratedElementsAsJava = enumeratedElements.asJava
  lazy val autoInjectedElements = elements.filter(p => p ?? "autoinject")
  lazy val autoInjectedElementsAsJava = autoInjectedElements.asJava
  lazy val fileIdElements = elements.filter(_ ?? "fileId")
  lazy val fileIdElementsAsJava = fileIdElements.asJava
  lazy val pushToClientCacheElements = elements.filter(_ ?? "pushToClientCache")
  lazy val pushToClientCacheElementsAsJava = pushToClientCacheElements.asJava


  lazy val titleElements = {
    elements.filter(p => p ?? "title")
  }
  lazy val titleElementsAsJava = titleElements.asJava
  //  lazy val allSimpleModels = (simpleRelations.map(p => (p.getAttribute("name"), p)).toMap ++
  //    Map(getAttribute("name") -> this)).map(_._2).toList
  //  lazy val allSimpleModelsAsJava = allSimpleModels.asJava
  protected var elements = List.empty[AbstractElement]
  protected  var innerDataModels = List.empty[DataModel]
  protected  var relatedDataModels = List.empty[DataModel]

  def containsInnerClass = innerDataModels.nonEmpty

  // todo solve page widget model bound name that is not in data model
  // todo solve html files reference to js directory problem
  override def containsElement(elemName: String): Boolean =
    elements.exists(p => p.getAttributes().getOrElse("name", "") == elemName)

  def +=(element: AbstractElement): Unit = {
    elements :+= element
  }

  def +=(elems: List[AbstractElement]): Unit = {
    elements ++= elems
  }

  def clearElements() = {
    elements = List.empty[AbstractElement]
  }

  def getInnerDataModels = innerDataModels

  def getRelatedDataModels = relatedDataModels

  def +=(dm: DataModel): Unit = {
    innerDataModels :+= dm
  }

  def addRelatedDataModel(dm: DataModel): Unit = {
    relatedDataModels +:= dm
  }

  def getElements = elements

  def getElementsAsJava = elements.asJava

  def getNonIdentifierElements = elements filterNot getIdentifiers.contains

  def getIdentifier = getIdentifiers.head

  def isCollection = collection

  def getSearchableElements = elements.filter(_ ?? "searchable")

  def filterElements(p: (AbstractElement) => Boolean) = elements.filter(p)

  def getRelatedDataModel(modelName: String): Option[DataModel] = {
    val index = modelName.lastIndexOf(".") match {
      case x if x < 0 => 0
      case x => x
    }
    val name = modelName.substring(index)
    relatedDataModels.filter(_.getAttribute("name") == name) match {
      case Nil => None
      case x :: xs => Some(x)
    }
  }

  override def toString: String = getUniqueName

  def makeElementsString(separator: String, attName: String, prefix: String, postFix: String): String = {
    elements.filter(p => p ?? attName).map(p => p.getAttribute(attName)).map(p => prefix + p + postFix).mkString(separator)
  }

}

class EnumModel extends DataModel(true)

class EntityModel extends DataModel(false)

class DataModelContainer extends ModelContainer[String, DataModel] {

  lazy val allModels: List[DataModel] = {
    modelMap.values.toList
  }

  lazy val entityModels: List[EntityModel] = {
    modelMap.values.filter(p => !p.isCollection).map(p => p.asInstanceOf[EntityModel]).toList
  }

  lazy val enumModels: List[EnumModel] = {
    modelMap.values.filter(p => p.isCollection).map(p => p.asInstanceOf[EnumModel]).toList
  }
  lazy val categorizeByPrefix: Map[String, List[ModelDescription]] = {
    import com.caspco.spl.cg.util.string.StringGrouping._

    val domains = modelMap.keys.toList
    val group = domains.groupByWithPrefix()
    group.map {
      case (k, v) => k -> v.map(domain => {
        val model = modelMap(domain)
        ModelDescription(domain.substring(0, domain.lastIndexOf(".")), model.getAttributeOrElse("name", ""),
          model.getAttributes(Constants.TranslationPattern, removePattern = true).toList)
      })
    }
  }
  lazy val precedencePackageName: String = {
    val domains = modelMap.keys.toList
    getPrecedencePackageName(domains)
  }

  def getPrecedencePackageName(packages: List[String]): String = {
    val split: Array[String] = packages.head.split("\\.")
    split.reduce(
      (x, y) => {
        if (packages.forall(_.startsWith(s"$x.$y"))) {
          s"$x.$y"
        } else if (packages.forall(_.startsWith(x))) {
          s"$x"
        } else {
          ""
        }
      }
    )
  }

  def getDataModel(name: String): Option[DataModel] = {
    modelMap.get(name)
  }

  def +=(m: DataModel): Unit = {
    if (modelMap.contains(m.getUniqueName))
      throw new DataModelAlreadyDefinedException(s"Data model ${m.getUniqueName}")
    nameSet += m.getUniqueName

    def recursivelyAddInnerModels(model: DataModel): Unit = {
      if (model.containsInnerClass) {
        model.getInnerDataModels foreach {
          case inner =>
            val name = model.getUniqueName + "." + inner.getUniqueName
            if (nameSet.contains(name))
              throw new DataModelAlreadyDefinedException(name)
            nameSet += name
            recursivelyAddInnerModels(inner)
        }
      }
    }

    recursivelyAddInnerModels(m)
    modelMap ++= Map(m.getUniqueName -> m)
  }
}